module.exports = {
    compare : function(a,b){
    	let r = a - b;
    	if (r > 0) {
    		return 1;
    	} else if(r < 0) {
    		return -1;
    	} else {
    		return 0;
    	}
    }
    
}