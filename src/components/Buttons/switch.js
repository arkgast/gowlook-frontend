import React, {Component} from 'react'
// material ui
import RaisedButton from 'material-ui/RaisedButton'
// material ui colors
import {grey300} from 'material-ui/styles/colors'

class Switch extends Component {
  constructor() {
    super()
    this.state = {
      hover1: false,
      hover2: false,
    }
  }
  toggleHover(type, value) {
    this.setState({
      [type]: value
    });
    /*
    this.setState(() => {
      this.state[type] = value
      return this.state
    })*/
  }
  render() {
    return (
        <div>
          <RaisedButton
            primary={( this.props.tab === 1 && !this.state.hover2 ) || this.state.hover1}
            label={this.props.label1}
            onTouchTap={this.props.buttonOneAction}
            onMouseOver={this.toggleHover.bind(this, "hover1", true)}
            onMouseLeave={this.toggleHover.bind(this, "hover1", false)}
            labelStyle={{textTransform: 'capitalize'}}
            className="btn-left"
            style={{
              borderRadius: '40px 0px 0px 40px',
              boxShadow: 'none',
              border: '1px solid ' + grey300,
              borderRight: 'none'
            }}
            overlayStyle={{borderRadius: '40px 0px 0px 40px'}}
            rippleStyle={{borderRadius: '40px 0px 0px 40px'}}
            buttonStyle={{borderRadius: '40px 0px 0px 40px'}} />
          <RaisedButton
            primary={( this.props.tab === 2 && !this.state.hover1 ) || this.state.hover2}
            label={this.props.label2}
            onMouseOver={this.toggleHover.bind(this, "hover2", true)}
            onMouseLeave={this.toggleHover.bind(this, "hover2", false)}
            onTouchTap={this.props.buttonTwoAction}
            labelStyle={{textTransform: 'capitalize'}}
            style={{
              borderRadius: '0px 40px 40px 0px',
              boxShadow: 'none',
              border: '1px solid ' + grey300,
              borderLeft: 'none'
            }}
            overlayStyle={{borderRadius: '0px 40px 40px 0px'}}
            rippleStyle={{borderRadius: '0px 40px 40px 0px'}}
            buttonStyle={{borderRadius: '0px 40px 40px 0px'}}
            className="btn-rigth" />
        </div>
      )
  }
}
export default Switch
