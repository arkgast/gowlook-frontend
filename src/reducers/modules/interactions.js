import {
  SET_COMPARATE_PRODUCTS,
  SELECT_DETAILS_PRODUCT,
  REMOVE_COMPARATE_PRODUCT,
  CLEAR_COMPARATIVE,
} from '../constants/interactionConst';

const initialStates = {
  comparativeProducts: [],
  selectedProduct: {}
}

export default function reducer(state = initialStates,  action) {
  switch (action.type) {
    case SET_COMPARATE_PRODUCTS:
      {
        return {
          ...state,
          comparativeProducts: [
            ...state.comparativeProducts,
            action.product
          ]
        }
      }
    case REMOVE_COMPARATE_PRODUCT:
      {
        return {
          ...state,
          comparativeProducts: state.comparativeProducts.filter( item => item.id !== action.productId)
        }
      }
    case CLEAR_COMPARATIVE:
      {
        return {
          ...state,
          comparativeProducts: []
        }
      }
    case SELECT_DETAILS_PRODUCT:
    {
      return {
        ...state,
        selectedProduct: action.selectedProduct
      }
    }
    default:
      break;
  }

  return state;
}
