import React from 'react';
import RaisedButton from 'material-ui/RaisedButton'
import Search from 'material-ui/svg-icons/action/search'
import {fullWhite} from 'material-ui/styles/colors'
import Pagination from 'material-ui-pagination'
import { connect } from 'react-redux'

import { productSearch } from '../../reducers/actions/productActions'
import './HomeSearcher.css'

class HomeSearcher extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      inputText: ''
    }
    this.onInputChange = this.onInputChange.bind(this)
    this.toggleSearchMenu = this.toggleSearchMenu.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  onInputChange (e) {
    const { value: inputText } = e.target
    this.setState({ inputText })
  }
  toggleSearchMenu () {
    const menu = this.refs.searchMenu
    const className = 'active'
    if (menu.classList.contains(className)) {
      menu.classList.remove('active')
    } else {
      menu.classList.add('active')
    }
  }
  handleSubmit (e) {
    e.preventDefault()
    const searchTerm = this.state.inputText
    this.setState({ inputText: '' })
    this.props.productSearch(searchTerm)
  }
  getResultsTotal (total = 0) {
    let message = `${total} resultado`
    return total === 0 || total > 1 ? message + 's' : message
  }
  renderCategories (categories) {
    return (
      <div className="search-menu-categories">
        {categories.map(category => (
          <a
            key={category.categoryEs.id}
            href="javascript:void()"
          >
            {category.categoryEs.name} ({category.counts})
          </a>
        ))}
      </div>
    )
  }
  renderProducts (products) {
    return (
      <div className="search-menu-items">
        {products.content.map((product) => (
          <div key={product.id} className="search-item">
            <img className="search-product-image" src={product.images[0]} alt={product.name} />
            <span className="search-product-name">{product.name}</span>
          </div>
        ))}
        <div className='pagination-wrapper'>
          <Pagination
            total={products.content.length}
            current={1}
            display={6}
          />
        </div>
      </div>
    )
  }
  render () {
    const { product, fetching, fetched } = this.props
    const categories = product.categoriesList || []
    const products = product.productList || { content: [] }
    return (
      <div>
        <div
          className="relative container-center"
          style={{width: '45%', margin: '0 auto', marginTop: '2%'}}
        >
          <form onSubmit={this.handleSubmit} style={{ width: '50%', margin: '0', padding: '0' }}>
            <input
              style={{
                border: '1px solid rgb(233, 233, 233)',
                borderRadius: '5px 0 0 5px',
                height: 32,
                outline: 'none',
                textIndent: 10,
                transition: '0.5s ease',
                width: '100%'
              }}
              onChange={this.onInputChange}
              onFocus={this.toggleSearchMenu}
              onBlur={this.toggleSearchMenu}
              value={this.state.inputText}
              placeholder={this.props.placeholder || 'Samsung Galaxy S7...'}
              className='home-input-search'
            />
          </form>
          <div className="button-search">
            <RaisedButton fullWidth primary icon={<Search color={fullWhite}/>}/>
          </div>
        </div>
        <div className="search-menu" ref="searchMenu">
          <div className="top-menu">
            <span className="text">{this.getResultsTotal(products.totalElements)}</span>
          </div>
          <div className="search-menu-wrapper">
            <div className="search-menu-container">

              {fetching && <h4>Cargando...</h4>}
              {!fetching && !fetched && <h4>Inicia tu busqueda</h4>}
              {!fetching && fetched && this.renderCategories(categories)}
              {!fetching && fetched && this.renderProducts(products)}

            </div>
          </div>
        </div>
      </div>)
  }
}

const mapStateToProps = state => {
  const { product, fetching, fetched } = state.product
  return {
    product,
    fetching,
    fetched
  }
}

export default connect(mapStateToProps, { productSearch })(HomeSearcher)
