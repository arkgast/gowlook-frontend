import React from 'react'

// material ui components
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import IconButton from 'material-ui/IconButton';
import {cyan500} from 'material-ui/styles/colors';

// material ui icons
import Cross from 'material-ui/svg-icons/navigation/close'

//assets
import logo from '../../assets/img/gowlook.png'

// common components
import MobileSearchInput from '../InputSearch/mobile'

// structure need to depurate
// stateles components
const DrawerMenu = (props) => (
  <Drawer open={props.show} width={ 400 } containerStyle={{overflowX: 'hidden'}} className="relative">
    <div className="flex-center">
      <img src={logo} alt="logo" style={{width: '230px', marginTop: '10px'}}/>
    </div>
    <IconButton style={{position: 'absolute', right: '10px', top: '5px'}} onTouchTap={props.close}>
      <Cross/>
    </IconButton>
    <div className="flex-center">
      <MobileSearchInput/>
    </div>
    {
      props.mainCategories.map( ( category, index ) => (
        <Menu key={index} style={{color: cyan500, marginTop: '20px', width: '100%'}}>
          <span style={{marginLeft: '20px'}}>{ category.name }</span>
          { category.id !== "undefined" &&
              props.subCategories[category.id].map((subCategory, key) => (
                <MenuItem key={key} style={{ marginTop: '10px', marginLeft: '10px' }}>
                  {subCategory.name} >>
                </MenuItem>
              ))
          }
        </Menu>
      ))
    }
  </Drawer>
)

export default DrawerMenu
