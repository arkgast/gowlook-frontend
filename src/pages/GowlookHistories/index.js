import React, {Component} from 'react'
import background from '../../assets/img/postech.png'
import { Grid, Row, Col } from 'react-flexbox-grid'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import SideBarMenu from '../../components/SideBarMenu'
import HistoryCard from '../../components/HistoryCard'
import { StickyContainer, Sticky } from 'react-sticky';
import {fetchCategories} from '../../reducers/actions/categoriesActions'
import './GowlookHistories.css'

class GowlookHistories extends Component {
  componentDidMount(){
    this.props.fetchCategories(10)
  }
  render() {
    const linkStyle = {textDecoration: 'none', margin: ' 0px 18px'}
    return (
      <div className="pull-left full-width">
        <div className="relative pull-left full-width background-white">
          <img className="bg-cover" src={background} alt=" " />
          <div className="cover-title">
            <h1>
              Gowlook stories
            </h1>
            <h2>
              technology is changing the world <br/>
              And we celebrate it
            </h2>
          </div>
          <div className="cover full-width">
            <Grid className="container">
              <Row>
                <Col xs={4}></Col>
                <Col xs={7} >
                  <Row>
                    <Col xs={5}>
                      <Link to="/postech" className="grey-link" style={linkStyle}>
                        Trending
                      </Link>
                      <Link to="/postech" className="grey-link" style={linkStyle}>
                        Latest
                      </Link>
                    </Col>
                    <Col xs={7}>
                      <Link to="/postech" className="pull-right grey-link" style={linkStyle}>
                        Our stories >
                      </Link>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Grid>
          </div>
        </div>
        <div className="pull-left margin-bottom-50 background-grey full-width">
          <StickyContainer>
            <div className="container" style={{paddingBottom: '30px'}}>
              <Grid fluid>
                <Row>
                  <Col xs={4} style={{paddingRight: "30px"}}>
                    <div className="border-right" style={{height: "100%"}}>
                      <Sticky>
                        <SideBarMenu categories={ this.props.mainCategories }/>
                      </Sticky>
                    </div>
                  </Col>
                  <Col xs={8}>
                    <h2 className="blue-title">Actualidad</h2>
                    {
                      [1,2,3,4].map(item =>
                        <HistoryCard className={`gradient${(item % 3) + 1}`} />
                      )
                    }
                  </Col>
                </Row>
              </Grid>
            </div>
          </StickyContainer>
        </div>
      </div>
    )
  }
}

// set redux states
const mapStates = (state) => ({
  mainCategories: state.categories.mainCategories,
})

// set redux dispatch actions
const mapDispatch = {
  fetchCategories,
}

export default connect(mapStates, mapDispatch)(GowlookHistories)
