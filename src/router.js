import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import { syncHistoryWithStore } from 'react-router-redux'

import App from './App'
import AsyncLoader from './hocs/AsyncLoader'


// Routes from A to Z please
const Comparison = AsyncLoader(() => import('./pages/Comparison'))
const Contact = AsyncLoader(() => import('./pages/Contact'))
const GowlookHistories = AsyncLoader(() => import('./pages/GowlookHistories'))
const Home = AsyncLoader(() => import('./pages/Home'))
const OurTeam = AsyncLoader(() => import('./pages/Comparison'))
const Page404 = AsyncLoader(() => import('./pages/Page404'))
const Postech = AsyncLoader(() => import('./pages/Postech'))
const PostechEditor = AsyncLoader(() => import('./pages/Postech/PostechEditor'))
const ProductSheet = AsyncLoader(() => import('./pages/ProductSheet'))
const Results = AsyncLoader(() => import('./pages/Results'))
const SinglePost = AsyncLoader(() => import('./pages/Postech/SinglePost'))

const Routes = (props) => {
  const customHistory = syncHistoryWithStore( props.history, props.store);
  return (
    <Router history={customHistory}>
      <App>
        <Switch>
          <Route exact path="/" render={() => <Home {...props}/>}/>
          <Route path="/histories" component={GowlookHistories}/>
          <Route exact path="/postech" component={Postech}/>
          <Route path="/postech/:category/:slug" component={SinglePost}/>
          <Route exact path="/postech/editor" component={PostechEditor}/>
          <Route path="/Results/:id/:mainPosition/:position" render={(props) => <Results {...props}/>}/>
          <Route path="/Sheet/:id" component={ProductSheet}/>
          <Route path="/comparison" component={Comparison}/>
          <Route path="/our-team" component={OurTeam}/>
          <Route path="/contacto" component={Contact}/>
          <Route path="*" component={Page404}/>
        </Switch>
      </App>
    </Router>
  );

}
export default Routes
