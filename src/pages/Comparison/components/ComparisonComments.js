import React from 'react'
import ReactStars from 'react-stars'
import avatar from '../../../assets/img/profileHistory.png'
import Pagination from 'react-js-pagination'
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu'
import FlatButton from 'material-ui/FlatButton'
import MenuItem from 'material-ui/MenuItem'
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down'

class ComparisonComments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openSortPopover: false
    }
  }

  render () {
    return (
      <div className='comparison-comments'>
        <div className='comparison-comments-top'>
          <h4 className='text-blue'>Todos los comentarios</h4>
          <span className='text-grey showing'>Mostrando 1 - 10 de 10</span>
          <span className='text-grey order'>Ordenar por &nbsp;
            <FlatButton
              className='sort-button'
              style={{margin: '0px 0px 20px 0px'}}
              labelStyle={{textTransform: 'capitalize'}}
              onTouchTap={(ev) => this.setState({openSortPopover: true, sortPopover: ev.currentTarget})}
              label="Ordenar">
              <KeyboardArrowDown />
            </FlatButton>
            <Popover
              open={this.state.openSortPopover}
              anchorEl={this.state.sortPopover}
              anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              onRequestClose={() => this.setState({openSortPopover: false})}
            >
              <Menu>
                <MenuItem primaryText="Fecha" />
                <MenuItem primaryText="Más votadas" />
              </Menu>
            </Popover>

            </span>
        </div>
        <div className='comparison-comments-all'>
          {
            Array(10).fill(null).map((item, index) => (
              <div className='comparison-comment' key={index}>
                <div className='comparison-comment-user'>
                  <h3 className='text-grey'>Lorem ipsum dolor</h3>
                  <img src={avatar} alt='name' />
                  <ReactStars
                    className='comparison-comment-user-stars'
                    count={5}
                    value={4}
                    size={10}
                    edit={false}
                    color2={'#00b5c8'} />
                  <h4 className='text-blue'>Gowlooker VIP</h4>
                  <span className='text-grey'>112 Opiniones</span>
                </div>
                <div className='comparison-comment-content'>
                  <div className='text-blue comparison-comment-content-top'>
                    <h3>Sensor de cámara</h3>
                    <div>
                      <span>4.3 &nbsp;&nbsp;</span>
                      <ReactStars
                        className='comparison-comment-stars'
                        count={5}
                        value={4}
                        size={15}
                        edit={false}
                        color2={'#00b5c8'} />
                    </div>
                  </div>
                  <div className='comparison-comment-content-text text-grey'>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </div>
                  <div className='text-grey comparison-comment-content-bottom'>
                    <span >¿Es una opinión útil?</span>
                    &nbsp;
                    <span>
                      <span>
                        4 &nbsp;<button className='gray-button' style={{display:'inline-block'}}>SI</button>
                      </span>
                      &nbsp;
                      &nbsp;
                      <span>
                        2 &nbsp;<button className='gray-button' style={{display:'inline-block'}}>NO</button>
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            ))
          }
        </div>
        <div className='comparison-comments-pagination'>
          <span className='text-grey'>Mostrando 1 - 10 de 10</span>

            <Pagination
              innerClass="pagination-inner"
              activeClass="pagination-active"
              activePage={1}
              itemsCountPerPage={10}
              totalItemsCount={450}
              pageRangeDisplayed={5}
            />
        </div>
      </div>)
  }
}

export default ComparisonComments;
