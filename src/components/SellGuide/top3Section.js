import React from 'react'

// Grid
import { Row, Col } from 'react-flexbox-grid'

//responsive component
//import MediaQuery from 'react-responsive'

// material-ui components
//import Divider from 'material-ui/Divider'
//import {lightBlue400, cyan700, blueGrey600, cyan500, grey300} from 'material-ui/styles/colors'
import Paper from 'material-ui/Paper'
//import Checkbox from 'material-ui/Checkbox';

// common components
import ToggleButtom from '../Buttons/toggle'
import RankingChart from '../RankingChart'
import PriceButton from '../Buttons/price'
import MediaQuery from 'react-responsive'
import uid from 'uid'

// json styles
/*
const checkboxStyles = {
  svg: {
    width: '18',
    marginTop: '0.8px',
    height:'18',
    fontWeight: 'normal',
    opacity: '0.7'
  },
  box: {
    width: '18',
    height:'18',
  },
  // this section should be depurate
  label: {
    fontWeight: 'normal',
    padding: '1px 10px',
  }
}*/

// stateless componentent
const Top3Section = (props) => (
  <Paper zDepth={0} className="relative sell-guide-top3" style={{minHeight: '300px'}}>
    <ToggleButtom mobile={props.mobile} className='sell-guide-toggle-top3' label1="Todos" label2="Top 3" tab={props.slideIndex} toggle={props.handleToggle} style={{position: 'absolute', top: '0px', left: '43px'}}/>
    { !props.mobile  && <h3 className="blue-title text-right"> <span className="icon-017-page-ranking" /> Ver Comparativa Completa >> </h3> }
    <MediaQuery query="(min-width: 760px)">
      {
          props.top3Products.map((product, index) => (
            <Row style={{margin: '0px'}} key={uid()}>
              <Col xs={12}>
                <div className='ResultCard list-view'>
                  <Row>
                    <Col xs={3}>
                      <img src={product.images[0]} alt='Producto?'/>
                    </Col>
                    <Col xs={3}>
                      <h3> { product.name } </h3>
                    </Col>
                    <Col xs={2}>
                      <Row center="xs">
                        <RankingChart ranking={product.ranking}/>
                        <Col xs={12}>
                          <p className="small-font text-grey margin-none"> Ranking </p>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={2}>
                      <Row center="xs">
                        <PriceButton price={product.price}/>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          ))
      }
    </MediaQuery>
    <MediaQuery query="(max-width:760px)">
      <div className='sg-top3-mobile'>
          {
            props.top3Products.map((product, index) => (
            <Paper className='sg-top3-content' key={uid()}>
              <div className='sg-top3-image'>
                <img src={product.images[0]} alt='Producto?'/>
              </div>
              <div className='sg-top3-info'>
                <h2>{product.name}</h2>
                <button><span>{product.price} €</span> | <span> Ver precios >></span></button>
              </div>
            </Paper>
          ))
        }
      </div>
    </MediaQuery>

    { props.mobile  && <h3 className="blue-title"> <span className="icon-017-page-ranking" /> Ver Comparativa Completa >> </h3> }
  </Paper>
)

export default Top3Section
