import {
  combineReducers
} from "redux"

import {  routerReducer } from 'react-router-redux'
import categories from "./modules/categories"
import product from "./modules/products"
import interactions from "./modules/interactions"
import appReducer from "./modules/app"

export default combineReducers({
  categories,
  product,
  routing: routerReducer,
  interactions,
  app: appReducer
})
