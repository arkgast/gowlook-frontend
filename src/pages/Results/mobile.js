import React, {Component} from 'react'
// Redux elements
import { connect } from 'react-redux'
//import {fetchProducts} from '../../reducers/actions/productActions'
import {selectProductDetails} from '../../reducers/actions/interactionActions'

//material ui components
//import {lightBlue200} from 'material-ui/styles/colors';
//import FontIcon from 'material-ui/FontIcon'
//import IconButton from 'material-ui/IconButton'
//import {Tabs, Tab} from 'material-ui/Tabs';

// SwipeableViews
import SwipeableViews from 'react-swipeable-views';

//grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// icons
//import Fire from 'material-ui/svg-icons/social/whatshot'

//page mobile components
import ItemSider from './ResultsFilter/mobile'

// common component
import MobileProduct from '../../components/Products/mobile'
import MobileOffer from '../../components/Products/mobileOffer'
import Top3Mobile from '../../components/Products/Top3Mobile'
import MobileSection from '../../components/SellGuide/mobile'
import BottomNavigation from '../../components/BottomNavigation'
import DetailsMobile from '../../components/Products/DetailsMobile';
import FilterBox from '../../components/filterBox/mobile'
import './styles.css'

// Mobile component for result view
class Mobile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openResultTab: 0,
      openDialogModal: false,
      openFilter: false,
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleOpenClick = this.handleOpenClick.bind(this)
    this.handleCloseClick = this.handleCloseClick.bind(this)
    this.showFilter = this.showFilter.bind(this)
    this.hideFilter = this.hideFilter.bind(this)
  }
  showFilter() {
    this.setState({openFilter: true})
  }
  hideFilter() {
    this.setState({openFilter: false})
  }
  handleChange(value) {
    this.setState({
      openResultTab: value,
    })
  }
/**
|-------------------------------------------------
|               handleOpenClick
|-------------------------------------------------
| propiedad padre se encarga de manejar el modal
| de la vista mobile de DetailsMobile.js
|-------------------------------------------------
*/
  handleOpenClick(product){
    this.setState({openDialogModal: true})
    this.props.selectProductDetails(product.id);
  }

/**
|-------------------------------------------------
|               handleCloseClick
|-------------------------------------------------
| propiedad padre se encarga de manejar el modal
| de la vista mobile de DetailsMobile.js
|-------------------------------------------------
*/
  handleCloseClick(){
    //this.state.openDialogModal = false;
    this.setState({OpenDialogModal: false});
    this.setState({OpenDialogModal: this.state.openDialogModal})
  }


  render() {
    const filters = [
      {
        id: 'type1',
        name: 'Tipo de productos',
        filterType: 'productTypes'

      },
      {
        id: 'brand1',
        name: 'Marcas',
        filterType: 'brandFilter'

      }
    ]
    const props = this.props
    return(
      <Grid fluid style={{
          borderTop: '1px solid #f1f1f1',
          backgroundColor: 'rgba(249, 249, 249, 0.67)',
          minHeight: '400px',
      }}>
        <SwipeableViews
          index={this.state.openResultTab}
          onChangeIndex={this.handleChange} >
          {/** Result tab **/}
          <div>
            <Row center="xs" className='results-container'>
              <Col xs={11} style={{marginTop: '20px', marginBottom: '10p'}}>
                <ItemSider onRightClick={this.showFilter} onLeftClick={this.showFilter}/>
              </Col>
              {
                this.props.products.map((item, value) => (
                  <Col xs={11} className='results-rows'>
                    <MobileProduct
                      {...item}
                      checkboxAction={this.props.checkboxAction}
                      unCheckAction={this.props.unCheckAction}
                      isChecked={props.comparativeProducts.filter(comparate => comparate.id === item.id).length > 0 }
                      customText={
                        ( ( props.comparativeProducts.filter(comparate => comparate.id === item.id).length > 0 ) && props.comparativeProducts.length > 1 )
                            && "Comparar ya" }
                      img={item.images[0]}
                      handleOpenClick={this.handleOpenClick}/>
                  </Col>
                ))
              }
            </Row>
          </div>
          {/** SellGuide tab **/}
          <div>
            <MobileSection top3Products={this.props.products.slice(0, 3)}/>
          </div>
          {/** top 3 tab **/}
          <div>
            <br />
            <Row>
              <Col xs={1}></Col>
              <Col xs={11}>
                <ItemSider onRightClick={this.showFilter} onLeftClick={this.showFilter}/>
              </Col>
            </Row>
            <Row center="xs">

              <Col xs={1} className='t3m-leftbar'>
                <ul className='t3m-leftbuttons'>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                  <li><span></span></li>
                </ul>
              </Col>
              <Col xs={11}>
                <Col xs={8}>
                  <h3 className="blue-title text-left">
                    Megapíxeles
                  </h3>

                </Col>
                {
                  this.props.products.slice(0, 3).map((item, value) => (
                    <Col xs={12}>
                      <Top3Mobile {...item} img={item.images[0]} percent={value * 15}/>
                    </Col>
                  ))
                }
              </Col>
            </Row>
          </div>
          {/** offer tab **/}
          <div>
            <Row center="xs" style={{overflow: 'hidden'}}>
              <Col xs={11} style={{marginTop: '20px', marginBottom: '10p'}}>
                <ItemSider onRightClick={this.showFilter}/>
              </Col>
              {
                this.props.products.map((item, value) => (
                  <Col xs={11}>
                    <MobileOffer {...item} img={item.images[0]}/>
                  </Col>
                ))
              }
            </Row>
          </div>
        </SwipeableViews>
        {
          /**
          |-------------------------------
          |         DetailsMobile
          |-------------------------------
          */
        }
        <DetailsMobile
          handleOpenClick={this.handleOpenClick}
          handleCloseClick={this.handleCloseClick}
          handleChange={this.handleChange}
          handleIndex={this.state.openDialogModal}
          product={this.props.selectedProduct}
          slideIndex={this.state.openResultTab}
        />
        <BottomNavigation handleChange={this.handleChange} slideIndex={this.state.openResultTab}/>
        <FilterBox
          filters={filters}
          brandFilter={this.props.brands}
          productTypes={this.props.productTypes}
          handleClose={this.hideFilter}
          open={this.state.openFilter}/>
      </Grid>

    )
  }
}
/**
|-------------------------------
|         redux
|-------------------------------
*/
// map states from reducer store
const mapStates = (states) => ({
  selectedProduct: states.product.productModal,
  productTypes: states.product.productTypeDTOS,
  brands: states.product.brandDTOS,
})

// set function with dispatch you cant trigger this reducers actions with props.yourAction()
const mapDispatch = {
  selectProductDetails,
}
export default connect(mapStates,mapDispatch)(Mobile)
