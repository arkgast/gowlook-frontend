import React, { Component } from 'react';

// material ui Components
import RaisedButton from 'material-ui/RaisedButton'
import AutoComplete from 'material-ui/AutoComplete'
//material ui color palette
import {
  fullWhite
} from 'material-ui/styles/colors'

// icons
import Search from 'material-ui/svg-icons/action/search'
import './InputSearch.css'

//component
class InputSearch extends Component {
  constructor(props) {
    super(props)
    this.state=  {
      dataSource: [
        {
          text:'Canon 55oD III Mark Canon Mark',
          value: 1
        },
        {
          text: 'Canon 55oD III Mark Canon Mark',
          value: 2
        },
        {
          text: 'Canon 55oD III Mark Canon Mark',
          value: 3
        },
        {
          text: 'Canon 55oD III Mark Canon Mark',
          value: 4
        },
        {
          text: 'Canon 55oD III Mark Canon Mark',
          value: 5
        },

      ]
    }
  }
  toggleFocus(value) {
    this.setState(() => ( {focus: value} ))
  }
  render() {
    return (
      <div
        className="flex-center input-search"
        style={{
          marginTop: '2%'
        }}>
        <div style={{ width: '80%'}}>
          <AutoComplete
              hintText="¿Que quieres comparar?"
              dataSource={this.state.dataSource}
              fullWidth={true}
            />
          </div>
          <div className="button-search">
            <RaisedButton
              fullWidth
              primary
              icon={<Search color={fullWhite}/>}/>
          </div>
        </div>
    )
  }
}
export default InputSearch
