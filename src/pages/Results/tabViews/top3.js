import React from 'react';

import { List, ListItem } from 'material-ui/List'

// common components
import FilProducts from '../../../components/Products/ResultsProducts'
import Top3Chart from '../../../components/top3Chart'
import Checkbox from 'material-ui/Checkbox'
import Slider from '../../../components/Slider'

import uid from 'uid'

import './top3.css'

class Top3 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open2: false,
      open3: false,
      open4: false,
    }
  }

  handleNestedListToggle(key) {
    this.setState({[ `open${key}` ]: !this.state[ `open${key}` ]})
  }

  render () {
    return (<div>
      <div style={{marginTop: '30px', display: 'flex'}}>
        <div style={{width: '25%'}}>
          <List>
            <ListItem
              key={2}
              primaryText="Tipo de producto"
              open={this.state.open2}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 2)}
              style={{fontSize: '1.3rem', color: '#979797'}}
              nestedItems={
                [{name: 'Tabled', id: '1'}].map(( item, index ) => (
                  <ListItem
                    key={uid()}
                    primaryText={item.name}
                    innerDivStyle={{padding: '16px 16px 16px 50px'}}
                    style={{ marginTop: '-15px'}}
                    leftCheckbox={
                      <Checkbox
                        value={item.id}
                      />
                    }
                  />
                ))
              }
            />
            <ListItem
              key={3}
              primaryText="Marcas"
              open={this.state.open3}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 3)}
              style={{fontSize: '1.3rem', color: '#979797'}}
              nestedItems={
                [{name: 'Samsung', id: 1}, {name: 'Amazon', id: 2}, {name: 'Lenovo', id: 3},{name: 'Samsung', id: 4}, {name: 'Amazon', id: 5}, {name: 'Lenovo', id: 6}].slice(0, 7).map((item, index) => (
                  <ListItem
                    key={uid()}
                    className="blue-hover"
                    id={index === 6 && "menu"}
                    hoverColor="white"
                    primaryText={index !== 6 ? item.name : undefined}
                    innerDivStyle={{padding: '16px 16px 16px 50px'}}
                    style={{
                      marginTop: '-15px',
                      textAlign: index !== 6 ? 'left' : "right"
                    }}
                    leftCheckbox={
                      index !== 6 &&
                      <Checkbox
                        value={item.id}
                        className="blue-hover"

                      />
                    }
                  >
                    {index === 6 && <span className="hover-border-non-padding"> Ver mas </span>}
                  </ListItem>
                ))
              }
            />
            <ListItem
              key={4}
              primaryText="Precio"
              open={this.state.open4}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 4)}
              style={{fontSize: '1.3rem', color: '#979797'}}
              nestedListStyle={{padding: '8px 30px'}}
              nestedItems={[
                 <Slider style={{width: '80%'}} key={uid()} />
              ]}
            />
          </List>
        </div>
        <div style={{width: '75%', display:'flex'}}>
            {this.props.products.map((item, index) => (
              <div
                style={{width: '40%'}} key={uid()}>
                <FilProducts img={item.images[0]} product={item} viewType={this.props.viewType} />
              </div>
            ))}
        </div>
      </div>
      <div className="margin-section" style={{display:'flex'}}>
        <div style={{width: '25%'}} className='top3-filter'>
          <ul>
            <li>Megapixeles</li>
            <li>Exposure</li>
            <li>ISO</li>
            <li>Memory</li>
            <li>Anatomy</li>
            <li>Zoom</li>
            <li>Focus</li>
          </ul>
        </div>
        <div style={{width: '75%'}}>
          <Top3Chart />
        </div>
      </div>
    </div>)
  }
}


export default Top3
