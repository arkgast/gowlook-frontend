import React , {Component} from 'react'

// material ui components
import Checkbox from 'material-ui/Checkbox'
import Paper from 'material-ui/Paper'
import {grey200, grey300, cyan500, fullWhite} from 'material-ui/styles/colors'

// react router addons
import { withRouter } from 'react-router'

// json Styles
const styles = {
  container: {
    height: '200px',
    border: '1px solid ' + grey200 ,
    marginTop: '10px'
  },
  checkboxStyles: {
    svg: {
      width: '18',
      marginTop: '0.8px',
      height:'18',
      fontWeight: 'normal',
      opacity: '0.7'
    },
    box: {
      width: '18',
      height:'18',
    },
    label: {
      fontWeight: 'normal',
    }
  },
  productImgStyles: {
    maxWidth: '100%',
    height: '100px',
    marginTop: '30px'
  }
}
// image
import speedmeter from '../../assets/img/speedmeter.jpg'
//grid
import { Row, Col } from 'react-flexbox-grid'

// common componets
import RankingChart from '../../components/RankingChart'
import PriceButton from '../../components/mobileButtons/price'



class MobileProduct extends Component  {
  constructor() {
    super()
    this.state = {
      openFilter: false,
      checked: false,
      hover: false,
    }
    this.onCheck = this.onCheck.bind(this)
    this.redirect = this.redirect.bind(this)
  }
  componentWillReceiveProps(nextprops) {
    this.setState({
      checked: nextprops.isChecked
    })
  }
  onCheck() {
    if(this.state.checked) {
      this.props.unCheckAction(this.props.id)
    } else {
      this.props.checkboxAction(this.props)
    }
    this.setState(() => ({ checked: !this.state.checked }))
  }
  toggleHover(value) {
    this.setState({hover: value})
  }
  redirect() {
    this.props.history.push(`/Sheet/${this.props.id}`)
  }
  render() {
    const props = this.props;
    const checkboxStyles = {
      svg: {
        width: '18',
        marginTop: '0.8px',
        height:'18',
        fontWeight: 'normal',
        opacity: '0.7'
      },
      box: {
        width: '18',
        height:'18',
      },
      // this section should be depurate
      label: {
        fontWeight: 'normal',
        padding: '3px 10px',
        whiteSpace: 'nowrap',
        color: this.state.checked ? cyan500 : grey300,
      }
    }
    return (
      <Paper zDepth={0} style={styles.container}>
        <Row style={{height: '100%'}}>
          <Col xs={5} style={{height: "100%"}}>
            <Row center="xs">
              <Col xs={11}>
                <img src={props.img} alt=" " style={styles.productImgStyles}/>
              </Col>
            </Row>
            <Row center="xs">
              <Col xs={7} style={{marginTop: '30px'}}>
                {
                  this.props.checkboxAction &&
                  <div
                    style={{
                      border: this.state.checked ? '1px solid ' + cyan500 : 'none',
                      backgroundColor: ( this.state.hover && this.props.customText ) ? cyan500 : fullWhite,
                      display: 'flex',
                      width: !this.state.checked ? '135px' : '145px'
                    }}>
                      <Checkbox
                        className={( this.state.hover && this.props.customText ) && "force-checked-color" }
                        onCheck={this.onCheck}
                        checked={this.state.checked || this.props.isChecked}
                        style={checkboxStyles.box}
                        labelStyle={checkboxStyles.label}
                        iconStyle={checkboxStyles.svg}
                      />
                      <span
                        style={{
                          ...checkboxStyles.label,
                          color: ( ( this.state.hover && this.props.customText ) && fullWhite) || ( this.state.checked && cyan500 ) || grey300 ,
                        }}
                        onClick={() => this.props.customText && this.props.history.push("/comparison")}
                        onMouseOver={this.toggleHover.bind(this, true)}
                        onMouseLeave={this.toggleHover.bind(this, false)} >
                        {!this.props.customText ? "Comparar" : this.props.customText }
                      </span>
                    </div>
                }
              </Col>
            </Row>
          </Col>
          <Col xs={7}>
            <h4 className="text-grey text-left">
              { props.name }
            </h4>
            <Row end="xs">
              <Col xs={6}>
                <Row center="xs">
                    <Col xs={12}>
                      <RankingChart ranking={props.ranking}/><br/>
                    </Col>
                    <Col xs={12}>
                      <p className="full-width text-left small-font" style={{margin: '0px'}}>Ranking</p>
                    </Col>
                </Row>
              </Col>
              <Col xs={6}>
                <Row center="xs">
                  <img src={speedmeter} alt="" style={{maxWidth: '100%'}}/>
                </Row>
                <p className="full-width text-center small-font" style={{margin: '0px'}}>Reputacion</p>
              </Col>
              <Col xs={11} sm={6}>
                <PriceButton handleOpenClick={props.handleOpenClick} productSelected={props} price={props.price}/>
              </Col>
            </Row>
          </Col>
        </Row>
      </Paper>
    )
  }
}

export default withRouter( MobileProduct )
