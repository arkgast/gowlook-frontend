import React from 'react'
const content = [
  {
    title: '1.Eficiencia energética: en una sola etiqueta tendrás toda la información',
    content: <p>
              <p>Si quieres cuidar tu factura de la luz, uno de los primeros pasos que deberás tener en cuenta es el hacerte con elementos electrónicos que consuman poca energía en el hogar: ordenadores, televisores, pequeños electrodomésticos, y por supuesto, los grandes electrodomésticos, unos auténticos devoradores de energía eléctrica.</p>

              <p>Las lavadoras son unos equipos que suelen usarse muy a menudo en un hogar, por lo tanto, lo primero que deberás fijarte a la hora de adquirir una nueva es en la etiqueta energética que acompaña al equipo. Debes procurar que la lavadora pertenezca a la categoría A de eficiencia o a alguna de sus variantes (A +, A ++ ó A +++). </p>

              <p>Asimismo, esta etiqueta —obligatoria— también suele indicar cuál es el consumo anual del equipo al que acompaña, por lo tanto, si por ejemplo tienes contratada una tarifa plana de luz, es importante que cuides tu consumo anual expresado en kWh.</p>
    </p>
  },
  {
    title: '2.	Carga de ropa: cuantos más miembros en la familia, mayor debe ser la carga máxima',
    content: <p>
      <p>Parecía fácil la elección de una nueva lavadora, ¿verdad? Pues un dato muy importante que tienes que contemplar en tu decisión es la carga máxima de ropa que admitirá el equipo.</p>

      <p>Las lavadoras pueden admitir cargas que comienzan en los cuatro kilogramos y llegar a los nueve kilogramos. Ahora bien, ¿cuál es el modelo que más te conviene? Tienes que guiarte por el número de personas que viven en tu hogar y cuál es la asiduidad con la que lavas la ropa a lo largo de la semana. Si quieres una explicación rápida de cómo decidirte rápido, te podemos decir que las lavadoras que tienen una carga máxima de 4 kilogramos están pensadas para ser usadas por solteros o parejas sin hijos ni nadie más a su cargo.</p>

      <p>Si subimos un peldaño más, el siguiente nivel de lavadoras va desde los cinco a los siete kilogramos de carga máxima. Quizás en este rango encontraríamos el estándar del mercado y es que son modelos que sirven para lavar un volumen de ropa equivalente a cuatro personas.</p>

      <p>En cambio, si estamos hablando que perteneces a una familia numerosa (más de 4 integrantes) o que sueles hacer lavadoras a menudo con piezas de gran tamaño, existen modelos de hasta 9 kilogramos de carga máxima. Ojo, porque estos modelos suelen conllevar un precio más elevado.</p>

    </p>
  },
  {
    title: '3.	Garantía: cuantos más años, mejor para ti',
    content: <p>
      <p>Las lavadoras suelen tener un uso muy continuado en nuestro día a día. Es por ello, que esto puede llevar a un mayor desgaste de las piezas que en otros sectores de la electrónica de consumo. Asimismo, en los primeros años, es posible que se tengan algunos malos funcionamientos por piezas en mal estado, etc.</p>

      <p>Es en estos casos cuando la garantía entra en juego y cuanto más meses tengamos disponibles, mucho mejor para nuestro bolsillo. Por ley, los fabricantes deben darte dos años de garantía en todos los productos, pero existen casos en lo que la propia marca facilita un periodo más extenso sin tener que pagar nada extra.</p>

      <p>Es en estos casos cuando podrás comprobar hasta qué nivel las marcas confían en sus productos y favorecen que esa opinión se traslade a sus clientes. Es decir, cuantos más meses tengas de garantía, mucho mejor. Pero como siempre avisamos: los malos usos no son cubiertos por la garantía.  </p>

    </p>
  },
  {
    title: '4.  Funciones: evita desempeños inútiles que únicamente encarecen el precio del equipo',
    content: <p>
      <p>
        Parece mentira, pero el desarrollo de las lavadoras de última generación contemplan tecnologías como el WiFi y que puedan ser controladas por el móvil o la tablet, a través del uso de una aplicación. ¿Realmente son necesarias estas funciones?</p>

      <p>Desde un punto de vista de innovación, no cabe duda de que es un gran adelanto y que seguramente en algunos casos pueda que les sea útil. Ahora bien, estas funciones lo único que harán es encarecer el precio de venta del equipo y no aportarán valor al uso diario.</p>

        <p>Por lo tanto, en Gowlook te recomendamos que apuestes por lavadoras que tengan programas de lavado para cualquier tipo de ropa. Y que como extra, si tu economía lo permite, que disponga de función rápida (entre 30 y 40 minutos de lavado, únicamente) que hará que puedas ahorrar tanto en energía como en gasto de agua.</p>

    </p>
  },
  {
    title: '5.	Diseño: elige entre carga frontal o carga superior',
    content: <p>
      <p>El diseño de una lavadora también dependerá de gustos y es que el mercado existen dos tipos de diseño: la carga frontal —la más extendida— y la carga superior. Ambos modelos presentan ventajas e inconvenientes y es interesante que los conozcan para hacerte una mejor idea de qué es lo que estás buscando.</p>

        <p>En las lavadoras de carga superior meter ropa será mucho más fácil y natural que si lo hacemos en una lavadora de carga frontal. Además, es posible añadir ropa en los ciclos de lavado  y son de un tamaño más menudo, ideales para hogares con poco espacio. Sin embargo, también es cierto que el uso de agua es mayor porque necesitan llenar todo el bombo de agua, así como también hacen un uso más generoso de detergente.</p>

        <p>En cambio, las lavadoras de carga frontal, aunque son de mayor tamaño y tienen una posición de carga menos natural, suelen ser modelos que equipan la última tecnología. Por lo tanto, ¿qué puedes ganar con estos modelos de lavadoras? Un equipo más eficiente con el gasto de agua, detergente y de energía. Y si esto te parece poco, también suelen ser más silenciosas, un dato muy a tener en cuenta si eres de los que por tiempo necesita lavar la ropa por la noche y no quiere tener problemas con los vecinos de la comunidad.</p>

    </p>
  },
]

export default content
