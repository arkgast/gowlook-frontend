import React from 'react'
import PropTypes from 'prop-types'

const SocialNetworks = (props) => {
  return (
    <div className={props.className || ''}>
      <a href="https://twitter.com/gowlook" target="_blank">
        <span className={`fa-stack ${props.size || ''} small-margin twitter-hover`}>
          <i className="fa fa-twitter fa-stack-1x border-circle"></i>
        </span>
      </a>
      <a href="https://www.linkedin.com/company/gowlook" target="_blank">
        <span className={`fa-stack ${props.size || ''} small-margin linkedin-hover`}>
          <i className="fa fa-linkedin fa-stack-1x border-circle"></i>
        </span>
      </a>
      <a href="https://www.facebook.com/Gowlook-534469503388904/?skip_nax_wizard=true" target="_blank">
        <span className={`fa-stack ${props.size || ''} small-margin facebook-hover`}>
          <i className="fa fa-facebook fa-stack-1x border-circle"></i>
        </span>
      </a>
     <a href="https://plus.google.com/+Gowlook" target="_blank">
        <span className={`fa-stack ${props.size || ''} small-margin google-hover`}>
          <i className="fa fa-google-plus fa-stack-1x border-circle"></i>
        </span>
      </a>
    </div>
  )
}


SocialNetworks.propTypes = {
  size: PropTypes.string
}

export default SocialNetworks
