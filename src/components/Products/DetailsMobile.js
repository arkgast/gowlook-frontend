/**
|---------------------------------------------------
|                 DEPENDENCIAS
|---------------------------------------------------
*/
import React, { Component, /*PropTypes*/ } from 'react';
/**
|---------------------------------------------------
|                 DEPENDENCIAS  material-ui
|---------------------------------------------------
*/

//components
import Dialog from 'material-ui/Dialog'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
//import Popover from 'material-ui/Popover';
//import Menu from 'material-ui/Menu'
import FlatButton from 'material-ui/FlatButton'
import MenuItem from 'material-ui/MenuItem'
//import RaisedButton from 'material-ui/RaisedButton'
import DropDownMenu from 'material-ui/DropDownMenu';
import {GridList} from 'material-ui/GridList';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

//paleta de colores
import {lightBlue200,grey200/*,cyan400*/} from 'material-ui/styles/colors';
//iconos
//import Cross from 'material-ui/svg-icons/navigation/close'
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right'
import Arrow from 'material-ui/svg-icons/navigation/arrow-upward'
//charts
//import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
//pagination
import ReactPaginate from 'react-paginate';
//tabs
import {Tabs, Tab} from 'material-ui/Tabs'
/**
|---------------------------------------------------
|                 ASSETS
|---------------------------------------------------
*/
// common componets
import HorizontalChart from '../horizontalChart'
import RankingChart from '../RankingChart/RankingChartDetail'
//import SimpleCard from '../SimpleCard'
import BuyButton from '../../components/mobileButtons/buy'
// helpers
import chartOptions from '../../helpers/chartOptions'

// assets img
//import map from '../../assets/img/map.jpg'
//import cloud from '../../assets/img/cloud.jpg'
import device from '../../assets/img/original-resultados_de_productos/ListaProductos_03.jpg'
import amazon from '../../assets/img/amazon.png'
/**
|---------------------------------------------------
|                 ESTILOS
|---------------------------------------------------
*/
import './styles/Details.css'

const styles ={
  titulos:{
    color: '#959595'
  },
  chevrons:{
    color: '#F1F1F1'
  },

}

//------- Styles Json---------
const labelStyles = {
  textTransform: 'capitalize',
}
const inkBarStyle = {
  backgroundColor: lightBlue200,
  height: '3px',
}
const buttonStyles = {
  flexDirection: 'row',
  height: '55px',
  width: 'auto',
}
const tabsStyles = {
  width: '300px',
  boxSizing: 'border-box',
}
/**
|---------------------------------------------------
|                 DEPENDENCIAS  node-modules
|---------------------------------------------------
*/
import {/*Grid,*/ Row, Col} from 'react-flexbox-grid';
import SwipeableViews from 'react-swipeable-views';

/**
|---------------------------------------------------
|                 hard data auxiliar
|---------------------------------------------------
*/

const dataProduct = {
  name: 'Sony DSC Hx-400 Camera-new',
  images: device,
  ranking: '43.1853338962593',
  reputation: '44.6567283558584',
  price: '49',
  prices: [
    {price: '90',image: amazon},
    {price: '80',image: amazon},
    {price: '70',image: amazon},
    {price: '45',image: amazon},
  ]
};

/*
const data = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];
*/
const LineChart2 = require("react-chartjs-2").Line
const chartData = {
    labels: ["2012 Q1", "2012 Q2", "2012 Q3", "2012 Q3", "2012 Q4", "2012 Q5", "2012 Q6"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(0, 185, 205, 0.3)",
            borderColor: "rgba(0, 185, 205,1)",
            pointBorderColor: "rgba(0, 185, 205,1)",
            pointBorderWidth: 5,
            pointHoverRadius: 5,
            //pointStrokeColor: "#fff",
            //pointHighlightFill: "#fff",
            //pointHighlightStroke: "rgba(220,220,220,1)",
            data: [10, 7,6, 5, 4, 3, 3],
            fill: false
        }
    ]
}


/**
|---------------------------------------------------
|                 asset component
|---------------------------------------------------
*/
class MobileDetails extends Component {

  /**
  |---------------------------------------------------
  |                 constructor
  |---------------------------------------------------
  */
  constructor(props){
  	super(props);
  	this.state = {
      value: 1,
      activePage:3,
      pageCount:3
    };
    //borrar este console depues de finalizar
    console.log(props);


    this.handleDropChange = this.handleDropChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }
  /**
  |---------------------------------------------------
  |                 funcion- handleChange
  |---------------------------------------------------
  */
    handleDropChange(event,index,value) {
      this.this.setState({value})
    }
  /**
  |---------------------------------------------------
  |                 funcion- handlePageChange
  |---------------------------------------------------
  */
    handlePageChange(pageNumber) {
      console.log(`active page is ${pageNumber}`);
      this.setState({activePage: pageNumber});
    }

  render() {
    return (
      <Dialog
        open={this.props.handleIndex}
        onRequestClose={this.props.handleCloseClick}
        contentStyle={{width: '90%'}}
        autoScrollBodyContent
        >
          {/**--------------Tabs----------------------------------*/}
          <Tabs
            onChange={this.props.handleChange}
            style={{borderBottom: '1px solid ' + grey200}}
            inkBarStyle={inkBarStyle}
            tabItemContainerStyle={tabsStyles}
            value={this.props.slideIndex} >
            <Tab
              icon={
                <FontIcon className="icon-031-cash tab-icon"/>
              }
              style={labelStyles}
              buttonStyle={buttonStyles}
              label="Precios"
              value={0} />
            <Tab
              icon={
                <FontIcon className="icon-017-page-ranking tab-icon"/>
              }
              style={labelStyles}
              buttonStyle={buttonStyles}
              label="Ranking"
              value={1} />
            <Tab
              icon={
                <FontIcon className="icon-032-like tab-icon"/>
              }
              style={labelStyles}
              buttonStyle={buttonStyles}
              label="Reputación"
              value={2} />
          </Tabs>




          {/**--------------Product detail----------------------------------*/}
          <Row>
            <Col xs={6}>
              <img
                style={{maxWidth: '100%', height: '100px'}}
                src={this.props.product.images && this.props.product.images[0]}
                alt={this.props.product.name}
              />
            </Col>
            <Col xs={6}>
              <h3>{this.props.product.name}</h3>
            </Col>
          </Row>


          {/**------------------DROPDOWN----------------------------------*/}
          <Row>
            <DropDownMenu style={{width: '100%',}} value={this.state.value} onChange={this.handleDropChange}>
              <MenuItem value={1} primaryText="3gb Memory" />
              <MenuItem value={2} primaryText="6gb Memory" />
              <MenuItem value={3} primaryText="8gb Memory" />
              <MenuItem value={4} primaryText="12gb Memory" />
              <MenuItem value={5} primaryText="16gb Memory" />
            </DropDownMenu>
          </Row>

          {/**------------------SwipeableViews----------------------------------*/}
          <SwipeableViews
            index={this.props.slideIndex}
            onChangeIndex={this.props.handleChange} >

              {
                /**
                |-----------------------------------------------------------------
                |                 PRICES LIST
                |-----------------------------------------------------------------
                */
              }

            <div className="display">

              {/**------------------LineChart2----------------------------------*/}
              <Row center="xs" style={{marginTop:'10px'}}>
                <Col xs={3}>
                <IconButton
                    iconStyle={{width:'48px', height:'48px'}}
                    style={{marginTop: '50px',padding:'0px',width:'56px',height: '56px'}}
                  >
                  <ChevronLeft/>
                </IconButton>
                </Col>
                <Col xs={6}>
                  <LineChart2 data={chartData} options={chartOptions} width={100}
                   height={80}/>
                </Col>
                <Col xs={3}>
                  <IconButton
                    iconStyle={{width:'48px', height:'48px'}}
                    style={{marginTop: '50px',padding:'0px',width:'56px',height: '56px'}}
                    >
                    <ChevronRight/>
                  </IconButton>
                </Col>
              </Row>


              {/**------------------ordenar por----------------------------------*/}
              <Row end="xs">
                <Col xs>
                  <h4 style={styles.titulos}>ordernar por:</h4>
                </Col>
                <Col xs>
                  <DropDownMenu style={{width: '100%',color: '#959595'}} value={this.state.value} onChange={this.handleDropChange}>
                    <MenuItem value={1} primaryText="precio" />
                    <MenuItem value={2} primaryText="memoria" />
                    <MenuItem value={3} primaryText="precio" />
                  </DropDownMenu>
                </Col>
              </Row>

              {/**------------------lista de precios----------------------------------*/}
              <GridList
                  cellHeight={50}
                  style={{width:'100%', height:'300px',marginTop: '5px',overflowT:'auto'}}
                >
                {
                  this.props.product.historyPrices &&
                    this.props.product.historyPrices.map.map((item,key) =>(
                    <div key={key}>
                        <Row center='xs'>
                          <h3 style={{color: '#1DB7CC'}}>{item.price} €</h3>
                        </Row>
                        <Row center='xs'>
                          <img style={{width:'60px', height:'15px'}} src={item.providerResponse.logo} alt={item.providerResponse.name}/>
                        </Row>
                        <Row center='xs'><BuyButton style={{marginTop: '5px'}}/></Row>
                    </div>
                  ))
                }
              </GridList>
              {/**------------------pagination----------------------------------*/}
              <Row center="xs">
                <Col xs={2}>
                  <p>mostrar:</p>
                </Col>
                <Col xs={4}>
                  <DropDownMenu style={{width: '100%',color: '#959595'}} value={this.state.value} onChange={this.handleDropChange}>
                    <MenuItem value={1} primaryText="12" />
                    <MenuItem value={2} primaryText="3" />
                    <MenuItem value={3} primaryText="34" />
                  </DropDownMenu>
                </Col>
                <Col xs={6}>
                  <ReactPaginate
                       previousLabel={"<"}
                       nextLabel={">"}
                       pageCount={this.state.pageCount}
                       marginPagesDisplayed={0}
                       pageRangeDisplayed={3}
                       onPageChange={this.handlePageChange}
                       containerClassName={"pagination"}
                       subContainerClassName={"pages-pagination"}
                       activeClassName={"activePage"}
                       /**
                        * activeClassName={"active"}
                        */
                        />
                </Col>
              </Row>


              {/**------------------alarm button----------------------------------*/}

              <FlatButton
                primary
                style={{color:'#fff'}}
                className="alarm"
                labelStyle={{textTransform: 'capitalize',color:'#fff',fontSize:'16px'}}
                fullWidth
                icon={<FontIcon className="icon-018-turn-notifications-on-button small-icon"/>}
                label="Alarm" />
            </div>
            {
              /**
              |-----------------------------------------------------------------
              |                 RANKING
              |-----------------------------------------------------------------
              */
            }
            <div className="display">
              {/**------------------RankingChart----------------------------------*/}
              <Row center="xs" style={{marginTop:'10px'}}>
                <RankingChart ranking={dataProduct.ranking} />
              </Row>
              {/**------------------HorizontalChart----------------------------------*/}
              <Row style={{margin: '0px'}}>
                <Col xs={12}>
                  <p>Camera</p>
                  <HorizontalChart percent={30}/>
                  <h5>12 Mph</h5>
                  <HorizontalChart percent={78} grey/>
                  <h5>34 Mph</h5>
                </Col>
                <Col xs={12}>
                  <p>Exposure</p>
                  <HorizontalChart percent={30}/>
                  <h5>12 Mph</h5>
                  <HorizontalChart percent={78} grey/>
                  <h5>34 Mph</h5>
                </Col>
                <Col xs={12}>
                  <p>ISO</p>
                  <HorizontalChart percent={30}/>
                  <h5>12 Mph</h5>
                  <HorizontalChart percent={78} grey/>
                  <h5>34 Mph</h5>
                </Col>
                <Col xs={12}>
                  <p>Memory</p>
                  <HorizontalChart percent={30}/>
                  <h5>12 Mph</h5>
                  <HorizontalChart percent={78} grey/>
                  <h5>34 Mph</h5>
                </Col>
              </Row>
              {/**------------------radio button----------------------------------*/}
              <RadioButtonGroup name="shipSpeed" defaultSelected="light" style={{width: '200px',display:'flex', fontSize: '14px'}}>
                <RadioButton
                  value="light"
                  label={dataProduct.name}
                  style={styles.radioButton}
                />
                <RadioButton
                  value="not_light"
                  label="All Cameras"
                  style={styles.radioButton}
                />
              </RadioButtonGroup>
              {/**------------------alarm button----------------------------------*/}

              <FlatButton
                primary
                style={{color:'#fff'}}
                className="alarm"
                labelStyle={{textTransform: 'capitalize',color:'#fff',fontSize:'16px'}}
                fullWidth
                icon={<FontIcon className="icon-018-turn-notifications-on-button small-icon"/>}
                label="Alarm" />
            </div>





            {
              /**
              |-----------------------------------------------------------------
              |                 REPUTACION
              |-----------------------------------------------------------------
              */
            }
            <div className="display">
              <Row center="xs">
                    {/**------------------chevron left----------------------------------*/}
                <Col xs={2}>
                  <IconButton
                      iconStyle={{width:'48px', height:'48px'}}
                      style={{marginTop: '50px',padding:'0px',width:'56px',height: '56px'}}
                    >
                    <ChevronLeft/>
                  </IconButton>
                </Col>

                  {/**------------------reputacion detail----------------------------------*/}
                <Col xs={8}>
                  <div>
                    <Row center="xs">
                      <Col xs={12}>
                        <div className="block">
                          <h4>Menciones del producto</h4>
                          <p>ultimos 30 dias</p>
                        </div>
                      </Col>
                      <Col xs={12}>
                        <div className="inline_block">
                          <div className="block">
                            <h2>{dataProduct.reputation.substring(0,7)}</h2>
                          </div>
                          <div className="block">
                            <h3 className="percentage"><span><Arrow style={{color: '#00B6CC'}}/></span> + 20%</h3>
                          </div>
                        </div>

                      </Col>
                    </Row>
                  </div>
                </Col>

                  {/**------------------chevron right----------------------------------*/}
                <Col xs={2}>
                  <IconButton
                      iconStyle={{width:'48px', height:'48px'}}
                      style={{marginTop: '50px',padding:'0px',width:'56px',height: '56px'}}
                    >
                    <ChevronRight/>
                  </IconButton>
                </Col>
              </Row>





              {/**------------------alarm button----------------------------------*/}

              <FlatButton
                primary
                style={{color:'#fff'}}
                className="alarm"
                labelStyle={{textTransform: 'capitalize',color:'#fff',fontSize:'16px'}}
                fullWidth
                icon={<FontIcon className="icon-018-turn-notifications-on-button small-icon"/>}
                label="Alarm" />
            </div>
          </SwipeableViews>
      </Dialog>
    );
  }
}




/**
|---------------------------------------------------
|                 export
|---------------------------------------------------
*/
export default MobileDetails;
