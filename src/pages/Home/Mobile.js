import React from 'react';
// material ui grid
import {
  GridList,
  GridTile
} from 'material-ui/GridList';

//material ui components
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import {
  grey400
} from 'material-ui/styles/colors'

//assets
import background from '../../assets/img/buhofondo.png'
import img1 from '../../assets/img/featured1.redimensionado.jpg';
import img2 from '../../assets/img/featured2.jpg';
import img3 from '../../assets/img/featured3.jpg';
import img4 from '../../assets/img/featured4.jpg';
import img5 from '../../assets/img/featured5.jpg';

// common components

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
  },
};
const tilesData = [
  {
    img: img1,
    title: 'videocámaras',
  },
  {
    img: img2,
    title: 'videocámaras',
  },
  {
    img: img3,
    title: 'Objetivos',
  },
  {
    img: img4,
    title: 'Visores Ópticos',
  },
  {
    img: img5,
    title: 'Marcos Digitales',
    featured: true,
  }
];
const MobileHome = ({ categories }) => (
  <div>
    <div className="relative">
      <img src={background} alt=" " style={{height: "200px", width: "100%", marginTop: "-24px"}}/>
      <div className="search-widget">
      </div>
    </div>
   <GridList
        cellHeight={180}
        style={styles.gridList}
      >
      <Subheader><h4 style={{color: grey400, fontSize: '18px', margin: '0px' }}>Categorías más Populares</h4></Subheader>
        {tilesData.map((tile) => (
            <GridTile
              key={tile.img}
              title={tile.title}
              cols={tile.featured ? 2 : 1}
              rows={1}
              actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
            >
              <img src={tile.img} alt=" " />
            </GridTile>
        ))}
    </GridList>
  </div>
);

export default MobileHome;
