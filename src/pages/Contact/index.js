import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppActions from '../../reducers/actions/appActions'
import { Col, Row } from 'react-flexbox-grid'
import logoContacto from '../../assets/img/logo_interrogacion.svg'
import './style.css'

class Contact extends React.Component {

    constructor(props) {
      super(props);

    }

    componentWillMount () {
      this.props.actions.toggleFooterHistories(false)
      this.props.actions.toggleFooterGow(false)
      this.props.actions.toggleFooterRegister(false)
    }

  render () {
    return (
      <section className='contact container'>
        <Row>
          <Col xs={12} sm={4} className='logo-container'>
            <img src={logoContacto} alt='Logo contacto' />
          </Col>
          <Col sm={8}>
            <Row>
              <h3 className='text-blue'>Contacta con nosotros</h3>
              <h4 className='text-grey'>Usa nuestro formulario para comentar lo que quieras de nuestra web</h4>
            </Row>
            <Row between='sm'>
              <Col xs={12} sm={6}>
                <input type='text' placeholder='Nombre' required />
              </Col>
              <Col xs={12} sm={6}>
                <input type='email' placeholder='Email' required />
              </Col>
              <Col xs={12} sm={12}><textarea placeholder='Tu texto' required></textarea></Col>
              <Col xs={12} sm={12}><button className='blue-fill-button'>Enviar</button></Col>
            </Row>
            <Row middle='sm'>
              <Col xs={12} sm={4}>
                <span className='text-blue'>Encuentranos también en...</span>
              </Col>
              <Col xs={12} sm={8}>
                <a href="https://www.facebook.com/Gowlook-534469503388904/?skip_nax_wizard=true" target="_blank">
                <span className="fa-stack small-margin facebook-hover">
                  <i className="fa fa-facebook fa-stack-1x border-circle"></i>
                </span>
                </a>
               <a href="https://plus.google.com/+Gowlook" target="_blank">
                <span className="fa-stack small-margin google-hover">
                  <i className="fa fa-google-plus fa-stack-1x border-circle"></i>
                </span>
                </a>
                <a href="https://twitter.com/gowlook" target="_blank">
                <span className="fa-stack small-margin twitter-hover">
                  <i className="fa fa-twitter fa-stack-1x border-circle"></i>
                </span>
                </a>
                <a href="https://www.linkedin.com/company/gowlook" target="_blank">
                <span className="fa-stack small-margin linkedin-hover">
                  <i className="fa fa-linkedin fa-stack-1x border-circle"></i>
                </span>
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
      </section>
    )
  }
}


function mapStateToProps (state) {
  return state
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(AppActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
