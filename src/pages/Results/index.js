import React, {Component} from 'react'

// uid library
import uid from 'uid'

// Redux elements
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

// redux actions
import {
  fetchProducts,
  fetchOffer,
  fetchTop3,
  fetchProductDetails
} from '../../reducers/actions/productActions'

import {
  setProductToCompare,
  selectProductDetails,
  removeProductToCompare,
  clearProducts
} from '../../reducers/actions/interactionActions'

//material ui components
import {List, ListItem} from 'material-ui/List'
import {Tabs, Tab} from 'material-ui/Tabs'
import FontIcon from 'material-ui/FontIcon'
//import IconButton from 'material-ui/IconButton'
import Divider from 'material-ui/Divider'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'

// material ui colors
import {grey500, /*cyan400*/} from 'material-ui/styles/colors'

//grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// icons
import ListIcon from 'material-ui/svg-icons/action/view-list'
import Fire from 'material-ui/svg-icons/social/whatshot'
import Module from 'material-ui/svg-icons/action/view-module'

//page components
import ItemSideR from './ResultsFilter/SidebarResulitem'
import MobileVersion from './mobile'

// common component
import SubNavR from '../../components/SubNav'
//import SideBarMenu from '../../components/SideBarMenu'
import Details from '../../components/Products/Details'
import Comparative from '../../components/Comparative'

//tab views
import tabViews from './tabViews'

//responsive component
import MediaQuery from 'react-responsive'

// swipeableViews
import SwipeableViews from 'react-swipeable-views';

class Results extends Component {
  constructor(props) {
    super(props)
    // change states to reducers interactions
    this.state = {
      viewType: 'list',
      open: false,
      activeTab: 2,
      openDetails: false,
      detailsTab: 0,
      slideIndex: 0,
    }
    // component functions
    this.handleSelectView = this.handleSelectView.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeTab = this.handleChangeTab.bind(this)
    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.handleRequestClose = this.handleRequestClose.bind(this)
    this.handleChangeSlider = this.handleChangeSlider.bind(this)
  }
  /* this line need change to code standar */
  handleChangeTab = (value) => {
    this.setState({
      slideIndex: value,
    });
  }
  componentWillReceiveProps(nextProps) {
    if( this.props.match.params.id  !== nextProps.match.params.id) {
      this.props.fetchProducts({
        categoryId: nextProps.match.params.id
      })
    }
  }
  componentDidMount() {
    //push scroll to top
    window.scrollTo(0,0)
    // set promise for assync request
    let Promises = []
    // fetch all products
    Promises.push(
      new Promise(
        () =>
        this.props.fetchProducts({
          categoryId: this.props.match.params.id
        })
      )
    )
    Promise.all(Promises);
  }
  /**
   * Filter by slide prices
   *
   * @param {Object} prices should have max and min integer values
   */
  handleChangeSlider (prices) {
    let Promises = []
    Promises.push(
      new Promise(
        () =>
        this.props.fetchProducts({
          categoryId: this.props.match.params.id,
          maxPrice: prices.max,
          minPrice: prices.min
        })
      )
    )
    Promise.all(Promises);
  }
  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
  }
  // change list view to icon
  handleSelectView(view) {
    console.log(view.type)
    this.setState({
      viewType: view.type
    })
  }
  handleRequestClose() {
    this.setState({
      open: false,
    })
  }
  handleTouchTapView(value) {
    this.setState({
      listView: value,
    })
    this.handleSelectView({
      type: value ? 'list' : 'icons'
    })
  }
  // change product details tab
  toggleModal(isopen, value, product) {
    this.setState({
      openDetails: isopen,
      detailsTab: value,
    })
    // set product data in reducer
    isopen && this.props.fetchProductDetails(product.id)
  }
  handleChange(value) {
    this.setState({
      detailsTab: value,
    })
  }
  render() {
    // json styles
    const labelStyles = {
      textTransform: 'capitalize',
    }
    const buttonStyles = {
      flexDirection: 'row',
      height: '55px',
      width: 'auto',
    }
    const inkBarStyle = {
      backgroundColor: 'rgb(0, 176, 198)',
      height: '3px',
    }
    const tabsStyles = {
      width: '600px',
      boxSizing: 'border-box',
    }
    const {viewType} = this.state
    // get props values
    const {subCategories, match} = this.props
    const category = subCategories[match.params.mainPosition]
    const subCategory = category ? category[match.params.position].name : []
    //tab views components
    const {
      Top3,
      Results,
      Offer,
      SellingGuide
    } = tabViews;

    return (
      <div>
        <MediaQuery query="(min-width: 800px)">
          <SubNavR category={subCategory} textColor={grey500}/>
            <div>
            <Grid style={{position: 'relative'}}>
              <Divider style={{width: '100%', position: 'absolute', top: '55px'}}/>
              <Row style={{margin: '0px'}}>
                <Col xs={12}>
                  <Row style={{margin: '0px'}} id="menu-container">
                    <Col xs={4} md={3}>
                      <List>
                        <ListItem
                          primaryText={subCategory}
                          style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}}
                          onTouchTap={this.handleTouchTap}
                          leftIcon={this.state.listView ? <ListIcon /> : <Module /> }
                          innerDivStyle={{fontSize: '20px', color: grey500}}
                          open={this.state.open1} >

                          <Popover
                            open={this.state.open}
                            anchorEl={this.state.anchorEl}
                            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                            targetOrigin={{horizontal: 'left', vertical: 'top'}}
                            onRequestClose={this.handleRequestClose} >
                              <Menu>
                                <MenuItem primaryText="Vista lista" onTouchTap={this.handleTouchTapView.bind(this, true)} leftIcon={<ListIcon />} />
                                <MenuItem primaryText="Vista Iconos" onTouchTap={this.handleTouchTapView.bind(this, false)} leftIcon={<Module />} />
                              </Menu>
                          </Popover>
                        </ListItem>
                      </List>
                    </Col>
                    <Col xs={8} md={9}>
                      <Tabs
                        onChange={this.handleChangeTab}
                        value={this.state.slideIndex}
                        inkBarStyle={inkBarStyle}
                        tabItemContainerStyle={tabsStyles}
                      >
                        <Tab
                          icon={
                            <FontIcon className='icon-010-square-grid tab-icon'/>
                          }
                          buttonStyle={buttonStyles}
                          className="blue-hover hover-border-tab"
                          label="Resultados"
                          style={labelStyles}
                          value={0}
                        />
                        <Tab
                          icon={
                            <FontIcon className='icon-011-open-book-top-view tab-icon'/>
                          }
                          label="Guia de compras"
                          className="blue-hover hover-border-tab"
                          buttonStyle={buttonStyles}
                          value={1}
                            style={labelStyles}
                        />
                        <Tab
                          icon={
                            <FontIcon className='icon-002-star-on-top-of-podium-of-sportive-competition tab-icon'/>
                          }
                          label="Top 3"
                          className="blue-hover hover-border-tab"
                          buttonStyle={buttonStyles}
                          style={labelStyles}
                          value={2}
                        />
                        <Tab
                          icon={
                            <Fire className="tab-icon"/>
                          }
                          label="Ofertas"
                          className="blue-hover hover-border-tab"
                          buttonStyle={buttonStyles}
                          style={labelStyles}
                          value={3}
                        />
                      </Tabs>
                    </Col>
                  </Row>
                  {/* this need to depurate*/}
                  <SwipeableViews
                    index={this.state.slideIndex}
                    onChangeIndex={this.handleChange}
                  >
                    {/** Result tab **/}
                    <Row style={{margin: '0px'}}>
                      <Col xs={3}>
                        <ItemSideR
                          handleChangeSlider={this.handleChangeSlider}
                          brandFilter={this.props.brands}
                          productTypes={this.props.productTypes}
                          handleSelectView={this.handleSelectView}
                          category={subCategory}
                        />
                      </Col>
                      <Col xs={9}>
                          <Results
                            products={this.props.products}
                            viewType={viewType}
                            comparativeProducts={this.props.comparativeProducts}
                            checkboxAction={this.props.setProductToCompare}
                            unCheckAction={this.props.removeProductToCompare}
                            priceAction={this.toggleModal}/>
                      </Col>
                    </Row>
                    {/** Selling Guide tab **/}
                    <Row style={{margin: '0px'}}>
                      <Col xs={9} xsOffset={3}>
                        <SellingGuide top3Products={this.props.products.slice(0, 3)}/>
                      </Col>
                    </Row>
                    {/** top 3 tab **/}
                    <Row style={{margin: '0px', minHeight: 100 }}>
                      <Col xs={12}>
                        <Top3 products={this.props.products.slice(0,3)} viewType="icons"/>
                      </Col>
                    </Row>
                    {/** offert tab **/}
                    <Row style={{margin: '0px'}}>
                      <Col xs={3}>
                        <ItemSideR
                          brandFilter={this.props.brands}
                          productTypes={this.props.productTypes}
                          categoryId={this.props.match.params.id}
                          handleSelectView={this.handleSelectView}
                          category={subCategory}
                        />
                      </Col>
                      <Col xs={9}>
                        <Offer
                          products={this.props.products}
                          comparativeProducts={this.props.comparativeProducts}
                          checkboxAction={this.props.setProductToCompare}
                          unCheckAction={this.props.removeProductToCompare}
                          viewType={viewType}/>
                      </Col>
                    </Row>
                  </SwipeableViews>
                </Col>
              </Row>
            </Grid>
          </div>
          <Details
            open={this.state.openDetails}
            product={this.props.selectedProduct}
            handleChange={this.handleChange}
            handleClose={() => this.toggleModal(false)}
            slideIndex={this.state.detailsTab} />
          <Comparative
            mobile={false}
            key={uid()}
            clearAction={this.props.clearProducts}
            products={this.props.comparativeProducts}/>
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">
          <MobileVersion
            comparativeProducts={this.props.comparativeProducts}
            checkboxAction={this.props.setProductToCompare}
            unCheckAction={this.props.removeProductToCompare}
            products={this.props.products}/>
          <Comparative
            mobile
            key={uid()}
            removeAction={this.props.removeProductToCompare}
            clearAction={this.props.clearProducts}
            products={this.props.comparativeProducts}/>
        </MediaQuery>
      </div>
    )
  }
}

// map states from reducer store
const mapStates = (states) => ({
  products: ( states.product.productsByCategory && states.product.productsByCategory.content ) || [],
  subCategories: states.categories.subCategories,
  comparativeProducts: states.interactions.comparativeProducts,
  selectedProduct: states.product.productModal,
  productTypes: states.product.productTypeDTOS,
  brands: states.product.brandDTOS,
})

// set function with dispatch you cant trigger this reducers actions with props.yourAction()
const mapDispatch = {
  fetchProducts,
  setProductToCompare,
  selectProductDetails,
  removeProductToCompare,
  fetchProductDetails,
  fetchOffer,
  clearProducts,
  fetchTop3
}
// connect with react router props and redux props
export default withRouter(
  connect(mapStates, mapDispatch)(Results)
)
