import React, {Component} from 'react'

//assets
import background from '../../assets/img/buhofondo.png'

//redux tools
import {connect} from 'react-redux'
import {fetchCategories} from '../../reducers/actions/categoriesActions'

// flexbox grid
import { Grid, Row, Col } from 'react-flexbox-grid'
//import {Tabs, Tab} from 'material-ui/Tabs'

// smooth scrolling container
import { StickyContainer, Sticky } from 'react-sticky';

// smooth srotlling event library
import Scroll from 'react-scroll'

// material ui color palette
import {grey400} from 'material-ui/styles/colors'

// common components
import HomeSearcher from '../../components/HomeSearcher'
import Category from '../../components/Category'
import SideBarMenu from '../../components/SideBarMenu'

// media query library
import MediaQuery from 'react-responsive'

// version mobile
import MobileHome from './Mobile'

// component styles
import './home.css'


class Home extends Component {
  componentDidMount(){
    this.props.fetchCategories(10)
  }
  render() {
    const Element = Scroll.Element;
    return (
      <div>
        <MediaQuery query="(min-width: 800px)">
          <div className="relative">
            <img src={background} alt=" " className="image-background"/>
            <div className="search-widget">
              <h2 className="home-title">La forma más ágil de <span> comparar tecnología</span> </h2>
              <HomeSearcher/>
            </div>
          </div>
          <StickyContainer>
            <div className="container">
                <Grid fluid>
                  <Row>
                    <Col lg={3} xs={4} style={{paddingRight: "30px"}}>
                      <div className="border-right" style={{height: "100%"}}>
                        <Sticky>
                          <SideBarMenu categories={ this.props.mainCategories }/>
                        </Sticky>
                      </div>
                    </Col>
                    <Col lg={9} xs={8}>
                      {/* category  loop*/}
                      {this.props.mainCategories.map((category, index) => (
                          <Element key={index} name={('mainCategory' + category.id).replace(/\s+/g,'')} className="category-element">
                            <h2 style={{fontWeight: 'normal', color: grey400}}>{category.name}</h2>
                            <Row>
                              { category.id !== "undefined" &&
                                  this.props.subCategories[category.id].map((subCategory, key) => (
                                      <Col xs={6} md={4} key={key}>
                                          <Category
                                            { ...subCategory }
                                            mainCategoryId={ category.id }
                                            position={ key }
                                            key={index}/>
                                      </Col>
                                  ))
                              }
                            </Row>
                          </Element>
                      ))}
                      </Col>
                    </Row>
                </Grid>
            </div>
          </StickyContainer>
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">
          <MobileHome/>
        </MediaQuery>
      </div>
    )
  }
}

const mapStates = (states) => ({
  categories: states.categories.categories,
  mainCategories: states.categories.mainCategories,
  subCategories: states.categories.subCategories
})
const mapDispatch = {
  fetchCategories,
}
export default connect(mapStates, mapDispatch)(Home)
