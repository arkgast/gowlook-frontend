import {
  TOGGLE_FOOTER_GOW,
  TOGGLE_FOOTER_HISTORIES,
  TOGGLE_FOOTER_REGISTER
} from '../constants/appConts';

const toggleFooterGow = value => dispatch => dispatch({type: TOGGLE_FOOTER_GOW, payload: value})
const toggleFooterHistories = value => dispatch => dispatch({type: TOGGLE_FOOTER_HISTORIES, payload: value})
const toggleFooterRegister = value => dispatch => dispatch({type: TOGGLE_FOOTER_REGISTER, payload: value})

export default {
  toggleFooterGow,
  toggleFooterHistories,
  toggleFooterRegister
}
