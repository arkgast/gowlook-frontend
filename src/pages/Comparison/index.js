import React from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import KeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import KeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
import { StickyContainer, Sticky } from 'react-sticky'
// import ComparisonTop from './components/ComparisonTop'
import ComparisonMiddle from './components/ComparisonMiddle'
import ComparisonCharacteristics from './components/ComparisonCharacteristics'
import ComparisonBottom from './components/ComparisonBottom'
import AttachBar from './components/AttachBar'
import './Comparison.css'

import emitterClient from '../../services/emitter'


function PrevArrow (props) {
  const {className, style, onClick} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={() =>{
    emitterClient.emit('carousel:setIndex', props.currentSlide-1 )
    pros.onSlideChange(props.currentSlide-1)
    return onClick()
  }} backgroundColor='rgba(255, 255, 255, 0.7)'><KeyboardArrowLeft  style={{fill:'#515151'}} /></FloatingActionButton>)
}

function NextArrow (props) {
  const {className, style, onClick, currentSlide} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={() =>{
    emitterClient.emit('carousel:setIndex', props.currentSlide+1 )
    pros.onSlideChange(props.currentSlide+1)
    return onClick()
  }} backgroundColor='rgba(255, 255, 255, 0.7)'><KeyboardArrowRight style={{fill:'#515151'}} /></FloatingActionButton>)
}

class Comparison extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentSlideIndex: 0
    }

    this.onSlideChange = this.onSlideChange.bind(this)
    this.changeSlider = this.changeSlider.bind(this)
  }

  onSlideChange (newIndex) {
    this.setState({currentSlideIndex: newIndex})
  }

  changeSlider (next) {
    let current = this.state.currentSlideIndex
    let newIndex = next ? current + 1 : current - 1
    emitterClient.emit('carousel:setIndex', newIndex )
  }


  render () {

    let carouselSettings = {
        dots: false,
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: <NextArrow onSlideChange={this.onSlideChange} />,
        prevArrow: <PrevArrow onSlideChange={this.onSlideChange} />,
        centerPadding: true,
        responsive: [{
                        breakpoint: 1024, settings: { slidesToShow: 5 }
                      },
                      {
                        breakpoint: 768, settings: { slidesToShow: 3 }
                      },
                      {
                        breakpoint: 640, settings: { slidesToShow: 2 }
                      }]
     }


    return (<section className='comparison container'>

      <StickyContainer>

        <Sticky>
            <AttachBar carouselSettings={carouselSettings} changeSlider={this.changeSlider} />
        </Sticky>

        <ComparisonMiddle carouselSettings={carouselSettings}/>

        <ComparisonCharacteristics carouselSettings={carouselSettings}/>

      <ComparisonCharacteristics carouselSettings={carouselSettings}/>

      </StickyContainer>

      <ComparisonBottom carouselSettings={carouselSettings} />

    </section>)
  }
}

export default Comparison
