import SellingGuide from './sellingGuide'
import Top3 from './top3'
import Offer from './offer'
import Results from './results'

// Export Tab views in json
export default {
  SellingGuide,
  Top3,
  Offer,
  Results
}
