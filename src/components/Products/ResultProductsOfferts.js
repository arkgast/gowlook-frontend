import React, {Component} from 'react'

// Grid
import { /*Grid,*/ Row, Col } from 'react-flexbox-grid'

// Material ui components
import Checkbox from 'material-ui/Checkbox';
import {fullWhite, cyan500, grey300} from 'material-ui/styles/colors';

// connect with router props
import {withRouter} from 'react-router-dom'

// styles
import '../../pages/Results/ResultsOferts/ResultOfferts.css'

class FilOferts extends Component {
  constructor() {
    super()
    this.state = {
      hover: false,
      checked: false
    }
    this.onCheck = this.onCheck.bind(this)
    this.redirect = this.redirect.bind(this)
  }
  toggleHover(value) {
    this.setState({hover: value})
  }
  redirect() {
    this.props.history.push(`/Sheet/${this.props.product.id}`)
  }
  onCheck() {
    if(this.state.checked) {
      this.props.unCheckAction(this.props.product.id)
    } else {
      this.props.checkboxAction(this.props.product)
    }
    this.setState(() => ({ checked: !this.state.checked }))
  }
  render() {
    const checkboxStyles = {
      svg: {
        width: '18',
        marginTop: '0.8px',
        height:'18',
        fontWeight: 'normal',
        opacity: '0.7'
      },
      box: {
        width: '18',
        height:'18',
      },
      // this section should be depurate
      label: {
        fontWeight: 'normal',
        padding: '3px 10px',
        whiteSpace: 'nowrap',
        color: this.state.checked ? cyan500 : grey300,
      }
    }
    return (
      <Row center='xs'>
        { this.props.viewType === 'icons' ?
            <Col xs={11} lg={10}>
              <div className='ResultCard icon-view offer'>
                <img src={this.props.img} onClick={this.redirect} alt='Producto?' className="pointer"/>
                <h4 className="title-grey"> {this.props.name} </h4>
                <Row center='xs' className="column-divider-horizontal">
                  <Col xs={6} className="column-divider-vertical-right padding-10">
                    <div className='price-buttom' >
                      <p>
                        {this.props.price} €
                      </p>
                    </div>
                  </Col>
                  <Col xs={6}>
                    <Row center="xs">
                    </Row>
                  </Col>
                  <Col xs={12}>
                    <Row center="xs">
                      <Col xs={12} md={6}>
                      </Col>
                    </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
              :
              <Col xs={11}>
                <div className='ResultCard list-view offer'>
                  <Row>
                    <Col xs={2} md={2}>
                      <img src={this.props.img} onClick={this.redirect} alt='Producto?' className="pointer"/>
                    </Col>
                    <Col xs={4} md={3}>
                      <div>
                        <h4
                          className="text-left text-indent-list pointer title-grey-strong margin-bottom-0 blue-hover hover-border-non-padding pull-left"
                          style={{maggin: "0"}}
                          onClick={this.redirect}>
                          {this.props.name}
                        </h4>
                      </div>
                      <Row style={{width: "100%", marginTop: "10px", float: "left"}}>
                        <Col xs={12} md={8} lg={4}>
                          {
                            this.props.checkboxAction &&
                              <div
                                style={{
                                  border: this.state.checked ? '1px solid ' + cyan500 : 'none',
                                  backgroundColor: ( this.state.hover && this.props.customText ) ? cyan500 : fullWhite,
                                  display: 'flex',
                                  width: !this.state.checked ? '135px' : '145px'
                                }}>
                                <Checkbox
                                  className={( this.state.hover && this.props.customText ) && "force-checked-color" }
                                  onCheck={this.onCheck}
                                  checked={this.state.checked}
                                  style={checkboxStyles.box}
                                  labelStyle={checkboxStyles.label}
                                  iconStyle={checkboxStyles.svg}
                                />
                                <span
                                  style={{
                                    ...checkboxStyles.label,
                                    color: ( ( this.state.hover && this.props.customText ) && fullWhite) || ( this.state.checked && cyan500 ) || grey300 ,
                                  }}
                                  onClick={() => this.props.customText && this.props.history.push("/comparison")}
                                  onMouseOver={this.toggleHover.bind(this, true)}
                                  onMouseLeave={this.toggleHover.bind(this, false)}
                                  className={ this.props.customText && "pointer" }>
                                  {!this.props.customText ? "Comparar" : this.props.customText }
                                </span>
                              </div>
                          }
                        </Col>
                      </Row>

                    </Col>
                    <Col xs={4} md={5} lg={5}>
                      <Row center="xs">
                        <p className="offer-value">
                          -50%
                        </p>
                      </Row>
                    </Col>
                    <Col xs={2} md={2} lg={2}>
                      <Row center="xs">
                        <div className="full-width">
                          <p className="title-grey text-margin-5">
                            {this.props.price} €
                          </p>
                        </div>
                        <div className="price-buttom" >
                          <p>
                            {this.props.price} €
                          </p>
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
          }
        </Row>

      
      )
    }
}
export default withRouter(FilOferts)
