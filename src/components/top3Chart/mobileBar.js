import React from 'react'
import './mobileBar.css'

// Mobile bar for top 3
const MobileBar = (props) =>  {

  var percent = props.percent
  var height = percent + "%"

  return (
    <div className="mobile-progress-bar">
      <div className="mobile-progress-track">
        <div className="mobile-progress-fill" style={{height: height}}>
          <span>{props.percent}%</span>
        </div>
      </div>
    </div>
  )
}

export default MobileBar
