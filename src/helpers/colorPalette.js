//import {darken, emphasize, lighten} from 'material-ui/utils/colorManipulator';
import {grey500, grey400} from 'material-ui/styles/colors';

export default {
  palette: {
    textColor: grey500,
  },
  appBar: {
    height: 50,
    color: 'white',
  },
  tabs:{
    backgroundColor: 'white',
    textColor: grey400,
    selectedTextColor: "rgb(0, 176, 198)",
  },
}
