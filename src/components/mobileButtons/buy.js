import React from 'react'

// styles
import './styles/buy.css'

// Grid
//import { Row, Col } from 'react-flexbox-grid'

// movile button
const BuyButton = (props) => (
  <div onClick={props.handleOpenClick}  className={ "buy-buttom-mobile text-center " + props.className} >
      <span>Comprar </span>
  </div>
)

export default BuyButton
