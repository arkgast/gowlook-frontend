import React from 'react'
// Grid
import { Row, Col } from 'react-flexbox-grid'

// material-ui components
import Divider from 'material-ui/Divider'
//import Checkbox from 'material-ui/Checkbox';
import {lightBlue400, cyan700, blueGrey600} from 'material-ui/styles/colors'

// charts
import {PieChart, Pie, Legend, Tooltip, ResponsiveContainer} from 'recharts'

// common components
import Toggle from '../Buttons/toggle'

import uid from 'uid'

// hard code data for selling guide chart
const data02 = [
  {name: 'Group A', value: 2400, fill: cyan700},
  {name: 'Group C', value: 1398, fill: lightBlue400},
  {name: 'Group F', value: 4800, fill: blueGrey600}
]

const SellingSection = (props) => (
  <div>
    <Row style={{margin: '0px'}}>
      <Col lg={9} md={7} xs={12} last={ props.right && "md" }>
        <h2 className='blue-title'>
          {props.title}
        </h2>
        <div className="text-grey content">
          {
            !props.mobile ?
                          props.content
                          :
                          <div className='sell-guide-content-mobile'>
                            <div className='mobile-content'>
                              { props.content }
                            </div>
                            <div className='right-align'>
                              <a className='read-more grey-text' href="#" >Leer más ></a>
                            </div>
                          </div>
          }

        </div>
      </Col>
      <Col lg={3} md={5} xs={12} className="relative" style={{paddingTop: '22px'}}>
        <Toggle className='sell-guide-toggle' label1="Todos" label2="Top 3" tab={props.slideIndex} toggle={props.handleToggle} style={{position: 'absolute', width: '97%'}}/>
        <Row center="xs">
          <ResponsiveContainer minHeight={250} width={props.mobile ? '100%' : 200}>
            <PieChart className='sell-guide-pie-chart' width={200} height={250}>
              <Pie dataKey={uid()} data={data02} cx={100} cy={100} innerRadius={50} outerRadius={70} fill={lightBlue400} label={props.mobile}/>
              { !props.mobile && <Tooltip/> }
              <Legend align={props.mobile ? 'left' : 'center'} verticalAlign={props.mobile ? 'middle': 'bottom'}/>
            </PieChart>
          </ResponsiveContainer>

        </Row>
      </Col>
    </Row>
    <Divider style={{width: '100%', marginTop: '30px'}}/>
  </div>
)

export default SellingSection
