import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'


function TopPost (props) {
  return (
    <div className='top-post'>
      <img src={props.img} alt={props.title} />
      <h2 className='top-post-title'>{props.title}</h2>
      <Link to='/postech/editor' className='top-post-link'>Ver más</Link>
    </div>
  )
}

TopPost.propTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired
}

export default TopPost
