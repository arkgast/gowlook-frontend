import React from 'react';

// Grid
import { Row, Col } from 'react-flexbox-grid'

// material ui components
import TextField from 'material-ui/TextField'
import Paper from 'material-ui/Paper'
import Divider from 'material-ui/Divider'
import {cyan400, blue900, red400} from 'material-ui/styles/colors'

// common components
import CurveButton from '../Buttons/curveButtons'
import Switch from '../Buttons/switch'

const LoginBoxMobile = (props) => {
  var classes = props.showLogin ? 'login animated slideInDown' : 'login hidden-login animated slideOutUp'
  const styles = { input: { width: '100%' } }
  return (
      <div className="relative">
        <div className="container">
          <Row className={classes}>
              <Col xs={8} style={{marginBottom: '5px', marginTop: '20px'}}>
                    <Switch
                      label1="Login"
                      buttonOneAction={props.buttonOneAction}
                      buttonTwoAction={props.buttonTwoAction}
                      label2="Sign up"
                      tab={props.tab}/>
              </Col>
              <div className="close-login" onClick={props.close}>
                <span> X </span>
              </div>
              <Col xs={12}>
                  <Paper zDepth={0} className="padding-10">
                      <form>
                          <h3
                            style={{marginTop: "0px"}}
                            className="title-grey text-left simple-title">
                              Login with:
                          </h3>
                          <Row center="xs">
                            <Col xs={5}>
                              <CurveButton
                                label="+google"
                                color={red400} />
                            </Col>
                            <Col xs={5} xsOffset={1}>
                              <CurveButton
                                label="Facebook"
                                color={blue900} />
                            </Col>
                          </Row>
                          <div className="loginDivider">
                              <span>or</span>
                          </div>
                          <div className="contentInput">
                            {
                              props.tab === 2 &&
                                <TextField
                                  style={styles.input}
                                  floatingLabelText="Nombre" />
                            }
                             <TextField
                                style={styles.input}
                                floatingLabelText="Email address" />
                             <TextField
                                style={styles.input}
                                floatingLabelText="Pasword" />
                            {
                              props.tab === 2 &&
                               <TextField
                                  style={styles.input}
                                  floatingLabelText="Confirm Pasword" />
                            }
                          </div>
                          <Row center="xs">
                            <Col xs={11}>
                              <CurveButton
                                label="Login"
                                color={cyan400}/>
                            </Col>
                          </Row>
                          <Row style={{marginTop: '20px'}}>
                            <Col xs={6} className="account">
                              <a href="#">Create an account</a>
                            </Col>
                            <Col xs={6} className="forgot">
                              <a href="#">Forgot Password</a>
                            </Col>
                          </Row>
                      </form>
                  </Paper>
              </Col>
          </Row>
        </div>
        <Divider style={{width: "100%", position: "absolute", left: 0, bottom: 0}}/>
      </div>
  )
}

export default LoginBoxMobile;
