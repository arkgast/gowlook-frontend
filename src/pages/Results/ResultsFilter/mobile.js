import React from 'react'

//grid
import { Row, Col } from 'react-flexbox-grid'

// material ui components
import Paper from 'material-ui/Paper'
import RightIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
import Tune from 'material-ui/svg-icons/image/tune'
import IconButton from 'material-ui/IconButton'

// Result filter mobile pane
const Mobile = (props) => (
  <Paper zDepth={1}>
    <Row>
      <Col xs={2} className="flex-center">
        <IconButton onTouchTap={props.onLeftClick}>
          <Tune />
        </IconButton>
      </Col>
      <Col xs={8}>
        <h3 className="text-grey text-left">
          Filtros
        </h3>
      </Col>
      <Col xs={2} className="flex-center">
        <IconButton onTouchTap={props.onRightClick}>
          <RightIcon/>
        </IconButton>
      </Col>
    </Row>
  </Paper>

)

export default Mobile
