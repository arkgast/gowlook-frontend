import React, {Component} from 'react'

// mobile view
import Mobile from './mobile'

// material ui components
import Divider from 'material-ui/Divider'

// material ui colors
import {
  grey300,
  grey400,
  grey500,
  cyan400,
  grey200
} from 'material-ui/styles/colors'

//material ui icons
import Star from 'material-ui/svg-icons/toggle/star'

// use this icons when rate working
//import StarBorder from 'material-ui/svg-icons/toggle/star-border'
//import StarHalf from 'material-ui/svg-icons/toggle/star-half'

// smooth scrolling container
import { StickyContainer, Sticky } from 'react-sticky';

// smooth srotlling event library
import Scroll from 'react-scroll'

// Redux elements
import { connect } from 'react-redux'
import {withRouter, Link} from 'react-router-dom'
import {fetchProduct} from '../../reducers/actions/productActions'

//grid
import { Row, Col } from 'react-flexbox-grid'

// slider action component
import SwipeableViews from 'react-swipeable-views';

// common componets
import RankingChart from '../../components/RankingChart'
import PriceButton from '../../components/Buttons/price'
import SubNavR from '../../components/SubNav'
import SideBarMenu from '../../components/SideBarMenu'
import Slider from '../../components/Slider'

// assets
import speedmeter from '../../assets/img/speedmeter.jpg'

//responsive component
import MediaQuery from 'react-responsive'

import './styles.css'

// ES6 component
class ProductSheet extends Component {
  constructor() {
    super()
    this.state = {
       image: 0
    }
    this.handleChange = this.handleChange.bind(this)
  }
  componentDidMount() {
    this.props.fetchProduct(this.props.match.params.id)
  }
  handleChange(value) {
    this.setState(() => ({image: value}))
  }
  render() {
    const {product} = this.props
    const Element = Scroll.Element;
    const nextIndex = product.images && ( this.state.image === product.images.length -1  ? this.state.image  : this.state.image + 1 )
    const prevIndex = product.images && ( this.state.image === 0 ? 0 : this.state.image - 1 )
    return (
      <div>
        <MediaQuery query="(min-width: 800px)">
          <SubNavR category="Producto" textColor={grey500}/>
          <div style={{minHeight: '500px'}}>
            <Row center="xs">
              <Col xs={10} className="flex-space-around blue-hover" style={{borderBottom: '1px solid' + grey300}}>
                <p className="text-grey pointer blue-hover flex-center margin-none" style={{marginBottom: "5px"}}>
                  <span className="icon-010-square-grid" style={{fontSize: '1.2em'}}/>
                  <Link
                    className="nav-item blue-hover pointer hover-border"
                    to="/results/11/1/0"
                    style={{marginLeft: '10px'}}
                  >
                    Ver todos
                  </Link>
                </p>
                <p className="text-grey pointer blue-hover flex-center margin-none" style={{marginBottom: "5px"}}>
                  <span className="icon-009-students-cap" style={{fontSize: '1.5em'}}/>
                  <span style={{marginLeft: '10px'}}>
                    Guia de compras
                  </span>
                </p>
                <p className="text-grey pointer blue-hover margin-none" style={{marginBottom: "5px"}}>
                  <span className="icon-008-logout-button" style={{fontSize: '1.2em'}}/>
                </p>
              </Col>
              <Col xs={10} style={{borderBottom: '1px solid' + grey300}}>
                <Row>
                  <Col xs={9} className="relative" style={{paddingBottom: '40px'}}>
                    <div
                      style={{borderRight: '1px solid' + grey300, }} >
                      <h1 className="text-center text-grey" style={{fontSize: '2.3em', fontWeight: 'normal'}}>
                        {product.name}
                      </h1>
                      <SwipeableViews
                        index={this.state.image}
                        onChangeIndex={this.handleChange} >
                        {
                          product.images &&
                          product.images.map(( image, index ) => (
                            <div className="flex-center" >
                              <img src={image} alt="" style={{maxWidth: '100%', height: '300px'}} key={index}/>
                            </div>
                          ))
                        }
                      </SwipeableViews>
                    </div>
                    <span
                      className="icon-006-back pointer"
                      onClick={() => this.handleChange(prevIndex)}
                      style={{fontSize: '3em', position: 'absolute', top: '200px', left: '50px', color: grey300}}/>
                    <span
                      className="icon-007-next pointer"
                      onClick={() => this.handleChange(nextIndex)}
                      style={{fontSize: '3em', position: 'absolute', top: '200px', right: '50px', color: grey300}}/>
                      <ul>
                        {
                          product.images &&
                          product.images.map(( image, index ) => (
                          <li
                            key={index}
                            style={{
                              display: 'inline-block',
                              backgroundColor: this.state.image === index ? cyan400 : grey200,
                              height: '15px',
                              width: '15px',
                              marginLeft: '5px',
                              borderRadius: '50%'
                              }}/>
                          ))
                        }
                      </ul>
                  </Col>
                  <Col xs={3}>
                    <Row center="xs">
                      <Col xs={8} className="flex-center" style={{marginTop: '30px'}}>
                        <PriceButton
                          containerStyle={{width: '120px', padding: '15px 0px'}}
                          priceStyle={{fontSize: '26px'}}
                          textStyle={{fontSize: '13px', margin: '22px 0px 0px 0px'}}
                          price={product.price}/>
                      </Col>
                      <Col xs={8} className="flex-center" style={{marginTop: '40px'}}>
                        <RankingChart
                          size="92px"
                          ranking={product.ranking}/>
                      </Col>
                      <Col xs={8} className="flex-center flex-wrap" style={{marginTop: '30px'}}>
                        <img
                          className="pointer"
                          src={speedmeter}
                          alt=" "
                          style={{maxWidth: '100%', height: '100px',}}/>
                        <p className="full-width text-center small-font" style={{margin: '0px'}}>Reputacion</p>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              {/* product feature container */}
              <Col xs={10}>
                <StickyContainer>
                  <Row>
                    <Col xs={4} style={{paddingRight: "30px"}}>
                      <div className="border-right" style={{height: "100%"}}>
                        <Sticky>
                          <SideBarMenu categories={product.groupFeatureByCategoryResponses} offset={-100}/>
                        </Sticky>
                      </div>
                    </Col>
                    <Col xs={8}>
                      {
                      product.groupFeatureByCategoryResponses &&
                        product.groupFeatureByCategoryResponses.map((item, index) => (
                          <Element name={('mainCategory'+ index).replace(/\s+/g,'')} key={index} style={{minHeight: '100px'}} className="categoryElement">
                            <h2 style={{fontWeight: 'normal', textTransform: 'capitalize', textAlign: 'left', marginTop: '10px'}}>
                              {item.name}
                            </h2>
                            {
                              item.productFeatureResponses.map((element, key) =>(
                                <Row key={key} style={{margin: '10px 0px', fontSize: '12px'}}>
                                  <Col xs={4} className="text-left">
                                    { element.name }
                                  </Col>
                                  <Col xs={8} className="text-left" style={{color: grey400}}>
                                    { element.value }
                                  </Col>
                                </Row>
                              ))
                            }
                          </Element>
                        ))}
                    </Col>
                  </Row>
                </StickyContainer>
              </Col>
              <Col xs={10} style={{marginTop: '20px'}}>
                <Row>
                  <Col xs={4} style={{borderRight: '1px solid ' + grey300, paddingRight: '30px' }}>
                    <p style={{color: cyan400, fontSize: '2em'}}> Vota y Opina</p>
                    <span className="text-center">{product.name}</span>
                    <img src={product.images && product.images[0]} alt={product.name} style={{maxWidth: '100%'}}/>
                    <span style={{color: cyan400}}>Valoracion de usuario</span>
                    <div className="flex-center" style={{margin: '10px 0px'}}>
                      {
                        [1, 2, 3, 4, 5].map(number => (
                          number === 5 ? <Star color={grey200}/> : <Star color={cyan400}/>
                        ))
                      }
                    </div>
                    <span style={{color: grey300, fontSize: '14px', }}>4.3 basado en reseñas</span>
                  </Col>
                  <Col xs={8} style={{paddingLeft: '2rem', paddingTop: '20px', paddingRight: '20px', paddingBottom: '20px'}}>
                    <Row style={{margin: '0px'}} className="flex-center">
                      <Col xs={3}>
                        <img
                          src="https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p160x160/16864101_10155183204123783_2537964751586452809_n.jpg?oh=c311f1df2fe626e81be168d9c5e87cc6&oe=5A0A05ED&__gda__=1510467993_e004c799929e4cba51847cdaf9500b2f"
                          alt=""
                          style={{width: '80px', height: '80px', borderRadius: '50%'}}/>
                      </Col>
                      <Col xs={9}>
                        <p style={{textAlign: 'left'}}> Joan Figueras</p>
                      </Col>
                    </Row>
                    <div style={{marginTop: '20px'}}>
                      <p style={{color: cyan400, textAlign: 'left'}}> Puntuar Todo</p>
                    </div>
                    <Divider/>
                    <div>
                      <Row style={{margin: '20px 0px'}}>
                        <Col xs={3} className="text-left truncate">
                          Caracteriastica1
                        </Col>
                        <Col xs={5}>
                          <Slider/>
                        </Col>
                      </Row>
                      <Row style={{margin: '20px 0px'}}>

                        <Col xs={3} className="text-left truncate">
                          Caracteriastica2
                        </Col>
                        <Col xs={5}>
                          <Slider/>
                        </Col>
                      </Row>
                      <Row style={{margin: '20px 0px'}}>

                        <Col xs={3} className="text-left truncate">
                          Caracteriastica3
                        </Col>
                        <Col xs={5}>
                          <Slider/>
                        </Col>
                      </Row>
                      <Row style={{margin: '20px 0px'}}>

                        <Col xs={3} className="text-left truncate">
                          Caracteriastica4
                        </Col>
                        <Col xs={5}>
                          <Slider/>
                        </Col>
                      </Row>
                      <Row style={{margin: '20px 0px'}}>

                        <Col xs={3} className="text-left truncate">
                          Caracteriastica5
                        </Col>
                        <Col xs={5}>
                          <Slider/>
                        </Col>
                      </Row>
                    </div>
                    <Divider/>
                    <div style={{marginTop: '15px'}}>
                      <p style={{textAlign: 'left'}}>Comentario</p>
                      <textarea style={{width: '100%', border: `1px solid ${grey200}`, height: '100px'}} cols="30" rows="10">
                      </textarea>
                      <button className="button-blue pull-right">
                        Publicar
                      </button>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">
          <Mobile/>
        </MediaQuery>
      </div>
    )
  }
}
const mapStates = (states) => ({
  product: states.product.product
})
const mapDispatch = {
  fetchProduct
}

export default withRouter( connect(mapStates, mapDispatch)( ProductSheet ) )
