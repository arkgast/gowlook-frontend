import { applyMiddleware, createStore, compose } from "redux"

import { default as thunk } from "redux-thunk"
import promise  from "redux-promise-middleware"
import { routerMiddleware } from 'react-router-redux'

const DevTools = require('./components/devtools/devtools').default
const { persistState } = require('redux-devtools')
const __SERVER__ = process.env.NODE_ENV === 'production'

const finalCreateStore = __SERVER__ ? () => compose(
  applyMiddleware(promise(), routerMiddleware(), thunk)
)(createStore) : (history) => compose(
  applyMiddleware(promise(), routerMiddleware(history), thunk),
  window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
  persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore)


export default finalCreateStore
