import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import pet from '../../assets/img/BgFooter.png'

//material-ui components
//import {grey300} from 'material-ui/styles/colors'

// our components
import Register from '../Register'

// styles
import './Footer.css'

//responsive component
import MediaQuery from 'react-responsive'
import FooterMobile from './mobile'

// unorganize hiperlinks you need repair that
const Footer = (props) => (
  <div>
    <MediaQuery query="(max-width: 800px)">
      <FooterMobile />
    </MediaQuery>
    <MediaQuery query="(min-width: 800px)">
      <div className="footer-container pull-left full-width">
        {
          props.showRegister && <Register registerAction={props.registerAction}/>
        }
        <div className="footer pull-left full-width relative" style={{zIndex: 2}}>
          { props.showPet && <img className="footer-img" alt=" " src={pet}/>}
          <Grid>
            <Row>
              <Col lg={5} md={6} xs={12}>
                <div className="social-widget">
                  <h2 className="text-center"> <div className="invert-word">¿</div>Quieres estar a la última? <br className='br' /> <span style={{fontSize: '22px'}}>¡Síguenos!</span></h2>
                  <div className="flex-center">
                    <a href="https://www.instagram.com/gowlook.photo/" target="_blank">
                    <span className="fa-stack fa-2x small-margin instagram-hover">
                      <i className="fa fa-instagram fa-stack-1x border-circle"></i>
                    </span>
                   </a>
                   <a href="https://plus.google.com/+Gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin google-hover">
                      <i className="fa fa-google-plus fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://www.facebook.com/Gowlook-534469503388904/?skip_nax_wizard=true" target="_blank">
                    <span className="fa-stack fa-2x small-margin facebook-hover">
                      <i className="fa fa-facebook fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://twitter.com/gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin twitter-hover">
                      <i className="fa fa-twitter fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://www.linkedin.com/company/gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin linkedin-hover">
                      <i className="fa fa-linkedin fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="#">
                    <span className="fa-stack fa-2x small-margin youtube-hover">
                      <i className="fa fa-youtube-play fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                  </div>
                  <br />
                  <div className='flex-center text-grey' style={{paddingBottom: 10}}>
                    Copyright © 2017 Gowlook. Todos los derechos reservados
                  </div>
                </div>
            </Col>
              <Col lg={7} md={6} xs={12}>
                <Row center="xs" className="footer-links-container">
                  <Col md={4} xs={3}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Loremipsum</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="https://gowlook.com/legal/privacy.html" target="_blank">Lorem ipsum</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Lorem ipsum</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/legalnotice.html" target="_blank">Lorem ipsum</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/faq.html" target="_blank">FAQS</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col md={3} xs={2}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Goowlok</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="/our-team" target="_blank">Equipo</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Acerca</a>
                        </li>
                        <li>
                          <a href="/contacto" target="_blank">Contacto</a>
                        </li>
                        <li>
                          <a href={"/postech/Histories"} target="_blank">FAQs</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col md={4} xs={3}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Business</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="https://gowlook.com/legal/legalnotice.html" target="_blank">Aviso legal</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/privacy.html" target="_blank">Politicas de privacidad</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Politica de cookies</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </div>
      </div>
    </MediaQuery>
  </div>
)

export default Footer
