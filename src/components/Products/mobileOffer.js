import React from 'react'

// Grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// Material ui components
import Checkbox from 'material-ui/Checkbox';
import Paper from 'material-ui/Paper'
import {grey200} from 'material-ui/styles/colors'

// json styles
const styles = {
  container: {
    height: '180px',
    border: '1px solid ' + grey200 ,
    marginTop: '20px'
  },
  price: {marginTop: '15px', marginBottom: '24px'},
  buttom: {marginBottom: '24px'},
  checkboxContainer: {marginTop: '28px'}
}

const checkboxStyles = {
  svg: {
    width: '18',
    marginTop: '0.8px',
    height:'18',
    fontWeight: 'normal',
    opacity: '0.7'
  },
  box: {
    width: '18',
    height:'18',
    margin: 'auto'
  },
  label: {
    fontWeight: 'normal',
  }
}

// component
const MobileOffer = (props) => (
  <Paper zDepth={0} style={styles.container} className="relative">
    <Grid fluid>
      <Row>
        <Col xs={12}>
          <div className='ResultCard view-mobile offer'>
            <Row>
              <Col xs={6}>
                <h4 className="text-left text-indent-list title-grey"> {props.name} </h4>
                <img src={props.img} alt='Producto?'/>
              </Col>
              <Col xs={6}>
                <Row center="xs">
                  <Col xs={10}>
                    <div className="full-width">
                      <p className="title-grey text-margin-5" style={styles.price}>
                        {props.price} €
                      </p>
                      <div className="flex-center" style={styles.button}>
                        <div className="price-buttom" >
                          <p>
                            {props.price} €
                          </p>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xs={12} style={styles.checkboxContainer} >
                    <Checkbox
                      label="Comparar"
                      style={checkboxStyles.box}
                      labelStyle={checkboxStyles.label}
                      iconStyle={checkboxStyles.svg}/>
                  </Col>
                </Row>
              </Col>
              <p className="offer-value-circle">
                -50%
              </p>
            </Row>
          </div>
        </Col>
      </Row>
    </Grid>
  </Paper>
)

export default MobileOffer
