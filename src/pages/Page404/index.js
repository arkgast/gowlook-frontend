import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppActions from '../../reducers/actions/appActions'
import { Col, Row } from 'react-flexbox-grid'
import img404 from '../../assets/img/page-404.png'
import HomeSearcher from '../../components/HomeSearcher'
import InputSearch from '../../components/InputSearch/mobile'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'
import './style.css'

class Page404 extends React.Component {

  constructor(props) {
    super(props);

  }

  componentWillMount () {
    this.props.actions.toggleFooterHistories(false)
    this.props.actions.toggleFooterGow(false)
    this.props.actions.toggleFooterRegister(false)
  }


  render () {
    return (<section className='page-404 container'>
      <Row center='sm'>
        <Col sm={10} xs={12} md={4}>
          <img className='logo-404' src={img404} alt='404' />
        </Col>
        <Col sm={10} xs={12} md={6}>
          <h3 className='text-blue'>UPS, aquí no está lo que estás buscando<br />Pero no te vayas:</h3>
          <div className='searcher'>
            <MediaQuery query='(min-width: 640px)'>
              <HomeSearcher placeholder='Busca lo que quieras' />
            </MediaQuery>
            <MediaQuery query='(max-width: 640px)'>
              <InputSearch placeholder='Busca lo que quieras' />
            </MediaQuery>
          </div>

          <h4 className='text-grey'>O echa un vistazo al Home:</h4>
          <div>
            <Link to='/' className='back-button'>Homepage</Link>
          </div>
        </Col>
      </Row>
    </section>)
  }
}

function mapStateToProps (state) {
  return state
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(AppActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page404);
