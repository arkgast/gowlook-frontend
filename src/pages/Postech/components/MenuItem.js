import React from 'react'
import FlatButton from 'material-ui/FlatButton'
import PropTypes from 'prop-types'
import {
  // lightBlue200,
  // grey200,
  cyan400,
} from 'material-ui/styles/colors'

function MenuItem (props) {
  return <FlatButton
                  style={{
                    borderBottom: props.isActived ? ` 3px solid ${cyan400}` : '2px solid #d3d3d3',
                    color: props.isActived ? cyan400 : ''
                  }}
                  className='menu-item'
                  label={props.label} />
}


MenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  isActived: PropTypes.bool
}

export default MenuItem
