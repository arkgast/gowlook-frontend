import React from 'react'

// styles
import './styles/price.css'

// Grid
//import { Row, Col } from 'react-flexbox-grid'

// movile button
const PriceButton = (props) => (
  <div onClick={props.handleOpenClick}  className={ "price-buttom-mobile text-center " + props.className}  style={{marginTop: '16px'}}>
      <span>{props.price} € |   { !props.mobile && 'Ver Precios' } >> </span>
  </div>
)

export default PriceButton
