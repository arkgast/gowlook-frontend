import FlagArgentina from '../assets/img/flags svg/006-argentina.svg'
import FlagAustralia from '../assets/img/flags svg/012-australia.svg'
import FlagBrazil from '../assets/img/flags svg/008-brazil.svg'
import FlagCanada from '../assets/img/flags svg/009-canada.svg'
import FlagChile from '../assets/img/flags svg/005-chile.svg'
import FlagChina from '../assets/img/flags svg/018-china.svg'
import FlagColombia from '../assets/img/flags svg/004-colombia.svg'
import FlagFrance from '../assets/img/flags svg/023-france.svg'
import FlagGermany from '../assets/img/flags svg/024-germany.svg'
import FlagIndia from '../assets/img/flags svg/016-india.svg'
import FlagItaly from '../assets/img/flags svg/021-italy.svg'
import FlagJapan from '../assets/img/flags svg/017-japan.svg'
import FlagKenya from '../assets/img/flags svg/003-kenya.svg'
import FlagSouthKorea from '../assets/img/flags svg/015-south-korea.svg'
import FlagMexico from '../assets/img/flags svg/007-mexico.svg'
import FlagNewZeland from '../assets/img/flags svg/011-new-zealand.svg'
import FlagNigeria from '../assets/img/flags svg/001-nigeria.svg'
import FlagPhilipines from '../assets/img/flags svg/014-philippines.svg'
import FlagPoland from '../assets/img/flags svg/020-poland.svg'
import FlagPortugal from '../assets/img/flags svg/019-portugal.svg'
import FlagRussia from '../assets/img/flags svg/022-russia.svg'
import FlagSouthAfrica from '../assets/img/flags svg/002-south-africa.svg'
import FlagUnitedArabEmirates from '../assets/img/flags svg/013-united-arab-emirates.svg'
import FlagUnitedKingdom from '../assets/img/flags svg/025-united-kingdom.svg'
import FlagUnitedStates from '../assets/img/flags svg/010-united-states.svg'

export default [{
    name: "Argentina",
    img: FlagArgentina
  },
  {
    name: "Australia",
    img: FlagAustralia
  },
  {
    name: "Brazil",
    img: FlagBrazil
  },
  {
    name: "Canada",
    img: FlagCanada
  },
  {
    name: "Chile",
    img: FlagChile
  },
  {
    name: "China",
    img: FlagChina
  },
  {
    name: "Colombia",
    img: FlagColombia
  },
  {
    name: "France",
    img: FlagFrance
  },
  {
    name: "Germany",
    img: FlagGermany
  },
  {
    name: "India",
    img: FlagIndia
  },
  {
    name: "Italy",
    img: FlagItaly
  },
  {
    name: "Japan",
    img: FlagJapan
  },
  {
    name: "Kenya",
    img: FlagKenya
  },
  {
    name: "South Korea",
    img: FlagSouthKorea
  },
  {
    name: "Mexico",
    img: FlagMexico
  },
  {
    name: "New Zeland",
    img: FlagNewZeland
  },
  {
    name: "Nigeria",
    img: FlagNigeria
  },
  {
    name: "Philipines",
    img: FlagPhilipines
  },
  {
    name: "Poland",
    img: FlagPoland
  },
  {
    name: "Portugal",
    img: FlagPortugal
  },
  {
    name: "Russia",
    img: FlagRussia
  },
  {
    name: "South-Africa",
    img: FlagSouthAfrica
  },
  {
    name: "United Arab Emirates",
    img: FlagUnitedArabEmirates
  },
  {
    name: "United-Kingdom",
    img: FlagUnitedKingdom
  },
  {
    name: "USA",
    img: FlagUnitedStates
  }
]
