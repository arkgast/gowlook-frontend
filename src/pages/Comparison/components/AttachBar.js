import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-slick'
import uid from 'uid'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import emitterClient from '../../../services/emitter'


class AttachBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      carouselRef: 'middle'
    }

  }

  componentDidMount () {
    emitterClient.on('carousel:setIndex', (currentSlide) => {
        this.refs[this.state.carouselRef].slickGoTo(currentSlide)
    })
  }

  componentWillUnMount () {
    eventEmitter.removeEventListener('carousel:setIndex')
  }

  render () {

    return (
      <div className='comparison-attach'>

        <div className='comparison-attach-buttons'>
          <button className='blue-fill-button small' onClick={() => this.props.changeSlider()}>&#60; Anterior</button>
          <button className='blue-fill-button small' onClick={() => this.props.changeSlider(true)}>Próximo &#62;</button>
        </div>


        <div className='comparison-attach-products'>
          <Carousel {...this.props.carouselSettings} ref={this.state.carouselRef}>

          {
            Array(7).fill(null).map(() => (
              <div className='comparison-attach-product' key={uid()}>
                <span className='close-button'><i className='fa fa-times'></i></span>
                <img src='https://s3-us-west-2.amazonaws.com/gowlook/categories/camaras' alt='camera' />
                <h3 className='text-blue small-font'>Canon 6D</h3>
              </div>
            ))
          }
        </Carousel>

        </div>

      </div>
    )
  }
}


AttachBar.propTypes = {
  carouselSettings: PropTypes.object.isRequired
}

export default AttachBar
