import React from 'react'

// material ui components
//import Checkbox from 'material-ui/Checkbox'
import Paper from 'material-ui/Paper'
import {grey200} from 'material-ui/styles/colors'

// Charts
import MobileBar from '../top3Chart/mobileBar'

// json Styles
const styles = {
  container: {
    height: '150px',
    border: '1px solid ' + grey200 ,
    marginTop: '10px',
    padding: '10px',
    position: 'relative'
  },
  checkboxStyles: {
    svg: {
      width: '18',
      marginTop: '0.8px',
      height:'18',
      fontWeight: 'normal',
      opacity: '0.7'
    },
    box: {
      width: '18',
      height:'18',
    },
    label: {
      fontWeight: 'normal',
    }
  },
  productImgStyles: {
    maxWidth: '100%',
    height: '100px'
  }
}
// image
//import speedmeter from '../../assets/img/speedmeter.jpg'
//grid
import { Row, Col } from 'react-flexbox-grid'

// common componets
import RankingChart from '../../components/RankingChart'
import PriceButton from '../../components/mobileButtons/price'
import './styles/top3.css'

const Top3Mobile = (props) => (
    <Row style={{height: '100%'}} className='rowt3m'>
      <Col xs={1} style={{height: "100%", textAlign: 'center'}}>
        <MobileBar percent={props.percent}/>
      </Col>
      <Col xs={11} style={{height: '100%'}} >
        <Paper zDepth={0} style={styles.container}>
          <Row center="xs">
            <Col xs={7}>
              <h4 className="text-grey text-center margin-none">
                { props.name }
              </h4>
              <img src={props.img} style={styles.productImgStyles} alt={``}  />
            </Col>
            <Col xs={5}>
              <Row center="xs">
                  <Col xs={12} className="flex-center">
                    <RankingChart ranking={props.ranking}/><br/>
                  </Col>
                  <Col xs={12}>
                    <span className="small-font">
                      Ranking
                    </span>
                  </Col>
              </Row>
              <Row end="xs">
                <Col xs={6}>
                </Col>
                <Col xs={12} sm={6}>
                  <PriceButton mobile={true} price={props.price}/>
                </Col>
              </Row>
            </Col>
          </Row>
        </Paper>
      </Col>
    </Row>

)
export default Top3Mobile
