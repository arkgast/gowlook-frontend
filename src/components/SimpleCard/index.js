import React from 'react'

// material ui color palette
import {
  grey200,
  /*grey300,*/
  grey500
} from 'material-ui/styles/colors'

// style for border color and width
const border = `1px solid ${grey200}`
// stateless compoennt
const SimpleCard = (props) => (
  <div style={{
      margin: '5px 0px',
      height: '120px',
      border: border}}>
    <div
      className="relative"
      style={{
        width: '100%',
        height: '25px',
        borderBottom: border}}>
      <span style={{left: '5px', top: '5px', position: 'absolute', color: grey500, fontSize: '13px'}}>
        {props.header}
      </span>
    </div>
    <div style={{height: '95px', display: 'flex'}}>
      {props.children}
    </div>
  </div>
)

export default SimpleCard
