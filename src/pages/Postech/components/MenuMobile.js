import React from 'react'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import FlatButton from 'material-ui/FlatButton'
import Popover from 'material-ui/Popover'
import PropTypes from 'prop-types'
import { cyan400 } from 'material-ui/styles/colors'
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down'

class MenuMobile extends React.Component {

  constructor(props) {
      super(props)

      this.state = {
        open: false
      }
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render () {
    let firstMenuChunk = this.props.menuItems.length > 2 ? [this.props.menuItems[0], this.props.menuItems[1]] : []

    const renderMenu = () => {
      if (firstMenuChunk.length){
        return (<div className='postech-menu-mobile'>
                  {
                    firstMenuChunk.map(({ label, key, isActived }, index) => <FlatButton
                                                                                key={key}
                                                                                style={{
                                                                                  borderBottom: isActived ? ` 3px solid ${cyan400}` : '2px solid #d3d3d3'
                                                                                }}
                                                                                className='menu-item'
                                                                                label={label} />)
                  }

                  <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    onRequestClose={this.handleRequestClose.bind(this)}
                  >
                    <Menu>
                      {
                        this.props.menuItems.map((item, index) => index !== 0 && index !== 1 ? <MenuItem key={item.key} primaryText={item.label} /> : '')
                      }
                    </Menu>
                  </Popover>
                  <FlatButton
                      className='menu-item more-items-button'
                      labelPosition="before"
                      style={{ borderBottom:  '2px solid #d3d3d3' }}
                      onClick={event => {
                            event.preventDefault()
                            this.setState({open: true, anchorEl: event.currentTarget})
                          }
                        }
                      label='Más'><KeyboardArrowDown /></FlatButton>
                </div>)
      }
      else
        return (<div className='postech-menu-mobile'>
            {
                this.props
                        .menuItems
                        .map(({ label, key, isActived }, index) => <FlatButton
                                                                              key={key}
                                                                              style={{
                                                                                borderBottom: isActived ? ` 3px solid ${cyan400}` : '2px solid #d3d3d3'
                                                                              }}
                                                                              className='menu-item'
                                                                              label={label} />)
            }
              </div>)
    }

    return (<nav>
              {
                renderMenu()
              }
            </nav>)
  }
}



MenuMobile.propTypes = {
  menuItems: PropTypes.array.isRequired,
}


export default MenuMobile
