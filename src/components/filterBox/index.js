import React, {Component} from 'react'

// flex
import { Row, Col } from 'react-flexbox-grid'

// material ui components
import {List, ListItem} from 'material-ui/List'
import Checkbox from 'material-ui/Checkbox'

//import Dialog from 'material-ui/Dialog'
import {
  grey500,
  cyan400,
  /*cyan500*/
} from 'material-ui/styles/colors'

import IconButton from 'material-ui/IconButton'
//import FlatButton from 'material-ui/FlatButton'

import Cross from 'material-ui/svg-icons/navigation/close'

// other library
import ToolTip from 'react-portal-tooltip'


// common components
import SideBarMenu from '../SideBarMenu'
import BlueButton from '../BlueButton'

let style = {
  style: {
    width: '80%',
    maxWidth: '1300px',
    maxHeight: '500px',
    overflowY: 'auto'
  },
  arrowStyle: {
  }
}

class FilterBox extends Component {
  render() {
    return(
      <ToolTip
        active={this.props.openFilter}
        position="bottom"
        parent="#menu-container"
        style={style}
      >
        <div className={this.props.openFilter && "animated slideInLeft" }>
          <Row style={{margin: '0px'}} className="relative">
            <Col md={2} xs={4}>
              <div className="border-right" style={{height: '100%'}}>
                <SideBarMenu categories={this.props.filters} />
              </div>
            </Col>
            <Col md={10} xs={8} style={{marginTop: '10px'}}>
                  {
                    this.props.filters.slice(1).map(( filter, index ) => (
                      <Row>
                        {
                            this.props[filter.filterType].chunk_inefficient(4).map(( filterArray, indexArray ) => (
                                <Col xs={3} key={indexArray}>
                                  <List>
                                    {
                                      filterArray.map((item, key) => (
                                        <ListItem
                                          key={key}
                                          className="blue-hover"
                                          primaryText={index !== 6 ? item.name : "ver mas"}
                                          innerDivStyle={{padding: '16px 16px 16px 50px'}}
                                          style={{
                                            color: this.props.radioValue.indexOf(item.id) > -1 ? cyan400 : grey500, marginTop: '-15px',
                                          }}
                                          leftCheckbox={
                                            <Checkbox
                                              value={item.id}
                                              onCheck={this.onChangeRadio}
                                              checked={this.props.radioValue.indexOf(item.id) > -1}
                                            />
                                          }
                                        />
                                      ))
                                    }
                                  </List>
                                </Col>
                            ))
                          }
                      </Row>
                    ))
                  }
            </Col>
            <IconButton
              onTouchTap={this.props.handleClose}
              iconStyle={{width: '20px', height: '20px'}}
              style={{position: 'absolute', right: '10px', top: '-10px', width: '20px', height: '20px'}}>
              <Cross/>
            </IconButton>
          </Row>
          <Row end="xs" style={{margin: '0px'}}>
            <span
              style={{
                display: 'flex',
                alignItems: 'center',
                height: '40px',
                marginRight: '20px',
                cursor: 'pointer'
              }}
              onClick={this.props.handleClose}
            >
              <span className="text-grey blue-hover hover-border-non-padding" >
                Cancelar
              </span>
            </span>
            <BlueButton text="Aplicar" />
          </Row>
        </div>
      </ToolTip>
    )
  }
}

export default FilterBox
