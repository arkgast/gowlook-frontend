import React from 'react'
import {
  cyan400
} from 'material-ui/styles/colors'
import PropTypes from 'prop-types'
import MenuItem from './MenuItem'
import PostCard from './PostCard'

function CategoryPostsSection (props) {
  return (<section className='postech-category-section'>
            <h2 style={{color: cyan400}} className='postech-category-section-title'>{props.title}</h2>

            <nav className='postech-category-section-submenu'>
              {
                props.menuItems.map(({ label, key, isActived }) => <MenuItem isActived={isActived || false} key={key} label={label} />)
              }
            </nav>
            <div className="postech-category-section-posts">
              { props.posts.map((item, index) => <PostCard key={index} {...item} />) }
            </div>

          </section>)
}

CategoryPostsSection.propTypes = {
  menuItems: PropTypes.array.isRequired,
  posts: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
}

export default CategoryPostsSection
