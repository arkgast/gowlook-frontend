import React from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import uid from 'uid'

class SubMenu extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedArticles: []
    }

  }

  render () {
    return (<div className='postech-submenu'>
            <div className='ps-nav'>
              <ul>
                {
                  this.props.subCategories.map(item => (<li
                                                            key={uid()}
                                                            className='smooth-hover'
                                                            onMouseEnter={(ev) => {
                                                                this.setState({selectedArticles: item.articles})
                                                                ev.target.classList.add('isActived')
                                                              }
                                                            }
                                                            onMouseLeave={(ev) => ev.target.classList.remove('isActived')}
                                                             >{item.name}</li>))
                }
              </ul>
            </div>
            <div className='ps-content'>
              <div className='ps-papers'>
                {
                  this.state.selectedArticles.map(item => <Paper className='ps-paper' key={uid()}>
                    <img src={item.headerImg} alt={item.title} />
                    <h3>{item.title}</h3>
                  </Paper>)
                }
              </div>
              <div className='ps-all-histories'>
                <a href="#">Ver todas las historias</a>
              </div>
            </div>
          </div>)
  }
}

SubMenu.propTypes = {
    subCategories: PropTypes.array.isRequired,
}

export default SubMenu;
