import React from 'react'
import ReactStars from 'react-stars'
import TextField from 'material-ui/TextField'
import Divider from 'material-ui/Divider'
import Chip from 'material-ui/Chip'
import {List, ListItem} from 'material-ui/List'
import avatar from '../../assets/img/Blog-Web_22_B.png'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import './PostechEditor.css'
// import uid from 'uid'

class PostechEditor extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      author: {
        name: 'Lorem ipsum dolor sit',
        avatar,
        stars: 4
      },
      tagsOptions: [],
      tags: [
        {id: 1, label: 'Cámaras'},
        {id: 2, label: 'Análisis'}
      ],
      seoTags: [
        {id: 0, label: 'Angular'},
        {id: 1, label: 'JQuery'},
        {id: 2, label: 'Polymer'},
        {id: 3, label: 'ReactJS'},
      ]
    }
  }


  render () {

    return (<div className='postech-editor'>
              <div className='postech-editor-leftside'>
                <div className='postech-editor-author-info'>
                  <h3 className='author-name text-grey'>{this.state.author.name}</h3>
                  <img src={this.state.author.avatar} alt={this.state.author.name} />
                    <ReactStars
                      className='author-stars'
                      count={5}
                      value={this.state.author.stars}
                      size={12}
                      edit={false}
                      color2={'#0094cc'} />
                    <h3 className='range'>Gowlooker VIP</h3>
                    <span className='text-grey num-articles'>112 articles</span>
                </div>
                <br />
                <Divider />
                <div className='postech-editor-category-chooser'>
                  <h4 className='text-grey'>Elige categoría</h4>
                  <List className='categories-list'>
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                    <ListItem primaryText="Lorem ipsum" className='pte-item' />
                  </List>
                </div>
                <div className='postech-editor-addicional-content'>
                  <h4 className='text-grey'>Tipo de contenido</h4>

                  <button>Añadir comparación  <i className='fa fa-plus'></i></button>
                  <button>Añadir producto  <i className='fa fa-plus'></i></button>
                  <button>Productor relacionados  <i className='fa fa-plus'></i></button>
                </div>
              </div>

              <div className='postech-editor-principal'>
                <div className='postech-editor-topbar'>
                  <div className='postech-chips'>
                    {
                      this.state.tags.map((item, index) => {
                        return (<Chip
                          key={item.id}
                          className='postech-editor-custom-chip'
                          onRequestDelete={() =>  {
                            let newState = this.state.tags.splice(index, 1)
                            this.setState({tags: newState})
                          }}
                        >
                          {item.label}
                        </Chip>)
                      })
                    }
                  </div>
                  <div className='postech-editor-action-buttons'>
                    <button className='line-blue-button'>Guardar</button>
                    <button className='blue-button'>Publicar</button>
                  </div>
                </div>
                <div className='postech-editor-editor'>
                  <div>
                    <div>
                      <TextField
                         hintText="Escribe Título Aquí"
                         className='postech-editor-title'
                       />
                    </div>
                    <div className='select-an-image'>
                      <span><i className='fa fa-file-image-o '></i> Añadir una fotografía para la cabecera</span>
                      <input type='file' />
                    </div>
                  </div>
                  <div style={{minHeight: 500}}>
                    {/* DOCS: https://jpuri.github.io/react-draft-wysiwyg/#/docs */}
                    <Editor
                      wrapperClassName="editor-container"
                      editorClassName="editor-editable"
                       />
                  </div>
                  <div>
                    <span className='text-grey'><i className='fa fa-tag'></i> Añadir etiquetas para que las personas puedan encontrar fácilmente tu artículo</span>
                  </div>
                </div>
              </div>
            </div>)
  }
}

export default PostechEditor;
