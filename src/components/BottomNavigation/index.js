import React from 'react';

// material ui components
import FontIcon from 'material-ui/FontIcon';
import Fire from 'material-ui/svg-icons/social/whatshot'
import {Tabs, Tab} from 'material-ui/Tabs'
import {lightBlue200, grey200} from 'material-ui/styles/colors';
import Paper from 'material-ui/Paper';
//import IconLocationOn from 'material-ui/svg-icons/communication/location-on';

// json styles
const labelStyles = {
  textTransform: 'capitalize',
}
const buttonStyles = {
  height: '100px',
}
const inkBarStyle = {
  backgroundColor: lightBlue200,
  marginBottom: '100px',
  height: '3px',
}
const tabsStyles = {
  position: 'absolute',
  boxSizing: 'border-box',
}
const paperStyles = {
  position: 'fixed',
  borderTop: '2px solid ' + grey200,
  bottom: '0px',
  left: '0px',
  width: '100%'
}
const BottomNavigationExampleSimple = (props) => (
  <Paper zDepth={1}
    style={paperStyles}>
    <Tabs
      onChange={props.handleChange}
      value={props.slideIndex}
      inkBarStyle={inkBarStyle}
      tabItemContainerStyle={tabsStyles} >
      {/** Result tab **/}
      <Tab
        icon={
          <FontIcon className='icon-010-square-grid tab-icon-mobile'/>
        }
        buttonStyle={buttonStyles}
        label="Resultados"
        style={labelStyles}
        value={0} />
      {/** Selling Guide tab **/}
      <Tab
        icon={
          <FontIcon className='icon-011-open-book-top-view tab-icon-mobile'/>
        }
        label="Guia"
        buttonStyle={buttonStyles}
        value={1}
        style={labelStyles} />
      {/** top 3 tab **/}
      <Tab
        icon={
          <FontIcon className='icon-002-star-on-top-of-podium-of-sportive-competition tab-icon-mobile'/>
        }
        label="Top 3"
        buttonStyle={buttonStyles}
        style={labelStyles}
        value={2} />
      {/** offert tab **/}
      <Tab
        icon={
          <Fire className="tab-icon-mobile-svg"/>
        }
        label="Ofertas"
        buttonStyle={buttonStyles}
        style={labelStyles}
        value={3} />
    </Tabs>
  </Paper>
);

export default BottomNavigationExampleSimple;
