import axios from "axios"
import {
  FETCH_PRODUCTS,
  FETCH_PRODUCTS_REJECTED,
  FETCH_FULLFILED,
  FETCH_COMPARISON_FULLFILED,
  FETCH_MODAL_FULLFILED,
  FETCH_MODAL_REJECTED,
  FETCH_PRODUCTS_TOP3,
  FETCH_PRODUCTS_OFFER,
  FETCH_SEARCH_FULLFILED,
  FETCH_REJECTED,
  FETCH_SEARCH_REJECTED,
  FETCH_COMPARISON_REJECTED,
  SET_PRODUCTS_TO_COMPARE,
  PRODUCT_SEARCH_INIT, PRODUCT_SEARCH_SUCCESS, PRODUCT_SEARCH_ERROR
} from '../constants/productsConst';

const gowlookApi = require('../../helpers/GowlookApi')

// fetch all products
export function fetchProducts({ categoryId, maxPrice, minPrice, ProductTypeId, brandId }) {
  const params = {
    "brandId": brandId || 0,
    "categoryId": categoryId,
    "maxPrice": maxPrice || -1,
    "minPrice": minPrice || -1,
    "productTypeId": ProductTypeId || 0,
  }
  return function(dispatch) {
    axios.post(gowlookApi.getByCategoryId(), params)
      .then(res => {
        console.log('<=== product', res.data)
        dispatch({
          type: FETCH_PRODUCTS,
          payload: res.data || []
        })
      })
      .catch(err =>
        dispatch({
          type: FETCH_PRODUCTS_REJECTED,
          payload: err
        })
      )
  }
}
// fetch all products top3
export function fetchTop3({ categoryId, maxPrice, minPrice, ProductTypeId, brandId }) {
  return function(dispatch) {
  axios.post(gowlookApi.getProductsTop3(), {
    "brandId": brandId || 0,
     "categoryId": categoryId,
     "maxPrice": maxPrice || -1,
     "minPrice": minPrice || -1,
     "productTypeId": ProductTypeId || 0
  })
      .then(res =>{
        dispatch({
          type: FETCH_PRODUCTS_TOP3,
          payload: res.data || []
        })
      }
      )
      .catch(err =>
        dispatch({
          type: FETCH_PRODUCTS_REJECTED,
          payload: err
        })
      )
  }
}

// fetch all products Offer
export function fetchOffer({ categoryId, ProductTypeId, topOffer, typeDiscount }) {
  return function(dispatch) {
  axios.post(gowlookApi.getOffer(), {
    "categoryId": categoryId || 0,
    "productTypeId": 0,
    "topOffer": topOffer || "empty",
    "typeDiscount": typeDiscount || "empty"
  })
      .then(res =>{
        dispatch({
          type: FETCH_PRODUCTS,
          payload: res.data || []
        })
      }
      )
      .catch(err =>
        dispatch({
          type: FETCH_PRODUCTS_OFFER,
          payload: err
        })
      )
  }
}

// fetch product details with product id for price panel
export function fetchProductDetails(productId) {
  return function(dispatch) {
    axios.get(gowlookApi.getProductModal(productId))
      .then((res) => {
        dispatch({
          type: FETCH_MODAL_FULLFILED,
          payload: res.data
        })

      })
      .catch((err) => {
        dispatch({
          type: FETCH_MODAL_REJECTED,
          payload: err
        })
      })
  }
}

// fetch product sheet information
export function fetchProduct(productId) {
  return function(dispatch) {
    axios.get(gowlookApi.getProduct(productId))
      .then((res) => {
        dispatch({
          type: FETCH_FULLFILED,
          payload: res.data
        })
      })
      .catch((err) => {
        dispatch({
          type: FETCH_REJECTED,
          payload: err
        })
      })
  }
}
// fetch comparison between products
export function fetchProductComparison(products) {
  let ids = products.map((el) => {
    return el.id
  });

  return function(dispatch) {
    axios.post(gowlookApi.getProductsComparison(), {
        ids: ids
      })
      .then((res) => {
        dispatch({
          type: FETCH_COMPARISON_FULLFILED,
          payload: res.data
        })
      })
      .catch((err) => {
        dispatch({
          type: FETCH_COMPARISON_REJECTED,
          payload: err
        })
      })
  }
}

export function setProductsToCompare(products) {
  return {
    type: SET_PRODUCTS_TO_COMPARE,
    payload: products
  }
}
export function fetchProductSearch(productId) {
  return function(dispatch) {
    axios.get(gowlookApi.getProductSearch(productId))
      .then((res) => {
        dispatch({
          type: FETCH_SEARCH_FULLFILED,
          payload: res.data
        })
      })
      .catch((err) => {
        dispatch({
          type: FETCH_SEARCH_REJECTED,
          payload: err
        })
      })
  }
}

const initProductSearch = () => {
  return {
    type: PRODUCT_SEARCH_INIT
  }
}

const productSearchSuccess = (payload) => {
  return {
    type: PRODUCT_SEARCH_SUCCESS,
    payload
  }
}

const productSearchError = (error) => {
  return {
    type: PRODUCT_SEARCH_ERROR,
    error
  }
}

export const productSearch = (pattern) => {
  const params = {
    categoryId: 0,
    nameSearch: pattern
  }
  const url = gowlookApi.getProductBySearch()

  return dispatch => {
    dispatch(initProductSearch())
    axios.post(url, params)
      .then((response) => {
        dispatch(productSearchSuccess(response.data))
      }).catch((error) => {
        dispatch(productSearchError(error))
      })
  }
}
