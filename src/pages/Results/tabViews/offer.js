import React from 'react'

// material ui components
import Divider from 'material-ui/Divider'
import {grey200} from 'material-ui/styles/colors'

// Grid
import { Row, Col } from 'react-flexbox-grid'

// common components
import FilOferts from '../../../components/Products/ResultProductsOfferts'

// paginator
import Pagination from 'react-js-pagination'

import uid from 'uid'

const Offer = (props) => (
  <Row style={{marginTop: '30px'}}>
    {props.products.map((item, index) => (
      <Col
        {...item}
        xs={props.viewType ==='icons' ? 6 : 12}
        md={props.viewType === 'icons' ? 4 : 12}
        key={uid()}
        >
        <FilOferts
          img={item.images[0]} {...item}
          product={item}
          customText={
            ( ( props.comparativeProducts.filter(comparate => comparate.id === item.id).length > 0 ) && props.comparativeProducts.length > 1 )
                && "Comparar ya" }
          viewType={props.viewType}
          checkboxAction={props.checkboxAction}
          unCheckAction={props.unCheckAction} />
        { props.viewType !== 'icons' &&  <Divider style={{backgroundColor: grey200}}/> }
      </Col>
    ))}
    <Col xs={12}>
        <Pagination
          innerClass="pagination-inner"
          activeClass="pagination-active"
          activePage={1}
          itemsCountPerPage={10}
          totalItemsCount={450}
          pageRangeDisplayed={5}
          onChange={console.log}
          onClick={console.log}
        />
    </Col>
  </Row>
)
export default Offer
