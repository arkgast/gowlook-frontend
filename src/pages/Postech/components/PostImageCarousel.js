import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-slick'
import uid from 'uid'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import KeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import KeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

function PostImageCarousel (props) {

  let settings = {
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      centerPadding: true
   }

   return (
     <div className='single-post-carousel'>
       <Carousel {...settings}>
         {
           props.images.map((item, index) => <div key={uid()}><img src={item} alt=""/></div>)
         }
       </Carousel>
     </div>
   )
}

function PrevArrow (props) {
  const {className, style, onClick} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={onClick} backgroundColor='rgba(255, 255, 255, 0.7)'><KeyboardArrowLeft  style={{fill:'#515151'}} /></FloatingActionButton>)
}
function NextArrow (props) {
  const {className, style, onClick} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={onClick} backgroundColor='rgba(255, 255, 255, 0.7)'><KeyboardArrowRight style={{fill:'#515151'}} /></FloatingActionButton>)
}

PostImageCarousel.propTypes = {
  images: PropTypes.array.isRequired
}

export default PostImageCarousel;
