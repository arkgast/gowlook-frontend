import React from 'react'
import './RankingChart.css'

const RankingChart = (props) => {
  var ranking = Math.round(props.ranking*100) / 100
  ranking = Math.round(ranking)
  var classes = "c100 p"+ranking
  return(
    <div
      onClick={props.onClick}
      className={classes}
      style={{cursor: 'pointer', fontSize: props.size ? props.size : '60px'}}>
        <span> {ranking} </span>
        <div className="slice">
          <div className="bar"></div>
          <div className="fill"></div>
        </div>
      </div>
  )
}

export default RankingChart;
