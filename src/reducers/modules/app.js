import {
  TOGGLE_FOOTER_GOW,
  TOGGLE_FOOTER_HISTORIES,
  TOGGLE_FOOTER_REGISTER
} from '../constants/appConts';

const initialState = {
  footerGowVisible: true,
  footerHistoriesVisibles: true,
  footerRegisterVisible: true,
}

export default function reducer(state = initialState,  action) {
  switch (action.type) {
    case TOGGLE_FOOTER_GOW:
      {
        return {
          ...state,
          footerGowVisible: action.payload
        }
      }

    break;
    case TOGGLE_FOOTER_HISTORIES:
      {
        return {
          ...state,
          footerHistoriesVisibles: action.payload
        }
      }

    break;
    case TOGGLE_FOOTER_REGISTER:
      {
        return {
          ...state,
          footerRegisterVisible: action.payload
        }
      }

    break;

    default:
      break;
  }

  return state;
}
