// chart options
const chartOptions = {
  elements: {
    point: {
      radius: 2, hitRadius: 5, hoverRadius: 5 
    } 
  },

  legend: {
    display: false
  },
  tooltips: {
      enabled : true
  },

    scales: {
        xAxes: [{
            display: false,
            //drawBorder: false,
        }],
        yAxes: [{
            //display: false,
            //drawBorder: false,
            ticks: {
                //beginAtZero: true,
                steps: 3,
                stepValue: 10,
                //max: 100
            }
        }],
    },
}

export default chartOptions
