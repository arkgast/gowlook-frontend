import React, {Component} from 'react'

// connect with router props
import {withRouter} from 'react-router-dom'

// Grid
import { Row, Col } from 'react-flexbox-grid'

//responsive component
import MediaQuery from 'react-responsive'

// material ui components
import Checkbox from 'material-ui/Checkbox';
import {fullWhite, cyan500, grey300} from 'material-ui/styles/colors';

// common componets
import RankingChart from '../../components/RankingChart'
import PriceButton from '../../components/Buttons/price'

// styles
// this style need to move to Product component folder
import '../../pages/Results/ResultsFilter/ResultItem.css'

class FilProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: props.isChecked,
      hover: false
    }
    this.onCheck = this.onCheck.bind(this)
    this.redirect = this.redirect.bind(this)
  }
  componentWillReceiveProps(nextprops) {
    this.setState({
      checked: nextprops.isChecked
    })
  }
  onCheck() {
    if(this.state.checked) {
      this.props.unCheckAction(this.props.product.id)
    } else {
      this.props.checkboxAction(this.props.product)
    }
    this.setState(() => ({ checked: !this.state.checked }))
  }
  toggleHover(value) {
    this.setState({hover: value})
  }
  redirect() {
    this.props.history.push(`/Sheet/${this.props.product.id}`)
  }
  render() {
    // json styles
    const checkboxStyles = {
      svg: {
        width: '18',
        marginTop: '0.8px',
        height:'18',
        fontWeight: 'normal',
        opacity: '0.7'
      },
      box: {
        width: '18',
        height:'18',
      },
      // this section should be depurate
      label: {
        fontWeight: 'normal',
        padding: '3px 10px',
        whiteSpace: 'nowrap',
        color: this.state.checked ? cyan500 : grey300,
      }
    }
    return (
      <Row center='xs' >
        { this.props.viewType === 'icons' ?
            <Col xs={12} lg={12}>
              <div className='ResultCard icon-view'>
                <img src={this.props.img} className="pointer" alt='Producto?' onClick={this.redirect}/>
                <h4 className="title-grey blue-hover hover-border-non-padding" onClick={this.redirect}> {this.props.product.name} </h4>
                <Row center='xs' className="column-divider-horizontal">
                  <Col xs={6} className="column-divider-vertical-right padding-10">
                    <PriceButton price={this.props.product.price} className="icon-price"/>
                  </Col>
                  <Col xs={6}>
                    <Row center="xs">
                      <RankingChart
                        onClick={this.props.rankingAction}
                        ranking={this.props.product.ranking}/>
                    </Row>

                    <Row center="xs">
                      <span className='text-grey small-font'>Ranking </span>
                    </Row>

                  </Col>
                  <Col xs={12}>
                    <Row center="xs">
                      <Col xs={12} md={6}>
                        {
                          this.props.checkboxAction &&
                            <div >
                              <Checkbox
                                onCheck={this.onCheck}
                                checked={this.state.checked || this.props.isChecked}
                                style={checkboxStyles.box}
                                labelStyle={checkboxStyles.label}
                                iconStyle={checkboxStyles.svg}
                              />
                              <span>
                                {!this.props.customText ? "Comparar" : this.props.customText }
                              </span>
                            </div>
                        }
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </Col>
            :
            <Col xs={12}>
              <div className='ResultCard list-view'>
                <Row>
                  <Col lg={2} md={3} xs={2}>
                    <img className="pointer" src={this.props.img} alt='Producto?' onClick={this.redirect}/>
                  </Col>
                  <Col lg={3} md={4} xs={7}>
                    <div>
                      <h4
                        className="text-left text-indent-list pointer title-grey-strong margin-bottom-0 blue-hover hover-border-non-padding pull-left"
                        style={{maggin: "0"}}
                        onClick={this.redirect}>
                        {this.props.product.name}
                      </h4>
                    </div>
                    <Row style={{width: "100%", marginTop: "10px", float: "left"}}>
                      <Col xs={12} md={8} lg={4}>
                        {
                          this.props.checkboxAction &&
                            <div
                              style={{
                                border: this.state.checked ? '1px solid ' + cyan500 : 'none',
                                backgroundColor: ( this.state.hover && this.props.customText ) ? cyan500 : fullWhite,
                                display: 'flex',
                                width: !this.state.checked ? '135px' : '145px'
                              }}>
                              <Checkbox
                                className={( this.state.hover && this.props.customText ) && "force-checked-color" }
                                onCheck={this.onCheck}
                                checked={this.state.checked || this.props.isChecked}
                                style={checkboxStyles.box}
                                labelStyle={checkboxStyles.label}
                                iconStyle={checkboxStyles.svg}
                              />
                              <span
                                style={{
                                  ...checkboxStyles.label,
                                  color: ( ( this.state.hover && this.props.customText ) && fullWhite) || ( this.state.checked && cyan500 ) || grey300 ,
                                }}
                                onClick={() => this.props.customText && this.props.history.push("/comparison")}
                                onMouseOver={this.toggleHover.bind(this, true)}
                                onMouseLeave={this.toggleHover.bind(this, false)}
                                className={ this.props.customText && "pointer" }>
                                {!this.props.customText ? "Comparar" : this.props.customText }
                              </span>
                            </div>
                        }
                      </Col>
                    </Row>
                  </Col>
                  <MediaQuery query="(min-width: 1025px)">
                    <Col xs={2}>
                      <Row center="xs">
                        <RankingChart
                          onClick={this.props.rankingAction}
                          ranking={this.props.product.ranking}/>
                        <Col xs={12}>
                          <p className="small-font text-grey margin-none"> Ranking </p>
                        </Col>
                      </Row>
                    </Col>
                  </MediaQuery>
                  <MediaQuery query="(min-width: 1120px)">
                    <Col
                      lg={3}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexWrap: "wrap"
                      }}>
                      <div onClick={this.props.rankingAction} className="pointer">
                        <p className="blue-title product-character" style={{margin: '0px'}}>
                          23
                        </p>
                        <p className="small-font text-grey" style={{margin: '5px 0px'}}> Megapixeles </p>
                      </div>
                    </Col>
                  </MediaQuery>
                  <Col
                    lg={2}
                    md={3}
                    xs={3}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}>
                    <PriceButton price={this.props.product.price} action={this.props.priceAction}/>
                  </Col>
                </Row>
              </div>
            </Col>
        }
      </Row>
    )
  }
}
export default  withRouter(FilProduct)
