import React, {Component} from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid';
import FilProduct from './FilterProduct.js'

export default class ResultItemWreapper extends Component {
  constructor() {
    super()
  }
  render() {
    return (
      <Row end='xs'>
        <Col xs={10}>
          <Row>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} md={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} sm={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} sm={4}>
              <FilProduct/>
            </Col>
            <Col xs={6} sm={4}>
              <FilProduct/>
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }
}
