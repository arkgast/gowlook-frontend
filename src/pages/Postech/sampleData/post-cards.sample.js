import header1 from '../../../assets/img/Blog-Web_32.jpg'
import header2 from '../../../assets/img/Blog-Web_37.jpg'
import header3 from '../../../assets/img/Blog-Web_30.jpg'
import header4 from '../../../assets/img/Blog-Web_42.jpg'
import avatar from '../../../assets/img/Blog-Web_22_B.png'

export const PostCardsSample = [
  {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    headerImg: header1,
    avatarImg: avatar,
    author: 'Joan Figueras',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
    tags: [
      {
        text: 'Electrónica',
        value: 'elect',
      },
      {
        text: 'Fotografía',
        value: 'photo',
      },
      {
        text: 'Videojuegos',
        value: 'videogame',
      }
    ]
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    headerImg: header2,
    avatarImg: avatar,
    author: 'Joan Figueras',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
    tags: [
      {
        text: 'Electrónica',
        value: 'elect',
      },
      {
        text: 'Fotografía',
        value: 'photo',
      },
      {
        text: 'Videojuegos',
        value: 'videogame',
      }
    ]
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    headerImg: header3,
    avatarImg: avatar,
    author: 'Joan Figueras',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
    tags: [
      {
        text: 'Electrónica',
        value: 'elect',
      },
      {
        text: 'Fotografía',
        value: 'photo',
      },
      {
        text: 'Videojuegos',
        value: 'videogame',
      }
    ]
  },
  {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    headerImg: header4,
    avatarImg: avatar,
    author: 'Joan Figueras',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
    tags: [
      {
        text: 'Electrónica',
        value: 'elect',
      },
      {
        text: 'Fotografía',
        value: 'photo',
      },
      {
        text: 'Videojuegos',
        value: 'videogame',
      }
    ]
  },
]

export default PostCardsSample;
