import React from 'react'

const BlueBar = (props) => (
   <div className="progress-bar">
    <div className="progress-track">
      <div className="progress-fill" style={props.style}>
        <span>{props.percent}</span>
      </div>
      <span className="progress-character text-grey">{props.character}</span>
    </div>
  </div>
)

export default BlueBar
