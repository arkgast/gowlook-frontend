import React from 'react'

// material ui color palette
import { cyan400, grey400, grey100 } from 'material-ui/styles/colors'

const HorizontalChart = (props) => (
  <div style={{height: '20px', borderRadius: '20px', backgroundColor: grey100}}>
    <div
      className="relative"
      style={{
        width: `${ props.percent }%`,
        height: '20px',
        backgroundColor: !props.grey ? cyan400 : grey400,
        borderRadius: '20px'}}>
      <span style={{right: '5px', top: '0px', position: 'absolute', color: 'white', fontSize: '13px'}}>
        {props.percent}%
      </span>
    </div>
  </div>
)

export default HorizontalChart
