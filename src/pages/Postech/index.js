import React, {Component} from 'react'
import MediaQuery from 'react-responsive'
import log from '../../assets/img/postech1.png'
import imgTopPost from '../../assets/img/Blog-Web_11.jpg'
import './Postech.css';
import PostechMenu from './components/PostechMenu'
import MenuMobile from './components/MenuMobile'
import TopPost from './components/TopPost'
import TopLatestPosts from './components/TopLatestPosts'
import CategoryPostsSection from './components/CategoryPostsSection'


// Sample data
import sampleMenuItems from './sampleData/menu-items.sample'
import postsSample from './sampleData/post-cards.sample'


// Sample sections

const sections = [
  {
    title: 'Móviles',
    menuItems: [
      {
        key: 'sm',
        label: 'Smartphone',
        isActived: true
      },
      {
        key: 'in',
        label: 'Innovación'
      },
      {
        key: 'apple',
        label: 'Apple'
      },
    ],
    posts: postsSample
  },
  {
    title: 'Drones',
    menuItems: [
      {
        key: 'sm',
        label: 'Smartphone'
      },
      {
        key: 'in',
        label: 'Innovación'
      },
      {
        key: 'apple',
        label: 'Apple',
        isActived: true
      },
    ],
    posts: postsSample
  },
  {
    title: 'Ofertas',
    menuItems: [
      {
        key: 'sm',
        label: 'Smartphone'
      },
      {
        key: 'in',
        label: 'Innovación',
        isActived: true
      },
      {
        key: 'apple',
        label: 'Apple'
      },
    ],
    posts: postsSample
  },
]

export default class Postech extends Component {

  constructor (props) {
    super(props)
    this.state = {
      menuItems: sampleMenuItems,
      topPost: {
        title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod',
        img: imgTopPost
      },
    }
  }

  render() {
      return (
        <section className='postech-container'>
          <img className="postech-logo" src={log} alt="Postech" />
          <MediaQuery query="(min-width: 600px)">
            <PostechMenu menuItems={ sampleMenuItems } />
          </MediaQuery>
          <MediaQuery query="(max-width: 600px)">
            <MenuMobile menuItems={ sampleMenuItems } />
          </MediaQuery>
          <TopPost {...this.state.topPost} />
          <TopLatestPosts posts={[postsSample[0], postsSample[1], postsSample[2]]} />
          {
            sections.map((item, index) => <CategoryPostsSection key={index} {...item} />)
          }
        </section>
      )
  }
}
