import React, {Component} from 'react'
import Paper from 'material-ui/Paper'
import Divider from 'material-ui/Divider'
import {Link} from 'react-router-dom'
import './Category.css'

class Category extends Component {
  constructor() {
    super()
    this.state = {
      hover: false
    }
    this.onHover = this.onHover.bind(this)
    this.leaveHover = this.leaveHover.bind(this)
  }
  onHover() {
    this.setState({hover: true})
  }
  leaveHover() {
    this.setState({hover: false})
  }
  render() {
     return (
        <Paper zDepth={this.state.hover ? 1 : 0} className="product-card" onMouseLeave={this.leaveHover} onMouseOver={this.onHover}>
          {this.state.hover &&
            <div className="thumbnail flex-center full-size remove-link-styles">
              <Link to={`/results/${ this.props.id }/${this.props.mainCategoryId}/${this.props.position}`}>
                <p className="hoverBtn">
                    Empezar a Comparar <i className="fa fa-signal"></i>
                </p>
              </Link>
            </div>
          }
          <div className="flex-center">
            <img src={this.props.image} alt=" " className="category-img"/>
          </div>
          <Divider/>
          <div className="category-footer">
            <span className={ `category-text ${this.state.hover ? 'hover' : ''}` }>
              {this.props.name}
            </span>
          </div>
        </Paper>

    )
  }
}

export default Category
