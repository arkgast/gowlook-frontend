const path = require('path')
const fs = require('fs')

const React = require('react')
const {Provider} = require('react-redux')
const {renderToString} = require('react-dom/server')
const {StaticRouter} = require('react-router-dom')
const {default: reducer } = require( "../src/reducers" )

const {default: getMuiTheme } = require( 'material-ui/styles/getMuiTheme' )
const {default: MuiThemeProvider } = require( 'material-ui/styles/MuiThemeProvider' )
const {default: colorPalette } = require( '../src/helpers/colorPalette' )
const {default: createStore } = require('../src/store')
const {default: App} = require('../src/App')
const muiTheme = getMuiTheme(colorPalette)

module.exports = function universalLoader(req, res) {
  const filePath = path.resolve(__dirname, '..', 'build', 'index.html')

  fs.readFile(filePath, 'utf8', (err, htmlData)=>{
    if (err) {
      console.error('read err', err)
      return res.status(404).end()
    }
    const context = {}
    const store = createStore()(reducer)
    const markup = renderToString(
      <MuiThemeProvider muiTheme={muiTheme}>
          <Provider store={store}>
            <StaticRouter
              location={req.url}
              context={context}
            >
              <App/>
            </StaticRouter>
          </Provider>
      </MuiThemeProvider>
    )

    if (context.url) {
      // Somewhere a `<Redirect>` was rendered
      redirect(301, context.url)
    } else {
      // we're good, send the response
      const RenderedApp = htmlData.replace('{{SSR}}', markup)
      res.send(RenderedApp)
    }
  })
}
