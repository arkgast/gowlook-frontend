import React from 'react';

// material ui Components
import RaisedButton from 'material-ui/RaisedButton'
import {fullWhite} from 'material-ui/styles/colors'
//import AutoComplete from 'material-ui/AutoComplete'

// icons
import Search from 'material-ui/svg-icons/action/search'
import AutoComplete from 'material-ui/AutoComplete'

import './InputSearch.css'


class MobileSearchInput extends React.Component {
  state = {
    dataSource: [],
  }

  handleUpdateInput = (value) => {
    this.setState({
      dataSource: [
        value,
        value + value,
        value + value + value,
      ],
    })
  }

  render () {
    return (<div className="relative margin-percent-5 flex-center">
              <AutoComplete
                className='mobile-menu-autocomplete'
                hintText={this.props.placeholder || "¿Que quieres comparar?"}
                dataSource={this.state.dataSource}
                onUpdateInput={this.handleUpdateInput}
                listStyle={
                  {
                    marginTop: '-20px'
                  }
                }
              />
              <div className="button-search">
                <RaisedButton fullWidth primary icon={<Search color={fullWhite}/>}/>
              </div>
          </div>

        )
  }
}

// Old MobileSearchInput component

// const MobileInputSearch = (props) => (
//   <div className="relative margin-percent-5 flex-center" style={{width: '80%'}}>
//     <AutoComplete className="input-search-mobile" placeholder="buscar"/>
//     <div className="button-search">
//       <RaisedButton fullWidth primary icon={<Search color={fullWhite}/>}/>
//     </div>
//   </div>
// )


export default MobileSearchInput;
