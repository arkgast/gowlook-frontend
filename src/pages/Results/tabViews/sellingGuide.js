import React , {Component} from 'react'

// common components
import SellingSection from '../../../components/SellGuide'
import Top3Section from '../../../components/SellGuide/top3Section'

// swipeableViews
import SwipeableViews from 'react-swipeable-views';


//Sample Data
import sampleData from './sampleSellguide.data'
import uid from 'uid'

class SellingGuide extends Component {

  constructor() {
    super()
    this.state = {}

    this.handleSlide = this.handleSlide.bind(this)
  }

  handleSlide(value, index) {
    this.setState({['slideIndex'+index]: value})
  }

  render() {

    const SellingSections = () => {
      return sampleData.map((item, index) => {
        const newIndex = (index + 1)
        const slideIndex = this.state['slideIndex' + newIndex]
        return (<SwipeableViews
                  index={slideIndex}
                  onChangeIndex={this.handleChange}
                  key={uid()}
                >
                  <SellingSection
                    right={newIndex % 2}
                    handleToggle={value => this.handleSlide(value, newIndex)}
                    slideIndex={slideIndex}
                    title={item.title}
                    content={item.content}
                    />
                  <Top3Section
                    handleToggle={value => this.handleSlide(value, newIndex)}
                    top3Products={this.props.top3Products}
                    slideIndex={slideIndex}/>
                </SwipeableViews>)
              })
    }

    return (
        <div>
          <div>
            <h2 className='blue-title'>
              Qué lavadora comprar: guía de compra de uno de los electrodomésticos más importantes
            </h2>
            <div className='text-grey'>
              <p>Las lavadoras se han vuelto un elemento imprescindible en el hogar. Y es que lavar a mano la ropa pasó a mejor vida. Ahora bien, si echas un vistazo a todas las opciones y no eres un experto en la materia, es muy posible que no sepas qué elementos debes tener en cuenta para decidir el modelo que se ajuste más a tus necesidades.</p>

              <p>Como solemos hacer en Gowlook, nuestra misión es facilitarte estas decisiones en lo que a electrónica de consumo se refiere. Es por ello que hemos creado una guía de compra para que elegir una lavadora sea lo menos complicado posible. ¿Necesitas una lavadora nueva? Pues ten en cuenta los puntos que te exponemos a continuación.</p>
            </div>

        </div>

        <div>
          { SellingSections() }
        </div>
        <div>
          <h2 className='blue-title'>
            Conclusiones: lavadora eficiente, de carga frontal y con funciones normales
          </h2>
          <p className='text-grey'>
            <p>Poniéndotelo fácil, en Gowlook te vamos a describir la lavadora perfecta desde nuestro humilde punto de vista. Y lo primero que te aconsejamos es que decidas el diseño que más se ajuste a tus necesidades: carga frontal o superior, siempre teniendo en cuenta lo que te hemos comentado a lo largo de la guía de compra.</p>

            <p>En segundo lugar, debes decidir la carga máxima de ropa que puede aceptar tu nueva lavadora. No te pilles los dedos con esta decisión y apuesta a caballo ganador: elije un modelo de siete kilogramos de carga máxima.</p>

            <p>Por último, deja a un lado las funciones llamativas y sé práctico en la decisión. ¿Realmente vas a sacar partido encender la lavadora cuando estés fuera de casa? Todas estas características encarecen el precio final y seguramente se queden en eso, una novedad muy llamativa que dispone tu modelo pero que pocas veces pondrás en práctica.</p>
          </p>
        </div>

      </div>)

  }
}

export default SellingGuide
