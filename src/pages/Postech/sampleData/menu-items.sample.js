import articles from './post-cards.sample'

const shuffleArray = (arr) => arr.sort(() => (Math.random() - 0.5))


function generateArticles () {
  return shuffleArray([
      {
        name: 'Lorem ipsum',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'dolor amet',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'eiusmod tempor',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'tempor incididunt',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'exercitation',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'officia deserunt',
        articles: shuffleArray(articles).slice(0,3)
      },
      {
        name: 'aliquip',
        articles: shuffleArray(articles).slice(0,3)
      },
    ])
}

const sampleItems = [
  {
    key: 'actualidad',
    label: 'Actualidad',
    isActived: true,
    subCategories: generateArticles()
  },
  {
    key: 'moviles',
    label: 'Móviles',
    subCategories: generateArticles()
  },
  {
    key: 'drones',
    label: 'Drones',
    subCategories: generateArticles()
  },
  {
    key: 'ofertas',
    label: 'Ofertas',
    subCategories: generateArticles()
  },
  {
    key: 'startup',
    label: 'Startups',
    subCategories: generateArticles()
  },
  {
    key: 'motor',
    label: 'Motor',
    subCategories: generateArticles()
  },
  {
    key: 'futuro',
    label: 'Futuro',
    subCategories: generateArticles()
  },
  {
    key: 'mas',
    label: 'Más'
  },
]


export default sampleItems
