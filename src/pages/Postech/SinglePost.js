import React from 'react'
import MediaQuery from 'react-responsive'
import { Link } from 'react-router-dom'
import Divider from 'material-ui/Divider'
import PostImageCarousel from './components/PostImageCarousel'
import PostArticlesCarousel from './components/PostArticlesCarousel'
import SocialNetworks from './components/SocialNetworks'
import KeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import KeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
import uid from 'uid'
import PostCard from './components/PostCard'


import avatar from '../../assets/img/Blog-Web_22_B.png'
import postsSample from './sampleData/post-cards.sample'

class SinglePost extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      carouselImages: ['http://lorempixel.com/800/400/business/', 'http://lorempixel.com/800/400/sports/', 'http://lorempixel.com/800/400/people/', 'http://lorempixel.com/800/400/city/']
    }
  }
  render () {
    return (<section className='postech-post-view'>

              <header className='postech-post-view-header'>
                <PostImageCarousel images={this.state.carouselImages} />
                <MediaQuery query='(max-width: 768px)'>
                  <h4 className='post-author'>Joan Figueras</h4>
                  <div className='post-view-navigation'>
                    <Link to='postech' title='Lorem ipsum'>
                      <KeyboardArrowLeft /> Articulo anterior
                    </Link>
                    <span>|</span>
                    <Link to='postech' title='Lorem ipsum'>
                      Articulo siguiente <KeyboardArrowRight />
                    </Link>
                  </div>
                </MediaQuery>
                <h2 className='postech-post-view-title'>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2>
              </header>

              <div className='postech-post-view-content'>

                <MediaQuery query='(min-width:800px)'>
                  <div className='post-view-left-content'>
                    <Link to="/postech/authors/@gowlook" className='post-view-left-content-avatar'>
                      <img src={avatar} alt='Joan Figueras' />
                    </Link>
                    <SocialNetworks className='post-view-social-networks-left' />
                  </div>
                </MediaQuery>

                <div className='post-view-primary-content'>

                  <MediaQuery query='(min-width:764px)'>
                    <h4 className='post-author'>Joan Figueras</h4>
                    <Divider />
                    <br />
                    <span className='post-view-date'>12 de noviembre de 2018</span>
                    <br />
                  </MediaQuery>

                  <div className='post-view-tags'>
                    <Link to='/postech/tag/electronica' title='Electrónica'>
                      Electrónica
                    </Link>
                    <Link to='/postech/tag/fotografia' title='Fotografía'>
                      Fotografía
                    </Link>
                    <Link to='/postech/tag/tendencias' title='Tendencias'>
                      Tendencias
                    </Link>
                  </div>

                  <div className='post-view-primary-text'>
                    <p>Pellentesque consequat, lacus vel elementum venenatis, metus nulla sodales sapien, ut commodo mi nisi ut mauris. Fusce lectus dolor, pulvinar sit amet mi in, rhoncus sagittis urna. Sed felis mi, facilisis et ipsum sit amet, eleifend aliquam nunc. Duis risus enim, feugiat eget suscipit sed, ultricies at eros. Sed scelerisque ipsum non est sodales commodo. Aenean nulla ipsum, molestie non tristique quis, pharetra sed urna. Aliquam eget odio justo. Nulla a velit fermentum, lobortis sapien sit amet, scelerisque diam. Aenean vitae arcu ut nisi commodo pharetra at nec ex. Nulla aliquam faucibus eros eget condimentum.</p>

                    <p>Fusce facilisis lacinia sapien et aliquet. Nunc sit amet consectetur magna. Phasellus vel malesuada magna. Praesent sed neque massa. Morbi tempor fermentum libero, eget sollicitudin nisl gravida sagittis. Aenean vitae hendrerit elit, quis varius urna. Morbi vitae diam sagittis, pulvinar tortor et, molestie odio. Nulla nec ligula eget libero sagittis tempus. Etiam non justo non turpis placerat molestie. Curabitur dictum vitae lorem vel pharetra. Donec vel auctor urna, tincidunt commodo nibh. Etiam sapien est, viverra et elementum sed, tempor semper ipsum. Integer quis neque sit amet elit ultricies lobortis. Fusce venenatis ac ipsum lacinia hendrerit. Pellentesque non egestas tortor. Sed dictum, nisi at pharetra gravida, lectus sem sagittis justo, sit amet aliquet tellus ante ac purus.</p>

                    <p>Nullam efficitur rhoncus metus, vel facilisis arcu cursus in. Nam a faucibus enim. Sed dictum mattis lacus, non cursus lacus ultrices vel. Duis malesuada aliquam ligula, et lobortis est hendrerit ut. Nam suscipit mi sed dictum volutpat. Duis ullamcorper gravida arcu ac aliquet. Integer tortor odio, egestas quis orci sed, porta malesuada turpis. In hac habitasse platea dictumst. Maecenas nec lacus et libero aliquam dignissim sit amet id lorem. Donec felis sem, accumsan id ex et, fermentum rhoncus justo. Curabitur eu euismod elit, a imperdiet mauris.</p>

                    <p>Quisque ornare purus sodales, sodales dui non, lacinia nisi. Donec vulputate nunc quam, non consequat orci pharetra ac. Phasellus eu ante erat. Donec sit amet tortor ut ex porta venenatis et hendrerit odio. Phasellus eget rhoncus neque. Suspendisse eget metus tellus. Etiam nec dolor fringilla, pulvinar ipsum non, gravida ante. Quisque egestas pretium interdum. Morbi cursus odio neque, ac fringilla augue dignissim in. Curabitur placerat rhoncus nisi, consequat congue urna. Suspendisse dui mi, mattis tincidunt volutpat non, viverra in lectus. Etiam tincidunt convallis nibh ac dignissim. Quisque a rhoncus neque. Curabitur in dui magna.</p>

                    <p>Nulla tristique odio sit amet imperdiet lobortis. Pellentesque mollis massa in nulla vulputate, non eleifend leo efficitur. Nullam non dui at nibh mollis vehicula. Suspendisse sit amet luctus sapien, vel aliquam lectus. Maecenas a feugiat turpis. Praesent gravida, lorem ut ultrices consequat, nulla lorem tempor purus, at mollis nisi erat a dolor. Donec dapibus ultricies tortor nec porta. Sed nec leo sed erat efficitur gravida vehicula in lectus.</p>
                  </div>
                  <MediaQuery query='(max-width: 764px)'>
                    <SocialNetworks className='post-view-social-networks-mobile' />
                  </MediaQuery>
                  <br />
                  <div className='post-view-navigation'>
                    <Link to='postech' title='Lorem ipsum'>
                      <KeyboardArrowLeft /> Articulo anterior
                    </Link>
                    <span>|</span>
                    <Link to='postech' title='Lorem ipsum'>
                      Articulo siguiente <KeyboardArrowRight />
                    </Link>
                  </div>
                </div>
              </div>

              <MediaQuery query='(max-width: 764px)'>
                <br />
                <br />
                <br />
                <Divider />
              </MediaQuery>

              <MediaQuery query='(min-width: 764px)'>
                <br />
                <br />
                <br />
              </MediaQuery>

              <div className='post-view-other-articles'>
                <h4>No te pierdas las mejores historias en fotografía</h4>
                <MediaQuery query='(min-width: 764px)'>
                  <PostArticlesCarousel posts={postsSample} />
                </MediaQuery>
                <MediaQuery query='(max-width: 764px)'>
                  <div className='post-view-other-articles-content'>
                    {postsSample.map(item => <PostCard key={uid()} {...item} />)}
                  </div>
                </MediaQuery>
              </div>


            </section>)
  }
}


export default SinglePost
