
const gowlookapi = require('./GowlookApi');
import axios from 'axios';

module.exports = {
    findCategoryById : function(listOfCategories, categoryId){
    	let result = listOfCategories.find(function(el){
    		return el.id == categoryId;
    	});

    	return result;
    },

    findProductsByCategoryId : function(categoryId, element){

    	axios.get(gowlookapi.getByCategoryId(categoryId))
	        .then(res => {
	         var c = res.data.content.map(obj => obj)
	         var prod = element.state.products.concat(c);
	          element.setState({
	            products : prod,
	            max : prod.length,
	            currentList : prod.slice(0,12),
	          });

	          /*console.log(element);*/
	          
	    });

	    
    },

    findProductsTop3 : function(categoryId, element){

    	axios.get(gowlookapi.getProductsTop3())
	        .then(res => {
	         var c = res.data.content.map(obj => obj)
	         var prod = element.state.products.concat(c);
	          element.setState({
	            products : prod,
	            max : prod.length,
	            currentList : prod.slice(0,12),
	          });

	          /*console.log(element);*/
	          
	    });

	    
    }



}