import React from 'react'
import './PercentBar.css'

const PercentBar = (props) => {
  return (
    <div className='percent-bar'>
      <div style={{width: `${props.percent}%`}}></div>
      <div>{props.percent}%</div>
    </div>
  )
}

export default PercentBar
