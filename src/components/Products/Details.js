import React, { Component } from 'react'

//grid
import { Row, Col } from 'react-flexbox-grid'
import {Tabs, Tab} from 'material-ui/Tabs'

// material ui components
import Dialog from 'material-ui/Dialog'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu'
import FlatButton from 'material-ui/FlatButton'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down'
import Pagination from 'react-js-pagination'

// material ui color palette
import {
  lightBlue200,
  grey200,
  cyan400
} from 'material-ui/styles/colors';

// material ui icons
import Cross from 'material-ui/svg-icons/navigation/close'

// SwipeableViews
import SwipeableViews from 'react-swipeable-views';

// common componets
import HorizontalChart from '../horizontalChart'
import RankingChart from '../RankingChart'
import SimpleCard from '../SimpleCard'

// helpers
import chartOptions from '../../helpers/chartOptions'

// assets img
import map from '../../assets/img/map.jpg'
import cloud from '../../assets/img/cloud.jpg'
import amazon from '../../assets/img/amazon.png'

// Styles files
import './styles/Details.css'

//charts
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, /*Legend*/} from 'recharts';

// hardcode data
const data = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];

const LineChart2 = require("react-chartjs-2").Line
const chartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(40, 146, 240, 0.3)",
            borderColor: "rgba(40, 146, 240,1)",
            pointBorderColor: "rgba(40, 146, 240,1)",
            //pointStrokeColor: "#fff",
            //pointHighlightFill: "#fff",
            //pointHighlightStroke: "rgba(220,220,220,1)",
            data: [10, 7,6, 5, 4, 3, 3],
            fill: false
        }
    ]
}
//------- Styles Json---------
const labelStyles = {
  textTransform: 'capitalize',
}
const inkBarStyle = {
  backgroundColor: lightBlue200,
  height: '3px',
}
const buttonStyles = {
  flexDirection: 'row',
  height: '55px',
  width: 'auto',
}
const tabsStyles = {
  width: '300px',
  boxSizing: 'border-box',
}
//-----------------------
//----------------------

//Es6 Component
class Details extends Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }



    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.handleRequestClose = this.handleRequestClose.bind(this)
  }
  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
  }

  handleRequestClose(){
    this.setState({
      open: false,
    })
  };
  render() {
    return (
      <Dialog
        autoScrollBodyContent
        modal
        open={this.props.open} >



        <Row>
          <Col xs={3} className="flex-center" style={{borderRight: '1px solid ' + grey200}}>
            <Row center="xs">
              <Col xs={12}>
                <h4 className='product-name'>
                  {this.props.product.name }
                </h4>
              </Col>
              <Col xs={12}>
                <FlatButton
                  className='button-with-arrow-down'
                  style={{margin: '0px 0px 20px 0px'}}
                  labelStyle={{textTransform: 'capitalize'}}
                  onTouchTap={this.handleTouchTap}
                  label="3GB Memory">
                  <KeyboardArrowDown style={{fill: cyan400 }} />
                </FlatButton>
                <Popover
                  open={this.state.open}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                  targetOrigin={{horizontal: 'left', vertical: 'top'}}
                  onRequestClose={this.handleRequestClose}
                >
                  <Menu>
                    <MenuItem primaryText="Refresh" />
                    <MenuItem primaryText="Help &amp; feedback" />
                    <MenuItem primaryText="Settings" />
                    <MenuItem primaryText="Sign out" />
                  </Menu>
                </Popover>
              </Col>
              <Col xs={12} style={{marginBottom: '10px'}}>
                <img
                  style={{maxWidth: '100%', height: '100px'}}
                  src={this.props.product.images && this.props.product.images[0]}
                  alt={this.props.product.name}/>
              </Col>
              <RankingChart ranking={this.props.product.ranking}/>
              <Col xs={12}>
                <p className="small-font text-grey margin-none"> Ranking </p>
              </Col>
              <Col xs={12}>
                <FlatButton
                  primary
                  style={{border: '1px solid' + cyan400, marginTop: '10px'}}
                  labelStyle={{textTransform: 'capitalize'}}
                  fullWidth
                  icon={<FontIcon className="icon-018-turn-notifications-on-button small-icon"/>}
                  label="Alarm" />
              </Col>
            </Row>
          </Col>
          <Col xs={9} className="relative">
            <IconButton
              iconStyle={{width: '15px', height: '15px'}}
              onTouchTap={this.props.handleClose}
              style={{position: 'absolute', right: '5px', top: '-15px', width: '15px', height: '15px'}}>
              <Cross/>
            </IconButton>
            <Tabs
              onChange={this.props.handleChange}
              style={{borderBottom: '1px solid ' + grey200}}
              inkBarStyle={inkBarStyle}
              tabItemContainerStyle={tabsStyles}
              value={this.props.slideIndex} >
              <Tab
                icon={
                  <FontIcon className="icon-031-cash tab-icon"/>
                }
                style={labelStyles}
                buttonStyle={buttonStyles}
                label="Precios"
                value={0} />
              <Tab
                icon={
                  <FontIcon className="icon-017-page-ranking tab-icon"/>
                }
                style={labelStyles}
                buttonStyle={buttonStyles}
                label="Ranking"
                value={1} />
              <Tab
                icon={
                  <FontIcon className="icon-032-like tab-icon"/>
                }
                style={labelStyles}
                buttonStyle={buttonStyles}
                label="Reputación"
                value={2} />
            </Tabs>
            <SwipeableViews
              index={this.props.slideIndex}
              onChangeIndex={this.props.handleChange} >
              <div className="dialog-body">
                <span className="small-font text-grey">Price Evolution <i className='fa fa-share-square-o' style={{fontSize: 16 }}></i></span>
                <br />
                <br />
                <LineChart2 data={chartData} options={chartOptions} width={250}
                 height={50}/>
               <div className='align-right'>
                 <span className='small-font text-grey'>Ordenar por: </span>
                 <FlatButton
                   className='sort-button'
                   style={{margin: '0px 0px 20px 0px'}}
                   labelStyle={{textTransform: 'capitalize'}}
                   onTouchTap={(ev) => this.setState({openSortPopover: true, sortPopover: ev.currentTarget})}
                   label="Ordenar">
                   <KeyboardArrowDown style={{fill: cyan400 }} />
                 </FlatButton>
                 <Popover
                   open={this.state.openSortPopover}
                   anchorEl={this.state.sortPopover}
                   anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                   targetOrigin={{horizontal: 'left', vertical: 'top'}}
                   onRequestClose={() => this.setState({openSortPopover: false})}
                 >
                   <Menu>
                     <MenuItem primaryText="Mayor Precio" />
                     <MenuItem primaryText="Menor Precio" />
                     <MenuItem primaryText="Relevancia" />
                   </Menu>
                 </Popover>
               </div>
               <Row style={{marginTop: '30px', textAlign: 'center'}}>
                 {
                   this.props.product.historyPrices &&
                     this.props.product.historyPrices.map(( item, index ) => (
                     <Col xs={3} key={index}>
                       <p style={{color: cyan400, textAlign: 'center'}}>{item.price}€</p>
                       <img src={item.providerResponse.logo} alt={item.providerResponse.name} style={{maxWidth: '100%', height: '30px'}}/>
                       <RaisedButton
                         primary
                         labelStyle={{textTransform: 'capitalize'}}
                         label="Comprar"
                       />
                     </Col>
                   ))
                 }
               </Row>
               <div className="right-align">
                 <span>Mostrar: </span>
                 <FlatButton
                   className='sort-button'
                   style={{margin: '0px 0px 20px 0px', width: '50px !important', minWidth: '50px !important'}}
                   labelStyle={{textTransform: 'capitalize'}}
                   onTouchTap={(ev) => this.setState({openNumberOfPagesPopover: true, sortPopover: ev.currentTarget})}
                   label="8">
                   <KeyboardArrowDown style={{fill: cyan400 }} />
                 </FlatButton>

                 <Pagination
                   innerClass="pagination-inner price-tab-paginator"
                   activeClass="pagination-active"
                   activePage={1}
                   itemsCountPerPage={10}
                   totalItemsCount={450}
                   pageRangeDisplayed={5}
                   onChange={() => { /*Do someting*/ }}
                   />
                   <Popover
                     open={this.state.openNumberOfPagesPopover}
                     anchorEl={this.state.sortPopover}
                     anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                     targetOrigin={{horizontal: 'left', vertical: 'top'}}
                     onRequestClose={() => this.setState({openNumberOfPagesPopover: false})}
                   >
                     <Menu>
                       <MenuItem primaryText="8" />
                       <MenuItem primaryText="12" />
                       <MenuItem primaryText="24" />
                     </Menu>
                   </Popover>
               </div>

              </div>
              <div className="dialog-body">
                <Row style={{margin: '0px'}}>
                  <Col xs={6}>
                    <p>Caracteristica</p>
                    <HorizontalChart percent={30}/>
                  </Col>
                  <Col xs={6}>
                    <p>Caracteristica</p>
                    <HorizontalChart percent={78} grey/>
                  </Col>
                  <Col xs={6}>
                    <p>Caracteristica</p>
                    <HorizontalChart percent={90}/>
                  </Col>
                  <Col xs={6}>
                    <p>Caracteristica</p>
                    <HorizontalChart percent={78} grey/>
                  </Col>
                </Row>
              </div>
              <div className="dialog-body">
                <Row style={{margin: '0px'}}>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                    </SimpleCard>
                  </Col>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                    </SimpleCard>
                  </Col>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                      <LineChart width={200} height={100} data={data}
                        margin={{ top: 10, right: 0, left: 0, bottom: 5 }}>
                        <XAxis dataKey="name" />
                        <YAxis />
                        <CartesianGrid strokeDasharray="3 3" />
                        <Tooltip />
                        <Line type="monotone" dataKey="pv" stroke="#8884d8" />
                        <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
                      </LineChart>
                    </SimpleCard>
                  </Col>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                      <img src={cloud} alt=" " style={{width: '100%', height: '91px'}}/>
                    </SimpleCard>
                  </Col>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                      <Row style={{margin: '0px'}}>
                        <Col xs={4}>
                          <p style={{margin: '0px'}}>char</p>
                        </Col>
                        <Col xs={8}>
                          <HorizontalChart percent={30}/>
                        </Col>
                        <Col xs={4}>
                          <p style={{margin: '0px'}}>char</p>
                        </Col>
                        <Col xs={8}>
                          <HorizontalChart percent={78}/>
                        </Col>
                        <Col xs={4}>
                          <p style={{margin: '0px'}}>char</p>
                        </Col>
                        <Col xs={8}>
                          <HorizontalChart percent={90}/>
                        </Col>
                      </Row>
                    </SimpleCard>
                  </Col>
                  <Col xs={6}>
                    <SimpleCard header="Menciones del producto">
                      <img src={map} alt=" " style={{width: '100%', height: '91px'}}/>
                    </SimpleCard>
                  </Col>
                </Row>
              </div>
            </SwipeableViews>
          </Col>
        </Row>
      </Dialog>
    )
  }
}

export default Details
