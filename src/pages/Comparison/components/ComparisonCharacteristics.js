import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-slick'
import uid from 'uid'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import emitterClient from '../../../services/emitter'

class ComparisonCharacteristics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      carouselRef: 'characteristics',
      openCollapsible: true
    }

  }

  componentDidMount () {
    emitterClient.on('carousel:setIndex', (currentSlide) => {
        this.refs[this.state.carouselRef].slickGoTo(currentSlide)
    })
  }

  componentWillUnMount () {
    eventEmitter.removeEventListener('carousel:setIndex')
  }

  render () {
    return (<div className='comparison-characteristics'>

        <h3
          className='comparison-characteristics-title'
          onClick={()=> this.setState({openCollapsible: !this.state.openCollapsible})}>
          <i className={this.state.openCollapsible ? 'fa fa-angle-up' : 'fa fa-angle-down'}></i> Imagen de sensor</h3>

        {
          this.state.openCollapsible && (
            <div>
              <div className='comparison-characteristic-labels'>
                <div>
                  <h5>Megapíxeles</h5>
                  <h5>Tamaño del sensor</h5>
                  <h5>Megapíxeles</h5>
                </div>
              </div>

              <div className='comparison-characteristic-values'>
                <Carousel {...this.props.carouselSettings} ref={this.state.carouselRef}>
                {
                  Array(7)
                  .fill(null)
                  .map(() =>
                  (<div className='comparison-characteristic-value' key={uid()}>
                    <div>32 MP</div>
                    <div>26,2 MM</div>
                    <div>CMOS</div>
                  </div>))
                }
                </Carousel>
              </div>
            </div>
          )
        }

    </div>)
  }
}

ComparisonCharacteristics.propTypes = {
  carouselSettings: PropTypes.object.isRequired
}

export default ComparisonCharacteristics
