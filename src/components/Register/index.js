import React from 'react'
import BlueButton from '../BlueButton'
import {grey700} from 'material-ui/styles/colors'
import './Register.css'

const Register = (props) => (
  <div className="pull-left relative flex-wrap full-width" style={{height: '330px', paddingTop: '40px'}}>
    <div>
      <h1 className="text-center full-width" style={{fontWeight: 'normal', color: grey700}}>
        <div className="invert-word">¿</div>Quieres experimentar la emoción de elegir bien?
      </h1>
      <div className="flex-center" style={{marginTop: '50px'}}>
        <BlueButton text="Registrarme Gratis" action={props.registerAction} size="60px" fontSize="18px"/>
      </div>
    </div>
  </div>
)
export default Register
