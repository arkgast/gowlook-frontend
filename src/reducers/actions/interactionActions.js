import {
  SET_COMPARATE_PRODUCTS,
  SELECT_DETAILS_PRODUCT,
  REMOVE_COMPARATE_PRODUCT,
  CLEAR_COMPARATIVE
} from '../constants/interactionConst';

export function setProductToCompare(product) {
  return {
    type: SET_COMPARATE_PRODUCTS,
    product: product
  }
}

export function removeProductToCompare(productId) {
  return {
    type: REMOVE_COMPARATE_PRODUCT,
    productId: productId
  }
}

export function selectProductDetails(product) {
  return {
    type: SELECT_DETAILS_PRODUCT,
    selectedProduct: product
  }
}
export function clearProducts() {
  return {
    type: CLEAR_COMPARATIVE,
  }
}
