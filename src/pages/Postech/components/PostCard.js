import React from 'react'
import Paper from 'material-ui/Paper'
import PropTypes from 'prop-types'
import { Col, Row } from 'react-flexbox-grid'
import MediaQuery from 'react-responsive'
import { Link } from 'react-router-dom'

function PostCard (props) {

  return (<Paper className='postech-post-card'>
            <header className='postech-post-card-header'>
              <img className='header-image' src={props.headerImg} alt={props.title} />

              <MediaQuery query="(min-width: 640px)">
                <img className='post-avatar' src={props.avatarImg} alt={props.author} />
                <h3 className='post-author'>{props.author}</h3>
              </MediaQuery>
            </header>
            <div className='postech-post-card-content'>
              <h2>
                  <Link to='postech/tecnologia/lorem-ipsum-dolor-sit-amet-consectetur-adipisicing-elit'>{ props.title }</Link>
              </h2>
              <MediaQuery query="(max-width: 640px)">
                <h3 className='post-author'>{props.author}</h3>
              </MediaQuery>
              <MediaQuery query="(min-width: 640px)">
                <p>
                  {props.content}
                </p>
                <div className='post-tags'>
                  <Row start="md">
                    { props.tags.map((item, index) => <Col key={index} className='post-tag'>
                      <Link to='/our-team'>{item.text}</Link>
                    </Col>) }
                  </Row>
                </div>
              </MediaQuery>


            </div>
          </Paper>)

}

PostCard.propTypes = {
  headerImg: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  avatarImg: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  content : PropTypes.string.isRequired,
  tags: PropTypes.array.isRequired,
}

export default PostCard
