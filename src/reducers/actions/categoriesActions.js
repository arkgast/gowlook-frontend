import axios from "axios"
import {/*FETCH,*/ FETCH_FULLFILED, FETCH_REJECTED} from '../constants/categoriConst'

const gowlookApi = require('../../helpers/GowlookApi')
export function fetchCategories(limit) {
  return function(dispatch) {
    axios.get(gowlookApi.getCategories(), {params: {size: limit}})
      .then((res) => {
        const categories = res.data.content.map(obj => obj);

        let mainCategories = [];
        let subCategories = {};
        let cat = [];
        //let subCategoriesId = [];

        categories.map((category, key) => {
          if (category.parentId === -1) {
            mainCategories.push(category);
          } else {
            if (typeof subCategories[category.parentId] === 'undefined') {
              subCategories[category.parentId] = [];
            }

            subCategories[category.parentId].push(category);
          }

          cat.push(category);
          return cat;
        })

        dispatch({
          type: FETCH_FULLFILED,
          payload: {
            mainCategories: mainCategories,
            subCategories: subCategories,
            categories: cat
          }
        })

      })
      .catch((err) => {
        dispatch({
          type: FETCH_REJECTED,
          payload: err
        })
      })
  }
}
