import React from 'react'
import PropTypes from 'prop-types'
import ReactStars from 'react-stars'
import PercentBar from './PercentBar'
import ComparisonComments from './ComparisonComments'
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu'
import FlatButton from 'material-ui/FlatButton'
import MenuItem from 'material-ui/MenuItem'
import avatar from '../../../assets/img/profileHistory.png'
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import Carousel from 'react-slick'
import uid from 'uid'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import emitterClient from '../../../services/emitter'

class ComparisonBottom extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showingForComment: false,
      sliderValue: 50,
      carouselRef: 'bottomCarousel'
    }

  }

  componentDidMount () {
    emitterClient.on('carousel:setIndex', (currentSlide) => {
        this.refs[this.state.carouselRef].slickGoTo(currentSlide)
    })
  }

  componentWillUnMount () {
    eventEmitter.removeEventListener('carousel:setIndex')
  }

  render () {

  return(
    <div className='comparison-bottom'>
      <div className='comparison-bottom-products'>
        <h3> ¿Cúal crees que es el mejor?</h3>
        <Carousel {...this.props.carouselSettings} ref={this.state.carouselRef}>
          {
            Array(7).fill(null).map(()=>(
              <div className='comparison-bottom-product' key={uid()}>
                <button className='blue-line-button'>Votar</button>
                <img src='https://s3-us-west-2.amazonaws.com/gowlook/categories/camaras' alt='camera' />
                <h4>Canon 550</h4>
                <ReactStars
                  className='comparison-bottom-product-stars'
                  count={5}
                  value={4}
                  size={10}
                  edit={false}
                  color2={'#0094cc'} />
              </div>
            ))
          }
        </Carousel>
      </div>


      <div className='comparison-bottom-vote'>

        <div className='comparison-bottom-vote-product'>
          <h3>Vota y opina</h3>

          <h4>Canon EOS 5D Mark III</h4>
          <img src='https://s3-us-west-2.amazonaws.com/gowlook/categories/camaras' alt='camera' />
          <span className='text-blue'>Valoración de usuarios</span>
          <ReactStars
            className='comparison-bottom-product-stars'
            count={5}
            value={4}
            size={10}
            edit={false}
            color2={'#0094cc'} />
          <span className='text-grey'>4.3 basado en 13 reseñas</span>
          {
            this.state.showingForComment && (<div>
              <br />
              <FlatButton
                className='sort-button'
                style={{margin: '0px 0px 20px 0px'}}
                labelStyle={{textTransform: 'capitalize'}}
                onTouchTap={(ev) => this.setState({openResena: true, selectResena: ev.currentTarget})}
                label="Mostrar las reseñas">
                <KeyboardArrowDown style={{fill: '#00b5c8' }} />
              </FlatButton>
              <Popover
                open={this.state.openResena}
                anchorEl={this.state.selectResena}
                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                onRequestClose={() => this.setState({openResena: false})}
              >
                <Menu>
                  <MenuItem primaryText="Lorem ipsum" />
                  <MenuItem primaryText="Lorem ipsum" />
                  <MenuItem primaryText="Lorem ipsum" />
                </Menu>
              </Popover>
            </div>)

          }
        </div>

        <div className='comparison-bottom-vote-comment'>
          {
            !this.state.showingForComment && (<div className='comparison-bottom-vote-step1'>
              <h4>Lo que han votado los demas usuarios:</h4>
              <div className='comparison-bottom-vote-features'>
                <div>
                  <span>Feature 1</span><div><PercentBar percent={89} /></div>
                </div>
                <div>
                  <span>Feature 2</span><div><PercentBar percent={38} /></div>
                </div>
                <div>
                  <span>Feature 3</span><div><PercentBar percent={76} /></div>
                </div>
                <div>
                  <span>Feature 4</span><div><PercentBar percent={97} /></div>
                </div>
                <div>
                  <span>Feature 5</span><div><PercentBar percent={57} /></div>
                </div>
              </div>
              <div>
                <button className='blue-line-button' onClick={() => this.setState({showingForComment: true})}>
                  Puntuar y escribir un comentario
                </button>
              </div>
            </div>)
          }


          {
            this.state.showingForComment && (<div className='comparison-bottom-vote-step2'>
              <div className='comparison-bottom-comment-author'>
                <img src={avatar} alt='' />
                <div>
                  <h4 className='text-grey'>Lorem ipsum</h4>
                  <div className='text-blue small-font'>Gowlooker VIP</div>
                </div>
              </div>
              <div>
                <FlatButton
                  className='sort-button'
                  style={{margin: '0px 0px 20px 0px', border: 'none !important', borderBottom: 'thin solid #d3d3 !important',  width: '100%'}}
                  labelStyle={{textTransform: 'capitalize'}}
                  onTouchTap={(ev) => this.setState({openSelectInComment: true, selectComment: ev.currentTarget})}
                  label="Puntuar todo">
                  <KeyboardArrowDown style={{fill: '#00b5c8' }} />
                </FlatButton>
                <Popover
                  open={this.state.openSelectInComment}
                  anchorEl={this.state.selectComment}
                  anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                  targetOrigin={{horizontal: 'left', vertical: 'top'}}
                  onRequestClose={() => this.setState({openSelectInComment: false})}
                >
                  <Menu>
                    <MenuItem primaryText="Puntuar todo" />
                    <MenuItem primaryText="Lorem ipsum" />
                    <MenuItem primaryText="Lorem ipsum" />
                    <MenuItem primaryText="Lorem ipsum" />
                  </Menu>
                </Popover>
              </div>
              <div>
                <Slider
                  className='inline-block'
                  min={0}
                  max={100}
                  value={this.state.sliderValue}
                  style={{width: '92%', height: 35}}
                  onChange={(ev, value) => this.setState({sliderValue: parseInt(value, 10)})} />
                &nbsp;<span className='text-grey'>{this.state.sliderValue}%</span>
              </div>
              <div>
                <TextField
                  floatingLabelText="Comentario"
                  multiLine={true}
                  style={{width: '100%'}}
                  rows={5}
                />
              </div>
              <div className='right-align'>
                <button className='blue-line-button'>Publicar</button>
              </div>
            </div>)
          }


        </div>

      </div>


      <ComparisonComments />

    </div>)
  }
}

ComparisonBottom.propTypes = {
  carouselSettings: PropTypes.object.isRequired
}

export default ComparisonBottom;
