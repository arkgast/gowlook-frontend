import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import {lightBlue900} from 'material-ui/styles/colors'

const BlueButton = ({ text, size, fontSize, action, fullWidth }) => (
  <FlatButton
    fullWidth={fullWidth}
    className="hover-white"
    style={{borderColor: '#00b5c8', border: '1px solid', height: size}}
    labelStyle={{textTransform: 'capitalize', fontSize: fontSize}}
    hoverColor={'#00b5c8'}
    onTouchTap={action}
    label={text}
    rippleColor={lightBlue900}
    primary />
)

export default BlueButton
