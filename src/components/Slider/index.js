import React, {Component} from 'react'
import ReactBootstrapSlider from 'react-bootstrap-slider';
import './Slider.css'

class Slider extends Component {
  constructor() {
    super()
    this.state = {
      currentValue: 1,
      step : 1,
      max : 50,
      min : 0
    }
  }

  changeValue(){

  }

  render() {

    return (
      <ReactBootstrapSlider
          value={this.state.currentValue}
          change={this.changeValue}
          slideStop={this.changeValue}
          step={this.state.step}
          max={this.state.max}
          min={this.state.min}
          orientation={this.props.orientation || "horizontal" }
          style={this.props.style || {}}
          reversed={false} />
    )
  }
}

export default Slider;
