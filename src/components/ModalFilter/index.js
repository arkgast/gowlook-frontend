import React from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import { Grid, Row } from 'react-flexbox-grid'
import { List, ListItem } from 'material-ui/List'
import Checkbox from 'material-ui/Checkbox'

import SideBarMenu from '../SideBarMenu'
import './styles.css'

const ModalFilter = ({ open, styles, mainCategories, subCategories }) => {
  const clsName = open ? 'active' : ''
  return (
    <div className={`modal-filter ${clsName}`} style={styles.modalFilter}>
      <Grid>
        <Row>
          <Paper style={styles.container} className='content-container'>

            <div style={styles.menu} className='menu'>
              <SideBarMenu categories={mainCategories} />
            </div>

            <div style={styles.content} className='content'>
              <List>
                {mainCategories.map((category, i) => {
                  return (
                    <ListItem
                      key={i}
                      primaryText={category.name}
                      nestedItems={subCategories[category.id].map((subCategory, j) => {
                        return (
                          <ListItem
                            key={j}
                            primaryText={subCategory.name}
                          />
                        )
                      })}
                    />
                  )
                })}
              </List>
            </div>

          </Paper>
        </Row>
      </Grid>
    </div>
  )
}

ModalFilter.defaultProps = {
  styles: {
    modalFilter: {},
    contentContainer: {},
    menu: {},
    content: {}
  }
}

ModalFilter.propTypes = {
  styles: PropTypes.shape({
    modalFilter: PropTypes.object,
    contentContainer: PropTypes.object,
    menu: PropTypes.object,
    content: PropTypes.object
  }),
}

export default ModalFilter
