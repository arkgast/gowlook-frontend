import React , {Component} from 'react';

// flex
import { Row, Col } from 'react-flexbox-grid'

// material ui components
import Dialog from 'material-ui/Dialog';
import {List, ListItem} from 'material-ui/List'
import Checkbox from 'material-ui/Checkbox'
import Divider from 'material-ui/Divider'
import {
  grey500,
  cyan400,
} from 'material-ui/styles/colors'
import {Tabs, Tab} from 'material-ui/Tabs';
import IconButton from 'material-ui/IconButton'
//import FlatButton from 'material-ui/FlatButton'
import Cross from 'material-ui/svg-icons/navigation/close'

// SwipeableViews
import SwipeableViews from 'react-swipeable-views';

// common components
import BlueButton from '../BlueButton'

class FilterDialog extends Component {
  constructor() {
    super()
    this.state = {
      radioValue: [],
      listView: false,
      openResultTab: 0
    }
    this.handleChange = this.handleChange.bind(this)
    this.onChangeRadio = this.onChangeRadio.bind(this)
    this.apply = this.apply.bind(this)
  }
  handleChange(value) {
    this.setState({
      openResultTab: value,
    })
  }
  apply() {
    this.props.handleClose()
  }
  onChangeRadio(event){
    const value = event.target.value ? parseInt( event.target.value, 10) : 0;
    if( this.state.radioValue.indexOf(value) > -1) {
      this.setState({radioValue: this.state.radioValue.filter(item => item !== value)})
    }
    else {
      this.setState({ radioValue: [ ...this.state.radioValue, value ] })
    }
    console.log(this.state.radioValue)

  }
  render() {
    // json styles
    const labelStyles = {
      textTransform: 'capitalize',
    }
    const buttonStyles = {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      height: '55px',
      paddingLeft: '20px',
      width: 'auto',
    }
    const inkBarStyle = {
      backgroundColor: 'rgb(0, 176, 198)',
      height: '3px',
    }
    const tabsStyles = {
      width: '280px',
      boxSizing: 'border-box',
    }
    return (
          <Dialog className="relative" openSecondary open={this.props.open} autoScrollBodyContent>
              <IconButton
                onTouchTap={this.props.handleClose}
                iconStyle={{width: '20px', height: '20px'}}
                style={{position: 'absolute', right: '35px', top: '10px', width: '20px', height: '20px'}}>
                <Cross/>
              </IconButton>
              <Tabs
                onChange={this.handleChange}
                inkBarStyle={inkBarStyle}
                tabItemContainerStyle={tabsStyles}
                value={this.state.openResultTab}
              >
                {
                  this.props.filters.map(( filter, index ) => (
                    <Tab
                      value={index}
                      key={index}
                      buttonStyle={buttonStyles}
                      className="blue-hover hover-border-tab"
                      style={labelStyles}
                      label={filter.name} />
                  ))
                }
              </Tabs>
              <Divider/>
              <SwipeableViews
                style={{marginTop: "20px"}}
                index={this.state.openResultTab}
                onChangeIndex={this.handleChange} >
              {
                this.props.filters.map(( filter, index ) => (
                  <div>
                    <Row style={{margin: "0px"}}>
                    {
                        this.props[filter.filterType].chunk_inefficient(4).map(( filterArray, indexArray ) => (
                          <Col xs={6}>
                              <List>
                                {
                                  filterArray.map((item, key) => (
                                    <ListItem
                                      key={key}
                                      className="blue-hover"
                                      primaryText={index !== 6 ? item.name : "ver mas"}
                                      innerDivStyle={{padding: '16px 16px 16px 50px'}}
                                      style={{
                                        color: this.state.radioValue.indexOf(item.id) > -1 ? cyan400 : grey500, marginTop: '-15px',
                                        fontSize: "14px"
                                      }}
                                      leftCheckbox={
                                        <Checkbox
                                          value={item.id}
                                          onCheck={this.onChangeRadio}
                                          checked={this.state.radioValue.indexOf(item.id) > -1}
                                        />
                                      }
                                    />
                                  ))
                                }
                              </List>
                          </Col>
                        ))
                      }
                      </Row>
                    </div>
                ))
              }
            </SwipeableViews>
            <Row end="xs" style={{margin: '0px'}}>
              <span
                onClick={this.props.handleClose}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '40px',
                  marginRight: '20px',
                }}>
                <span className="text-grey blue-hover hover-border-non-padding" >
                  Cancelar
                </span>
              </span>
              <BlueButton text="Aplicar" action={this.apply}/>
            </Row>
          </Dialog>
    )
  }
}
export default FilterDialog;
