import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from "react-redux"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import createBrowserHistory from 'history/createBrowserHistory'
import injectTapEventPlugin from 'react-tap-event-plugin';

//import Devtools from './components/devtools/devtools'
import Router from './router'
import colorPalette from './helpers/colorPalette'

import './index.css'
import createStore from "./store"
import reducer from "./reducers"

injectTapEventPlugin();
const muiTheme = getMuiTheme(colorPalette)
const SET_NODE = document.getElementById('root')
const history = createBrowserHistory()
const store = createStore(history)(reducer)

if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install()
}

ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
      <div>
        <Router store={store} history={history} />
      </div>
    </Provider>
  </MuiThemeProvider>,
  SET_NODE
);
