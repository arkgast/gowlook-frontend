import React, { Component } from 'react'

// redux tools
import {connect} from 'react-redux'
import {fetchCategories} from './reducers/actions/categoriesActions'

//react router tools
//import {Link} from 'react-router-dom'
import {withRouter} from 'react-router-dom'

//responsive component
import MediaQuery from 'react-responsive'

// Grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// material ui components
//import FlatButton from 'material-ui/FlatButton'
import Divider from 'material-ui/Divider'
import FontIcon from 'material-ui/FontIcon'

// material ui color palette
import {
  //grey900,
  //grey400,
  grey500,
  //grey300,
} from 'material-ui/styles/colors'

// our components
import Footer from './components/Footer'
//import BlueButton from './components/BlueButton'
import HistoryCard from './components/HistoryCard'
import LoginBox from './components/LoginBox'
import LoginBoxMobile from './components/LoginBox/mobile'
import NavBar from './components/AppBar'
import NavMobile from './components/AppBar/mobile'
import DrawerMenu from './components/DrawerMenu'
import ModalFilter from './components/ModalFilter'

//scroll Component
import Scroll from 'react-scroll'

// styles
import 'animate.css/animate.css'
import './App.css'
import './assets/fonts.css'

//assets
//import logo from './assets/img/gowlook.png'

// helpers
import './helpers/chunkarrays'

class App extends Component {
  constructor(props){
    super(props)
    this.scrollToTop = this.scrollToTop.bind(this)
    this.showingDrawer = this.showingDrawer.bind(this)
    this.redirect = this.redirect.bind(this)
    this.showModalFilter = this.showModalFilter.bind(this)
    this.moreAction = this.moreAction.bind(this)
    this.state = {
      openLogin: false,
      loginTab: 1,
      showDrawer: false,
      modalFilterStatus: false
    }
  }
  componentDidMount() {
    this.props.fetchCategories(10)
  }

  componentWillReceiveProps(newProps) {
    if(newProps.location.pathname !== this.props.location.pathname)
      this.toggleLogin(false)
  }
  scrollTo() {
    const scroller = Scroll.scroller;
    scroller.scrollTo('mainCategory' + this.props.category.id, {
      duration: 1000,
      delay: 100,
      smooth: true,
      offset: -30,
    })
  }
  showModalFilter () {
    this.setState((prevState) => {
      return {
        modalFilterStatus: !prevState.modalFilterStatus
      }
    })
  }
  moreAction () {
    if (this.props.location.pathname === '/') {
      this.scrollTo()
    } else {
      this.showModalFilter()
    }
  }
  scrollToTop() {
    const scroll = Scroll.animateScroll
    scroll.scrollToTop(
      {
        duration: 1500,
        delay: 1000,
        smooth: true,
      }
    )
  }
  redirect(url) {
    this.props.history.push(url)
  }
  showingDrawer(value) {
    this.setState(() => ( {
      showDrawer: value
    }))
  }
  toggleLogin(value, tab) {
    this.setState({
      loginTab:tab,
      showLogin: value
    })
  }
  render() {
    //const labelStyles = {textTransform: 'capitalize', color: grey900}
    const animationStyles = {transform: 'translateX(420px)', transition: '0.3s'}
    return (
      <div>
        <DrawerMenu
          show={this.state.showDrawer}
          close={() => this.showingDrawer(false)}
          toggleLogin={this.toggleLogin.bind(this, true, 1)}
          subCategories={this.props.subCategories}
          mainCategories={this.props.mainCategories}/>
        <MediaQuery query="(min-width: 800px)">
          <LoginBox
            showLogin={this.state.showLogin}
            buttonOneAction={this.toggleLogin.bind(this, true, 1)}
            buttonTwoAction={this.toggleLogin.bind(this, true, 2)}
            close={this.toggleLogin.bind(this, false)}
            tab={this.state.loginTab} />
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">
          <LoginBoxMobile
            showLogin={this.state.showLogin}
            buttonOneAction={this.toggleLogin.bind(this, true, 1)}
            buttonTwoAction={this.toggleLogin.bind(this, true, 2)}
            close={this.toggleLogin.bind(this, false)}
            tab={this.state.loginTab} />
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">
          <NavMobile
            showingDrawer={this.showingDrawer}
            toggleLogin={this.toggleLogin.bind(this, true, 1)}
            redirect={this.redirect} />
        </MediaQuery>
        <div style={this.state.showDrawer ? animationStyles : {}}>
          <MediaQuery query="(min-width: 800px)">
            <div className="container">
                <NavBar
                  moreAction={this.moreAction}
                  toggleLogin={this.toggleLogin.bind(this, true, 1)}
                  toggleSignIn={this.toggleLogin.bind(this, true, 2)}
                  redirect={this.redirect}/>
            </div>
          </MediaQuery>
            {this.props.children}
          {
            this.props.app.footerHistoriesVisibles && <MediaQuery query="(min-width: 800px)" style={{paddingTop: '40px', margin: '20px auto'}}>
              <div className="container">
                <h1 className="text-center" style={{color: grey500, fontWeight: 'normal'}}>
                  No te pierdas las mejores historias
                </h1>
                <Grid>
                  <Row>
                    {
                      [1,2,3].map(item =>
                        <Col key={item} sm={4}>
                          <HistoryCard
                            className={`gradient${item}`}
                          />
                        </Col>
                      )
                    }
                    <Col md={5} xs={4}><Divider/></Col>
                    <Col md={2} xs={4} className="text-center blue-hover" onClick={() => this.props.history.push('/histories')}>
                      <h3 className="text-center pointer blue-hover" style={{margin: '-10px 0px 5px 0px', color: grey500}}>
                        Ver todas las historias
                      </h3>
                      <FontIcon className="icon-026-arrows pointer blue-hover" style={{color: grey500}}/>
                    </Col>
                    <Col md={5} xs={4}><Divider/></Col>
                  </Row>
                </Grid>
              </div>
            </MediaQuery>
          }
          <Footer
            registerAction={this.scrollToTop} 
            showRegister={this.props.app.footerRegisterVisible}
            showPet={this.props.app.footerGowVisible}/>/>
          <ModalFilter
            open={this.state.modalFilterStatus}
            mainCategories={this.props.mainCategories}
            subCategories={this.props.subCategories}
          />
        </div>
      </div>
    );
  }
}

const mapStates = (state) => ({
  countrySelected: state.interactions.countrySelected,
  category: state.categories.mainCategories[0] || {},
  mainCategories: state.categories.mainCategories,
  subCategories: state.categories.subCategories,
  app: state.app
})
const mapDispatch = {
  fetchCategories,
}

export default withRouter( connect(mapStates, mapDispatch)( App ) );
