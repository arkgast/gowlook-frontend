import React from 'react'
import { Col, Row } from 'react-flexbox-grid'
import PropTypes from 'prop-types'
import PostCard from './PostCard'

function TopLatestPosts (props) {
  return (<div className='postech-latest-posts'>
            <Row>
              { props.posts.map((item, index) => <Col key={index} md={4}><PostCard key={index} {...item} /></Col>) }
            </Row>
          </div>)
}

TopLatestPosts.propTypes = {
  posts: PropTypes.array.isRequired
}

export default TopLatestPosts
