import React from 'react'
import PropTypes from 'prop-types'
import MenuItem from './MenuItem'
import SubMenu from './SubMenu'

class PostechMenu extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      isSubMenuOpen: false,
      isSubMenuHover: false,
      currentSubcategories: []
    }


    this.handleSubMenu = this.handleSubMenu.bind(this)
    this.handleSubMenuLeave = this.handleSubMenuLeave.bind(this)
  }

  handleSubMenu (subCategories) {
    if(subCategories && subCategories.length)
      setTimeout(() => this.setState({ currentSubcategories: subCategories, isSubMenuOpen: true }), 500)
  }

  handleSubMenuLeave () {
    setTimeout(() => {
      if(!this.state.isSubMenuHover)
        this.setState({ isSubMenuOpen: false, currentSubcategories: [] })
    }, 500)
  }


  render () {
    return (<nav className='postech-menu'>
              {
                this
                  .props
                  .menuItems
                  .map(({ label, key, isActived, subCategories }) => {
                    return (<span
                                key={key}
                                onMouseEnter={() => this.handleSubMenu(subCategories)}
                                onMouseLeave={this.handleSubMenuLeave}>
                                  <MenuItem isActived={isActived || false} label={label} /></span>)
                  })
              }
              { this.state.isSubMenuOpen && <div
                                              onMouseEnter={() => this.setState({isSubMenuHover: true, isSubMenuOpen: true})}
                                              onMouseLeave={() => {
                                                this.setState({isSubMenuHover: false})
                                                this.handleSubMenuLeave()
                                              }}
                                              >
                                              <SubMenu subCategories={this.state.currentSubcategories} />
                                            </div>}
            </nav>)
  }
}

PostechMenu.propTypes = {
  menuItems: PropTypes.array.isRequired
}


export default PostechMenu
