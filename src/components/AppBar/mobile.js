import React from 'react'

//assets
import logo from '../../assets/img/logoMovil.png'

//react router tools
import {Link} from 'react-router-dom'

//responsive component
//import MediaQuery from 'react-responsive'

// material ui components
//import FlatButton from 'material-ui/FlatButton'
import AppBar from 'material-ui/AppBar'
//import IconMenu from 'material-ui/IconMenu'
import FontIcon from 'material-ui/FontIcon'
//import DownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down'
import IconButton from 'material-ui/IconButton'
//import Divider from 'material-ui/Divider'
//import {grey900, grey400} from 'material-ui/styles/colors'

// our components
//import BlueButton from '../BlueButton'

// json styles
//const labelStyles = {textTransform: 'capitalize', color: grey900}

//Appbar custom
const AppBarMobile = (props) => (
  <AppBar
    className="relative"
    style={{paddingRight: "0px"}}
    zDepth={0} >
      <div style={{position: "absolute", left: 5, top: 0}}>
        <div className="pull-left">
          <IconButton onTouchTap={() => props.showingDrawer(true)}>
            <FontIcon className="icon-003-search blue-icon"/>
          </IconButton>
        </div>
        <div className="pull-left">
          <Link to="/"> <img src={ logo } alt=" " style={{width: '90px', marginTop: '10px'}}/> </Link>
        </div>
      </div>
      <div>
        <IconButton
          style={{paddingRight: "0px", paddingLeft: "0px"}}
          onTouchTap={() => props.redirect("/histories")}
          iconStyle={{fontSize: "20px"}}>
          <FontIcon className="icon-011-open-book-top-view grey-icon"/>
        </IconButton>
        <IconButton
          style={{paddingRight: "0px", paddingLeft: "0px", width: "30px"}}
          iconStyle={{fontSize: "20px"}}>
          <FontIcon className="icon-024-share grey-icon"/>
        </IconButton>
        <IconButton
          style={{paddingLeft: "0px"}}
          onTouchTap={props.toggleLogin}
          iconStyle={{fontSize: "20px"}}>
          <FontIcon className="icon-004-user grey-icon"/>
        </IconButton>
      </div>
    </AppBar>
)

export default AppBarMobile
