import React from 'react'
// material ui
import RaisedButton from 'material-ui/RaisedButton'
import {grey300, grey900, cyan700} from 'material-ui/styles/colors'

// this component is like than Switch component, this need a optimization
const ToggleButtons = (props) => {
  if ( props.mobile ) {
    return (<div className={ "flex-center " + (props.className || '') }>
    <RaisedButton
      backgroundColor={props.tab !== 0 ? 'white' : grey300}
      labelColor={props.tab === 0 ? grey900 : cyan700}
      label={props.label1}
      labelStyle={{textTransform: 'capitalize'}}
      onTouchTap={() => props.toggle(0)}
      className="btn-left" />
    <RaisedButton
      backgroundColor={props.tab !== 1 ? 'white' : grey300}
      labelColor={props.tab === 1 ? grey900 : cyan700}
      label={props.label2}
      onTouchTap={() => props.toggle(1)}
      buttonStyle={{width: '60px'}}
      labelStyle={{textTransform: 'capitalize'}}
      className="btn-rigth" />
    </div>)
  }
  else
    return (<div className={ "flex-center " + (props.className || '') } style={props.style}>
      <RaisedButton
        backgroundColor={props.tab !== 0 ? 'white' : grey300}
        labelColor={props.tab === 0 ? grey900 : cyan700}
        label={props.label1}
        labelStyle={{textTransform: 'capitalize', fontSize: '10px'}}
        overlayStyle={{width: '60px'}}
        rippleStyle={{width: '60px'}}
        onTouchTap={() => props.toggle(0)}
        buttonStyle={{width: '60px'}}
        style={{width: '60px', minWidth: '60px', height: '20px'}}
        className="btn-left" />
      <RaisedButton
        backgroundColor={props.tab !== 1 ? 'white' : grey300}
        labelColor={props.tab === 1 ? grey900 : cyan700}
        label={props.label2}
        rippleStyle={{width: '60px'}}
        overlayStyle={{width: '60px'}}
        onTouchTap={() => props.toggle(1)}
        buttonStyle={{width: '60px'}}
        labelStyle={{textTransform: 'capitalize', fontSize: '10px'}}
        style={{width: '60px', minWidth: '60px', height: '20px'}}
        className="btn-rigth" />
    </div>)
}

export default ToggleButtons
