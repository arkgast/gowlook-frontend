import React from 'react'

// material ui components
import Paper from 'material-ui/Paper';

// material ui color palette
import {grey500} from 'material-ui/styles/colors'

// Grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// truncate component
import TextTruncate from 'react-text-truncate'

// react router compoennts
//import {Link} from 'react-router-dom'

// assets
import profile from '../../assets/img/profileHistory.png'
//style files
import './HistoryCard.css'

const HistoryCard = (props) => (
  <div className={ "history-card " + props.className }>
    <Paper className="subTarget1 absolute height-200 card-border" zDepth={0}>
      <Grid className="full-width">
        <Row style={{marginTop: '10px'}}>
          <Col sm={2} className="flex-center" style={{justifyContent: 'flex-start', maxWidth: '50px'}}>
            <img src={profile} alt="profile" style={{maxWidth: '35px', height: '35px', borderRadius: '50%'}}/>
          </Col>
          <Col xs={10} style={{maxWidth: 'calc(100% - 50px)'}}>
            <p style={{color: grey500, margin: '5px 0px 2px 0px', width: '100%'}}>Jean Marco</p>
            <span style={{width: '100%'}}>Hace 6 horas</span>
          </Col>
          <Col xs={12} style={{height: '80px'}} className="flex-center">
            <TextTruncate
              line={2}
              className="font-medium"
              style={{color: grey500}}
              truncateText="..."
              text="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis"
            />
          </Col>
          <Col md={4} xs={5} style={{textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap'}}>
            <span style={{fontSize: '10px', color: grey500}}>
              <i className="fa fa-eye"></i> 212 views
            </span>
          </Col>
          <Col md={8} xs={7} style={{textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap'}}>
            <span style={{fontSize: '10px', color: grey500}}>
              <i className="fa fa-tag"></i> Voice and Messagin Api
            </span>
          </Col>
        </Row>
      </Grid>
    </Paper>
    <Paper className="subTarget2 absolute height-200 card-border" zDepth={0}>
    </Paper>
    <Paper className="subTarget3 absolute height-200 card-border" zDepth={0}>
    </Paper>
  </div>
);
export default HistoryCard
