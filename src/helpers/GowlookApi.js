const basepath = 'http://159.203.65.97:8090'

module.exports = {
  getCategories: function() {
    return basepath + '/gowlook-api/es-es/categories/'
  },
  getByCategoryId: function(categoryId) {
    return basepath + '/gowlook-api/es-es/comparator/resultList'
  },
  getProductModal: function(productId) {
    return basepath + '/gowlook-api/es-es/products/modal/' + productId
  },
  getProduct: function(productId) {
    return basepath + '/gowlook-api/es-es/products/sheet/' + productId
  },
  getProductBySearch: function (search) {
    return basepath + '/gowlook-api/es-es/comparator/ searchEs'
  },
  getProductsComparison: function() {
    return basepath + '/gowlook-api/es-es/comparator/category'
  },
  getProductSearch: function(productId) {
    return basepath + '/gowlook-api/es-es/products/search/' + productId
  },
  getProductsTop3: function() {
    return basepath + '/gowlook-api/es-es/comparator/top3'
  },
  getOffer: function() {
    return basepath + '/gowlook-api/es-es/comparator/topoffer'
  }
}
