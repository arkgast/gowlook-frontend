import React, {Component} from 'react'

// flex
import { Row, Col } from 'react-flexbox-grid'
// material ui components
import {List, ListItem} from 'material-ui/List'
import Checkbox from 'material-ui/Checkbox'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import {grey500, cyan400} from 'material-ui/styles/colors'

// material ui icons
import RadioChecked from 'material-ui/svg-icons/toggle/radio-button-checked';
import RadioUnchecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import Bank from 'material-ui/svg-icons/action/account-balance'
import Money from 'material-ui/svg-icons/editor/attach-money'
import Shopping from 'material-ui/svg-icons/action/shopping-basket'
import ListIcon from 'material-ui/svg-icons/action/view-list'
import Module from 'material-ui/svg-icons/action/view-module'

// common components
import SideBarMenu from '../../../components/SideBarMenu'
import FilterBox from '../../../components/filterBox'

export default class SideFilterMenu extends Component {
  constructor(props) {
    super(props)
    // open list each component key
    this.state = {
      open: false,
      openFilter: false,
      open2: true,
      open3: true,
      open4: true,
      radioValue: [],
      listView: false,
    }
    this.onChangeRadio = this.onChangeRadio.bind(this)
  }
  onChangeRadio(event){
    const value = parseInt( event.target.value )
    if( this.state.radioValue.indexOf(value) > -1) {
      this.setState({radioValue: this.state.radioValue.filter(item => item !== value)})
    }
    else {
      this.setState({ radioValue: [ ...this.state.radioValue, value ] })
    }
    console.log(this.state.radioValue)

  }
  toggleModal(value) {
    this.setState(() => ( {openFilter: value} ))
  }
  handleNestedListToggle(key) {
    // toggle list each states
    this.setState({[ `open${key}` ]: !this.state[ `open${key}` ]})
  }
  render() {
  // json style
    const labelStyle = { textTransform: 'capitalize' }
    const filters = [
      {
        id: 'type1',
        name: 'Tipo de productos',
        filterType: 'productTypes'

      },
      {
        id: 'brand1',
        name: 'Marcas',
        filterType: 'brandFilter'

      }
    ]
    return (
      <div>
        <List>
            <ListItem
              style={{marginTop: '25px'}}
              key={2}
              primaryText="Tipo de producto"
              open={this.state.open2}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 2)}
              nestedItems={
                this.props.productTypes.map(( item, index ) => (
                  <ListItem
                    key={index}
                    primaryText={item.name}
                    innerDivStyle={{padding: '16px 16px 16px 50px'}}
                    style={{color: this.state.radioValue.indexOf(item.id) > -1 ? cyan400 : grey500, marginTop: '-15px'}}
                    leftCheckbox={
                      <Checkbox
                        value={item.id}
                        onCheck={this.onChangeRadio}
                        checked={this.state.radioValue.indexOf(item.id) > -1}
                        checkedIcon={<RadioChecked />}
                        uncheckedIcon={<RadioUnchecked />}
                      />
                    }
                  />
                ))
              }
            />
            <ListItem
              key={3}
              primaryText="Marcas"
              open={this.state.open3}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 3)}
              nestedItems={
                this.props.brandFilter.slice(0, 7).map((item, index) => (
                  <ListItem
                    key={index}
                    className="blue-hover"
                    id={index === 6 && "menu"}
                    hoverColor="white"
                    primaryText={index !== 6 ? item.name : undefined}
                    onTouchTap={this.toggleModal.bind(this, true)}
                    innerDivStyle={{padding: '16px 16px 16px 50px'}}
                    style={{
                      color: this.state.radioValue.indexOf(item.id) > -1 ? cyan400 : grey500, marginTop: '-15px',
                      textAlign: index !== 6 ? 'left' : "right"
                    }}
                    leftCheckbox={
                      index !== 6 &&
                      <Checkbox
                        value={item.id}
                        className="blue-hover"
                        onCheck={this.onChangeRadio}
                        checked={this.state.radioValue.indexOf(item.id) > -1}
                        checkedIcon={<RadioChecked />}
                        uncheckedIcon={<RadioUnchecked />}
                      />
                    }
                  >
                    {index === 6 && <span className="hover-border-non-padding"> Ver más </span>}
                  </ListItem>
                ))
              }
            />
            <ListItem
              key={4}
              primaryText="Precio"
              open={this.state.open4}
              onNestedListToggle={this.handleNestedListToggle.bind(this, 4)}
              nestedItems={[
                <ListItem key={1} />,
              ]}
            />
            </List>
            <FilterBox
              filters={filters}
              brandFilter={this.props.brandFilter}
              handleClose={this.toggleModal.bind(this, false)}
              productTypes={this.props.productTypes}
              radioValue={this.state.radioValue}
              openFilter={this.state.openFilter}
            />
      </div>
    )
  }
}
