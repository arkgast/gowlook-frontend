import React, {Component} from 'react'

//material ui components
import RaisedButton from 'material-ui/RaisedButton'

// material ui colors
import {grey300} from 'material-ui/styles/colors'

class CurveButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ishover: false
    }
  }
  changeOver(value) {
    this.setState({ishover: value})
  }


  render() {
    return (
      <RaisedButton
        onMouseOver={this.changeOver.bind(this, true)}
        onMouseLeave={this.changeOver.bind(this, false)}
        label={this.props.label}
        labelStyle={{textTransform: 'capitalize'}}
        style={{borderRadius: '40px', boxShadow: 'none', border: '1px solid ' + grey300 }}
        buttonStyle={{borderRadius: '40px'}}
        overlayStyle={{borderRadius: '40px'}}
        rippleStyle={{borderRadius: '40px'}}
        backgroundColor={!this.state.ishover ? 'white' : this.props.color}
        labelColor={!this.state.ishover ? this.props.color : 'white'}
        fullWidth />
    )
  }
}
export default CurveButton
