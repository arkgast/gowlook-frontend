import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import './SubNav.css'

const SubNavR = (props) => (
    <div  className='SubNav'>
      <Grid>
        <Row className="container">
          <Col xs={4} className="SubNav-sections">
            <span className="hover-border-non-padding pointer">Gowlook</span>
            <span style={{color: props.textColor}}> > { props.category } </span>
          </Col>
          <Col xs={4} xsOffset={4}>
            <div className="social-container">
              <a href="https://www.instagram.com/gowlook.photo/" target="_blank">
                  <i className="fa fa-instagram"></i>
             </a>
             <a href="https://plus.google.com/+Gowlook" target="_blank">
                <i className="fa fa-google-plus"></i>
              </a>
              <a href="https://www.facebook.com/Gowlook-534469503388904/?skip_nax_wizard=true" target="_blank">
                <i className="fa fa-facebook"></i>
              </a>
              <a href="https://twitter.com/gowlook" target="_blank">
                <i className="fa fa-twitter"></i>
              </a>
            </div>
          </Col>
        </Row>
      </Grid>
    </div>
)

export default SubNavR;
