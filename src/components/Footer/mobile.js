import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import './Footer.css'

const Footer = (props) => (
  <div>
    <br />
    <br />
      <div className="footer-container pull-left full-width">

        <div className="footer pull-left full-width relative" style={{zIndex: 2}}>
          <Grid>
            <Row>
              <Col lg={5} md={6} xs={12}>
                <div className="social-widget">
                  <h2 className="text-center"> <div className="invert-word">¿</div>  Quieres estar a la última? ¡Síguenos!</h2>
                  <div className="flex-center">
                    <a href="https://www.instagram.com/gowlook.photo/" target="_blank">
                    <span className="fa-stack fa-2x small-margin instagram-hover">
                      <i className="fa fa-instagram fa-stack-1x border-circle"></i>
                    </span>
                   </a>
                   <a href="https://plus.google.com/+Gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin google-hover">
                      <i className="fa fa-google-plus fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://www.facebook.com/Gowlook-534469503388904/?skip_nax_wizard=true" target="_blank">
                    <span className="fa-stack fa-2x small-margin facebook-hover">
                      <i className="fa fa-facebook fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://twitter.com/gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin twitter-hover">
                      <i className="fa fa-twitter fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="https://www.linkedin.com/company/gowlook" target="_blank">
                    <span className="fa-stack fa-2x small-margin linkedin-hover">
                      <i className="fa fa-linkedin fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                    <a href="#">
                    <span className="fa-stack fa-2x small-margin youtube-hover">
                      <i className="fa fa-youtube-play fa-stack-1x border-circle"></i>
                    </span>
                    </a>
                  </div>
                </div>
            </Col>
              <Col lg={7} md={6} xs={12}>
                <Row center="xs">
                  <Col md={4} xs={3}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Loremipsum</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="https://gowlook.com/legal/privacy.html" target="_blank">Politicas de privacidad</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Politica de cookies</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/legalnotice.html" target="_blank">Aviso legal</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/faq.html" target="_blank">FAQS</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col md={3} xs={3}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Goowlok</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="https://gowlook.com/legal/privacy.html" target="_blank">Consectetur</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Adipiscing</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/legalnotice.html" target="_blank">Maecenas</a>
                        </li>
                        <li>
                          <a href={"/postech/Histories"} target="_blank">Nam uma</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col md={4} xs={3}>
                    <div className="flex-center flex-wrap">
                      <h5 className="title-footer full-width text-left">Business</h5>
                      <ul className="list full-width text-left">
                        <li>
                          <a href="https://gowlook.com/legal/privacy.html" target="_blank">Consectetur</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/cookies.html" target="_blank">Adipiscing</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/legalnotice.html" target="_blank">Maecenas</a>
                        </li>
                        <li>
                          <a href="https://gowlook.com/legal/faq.html" target="_blank">Nam uma</a>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </div>
      </div>
  </div>
)

export default Footer
