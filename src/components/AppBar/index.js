import React from 'react'

//assets
import logo from '../../assets/img/gowlook.png'

//react router tools
import {Link} from 'react-router-dom'

//responsive component
import MediaQuery from 'react-responsive'

// material ui components
import FlatButton from 'material-ui/FlatButton'
import AppBar from 'material-ui/AppBar'
//import IconMenu from 'material-ui/IconMenu'
import Book from 'material-ui/svg-icons/maps/local-library'
import DownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down'
import IconButton from 'material-ui/IconButton'
//import Divider from 'material-ui/Divider'
import {
  //grey900,
  //grey400,
  grey500,
  cyan400
} from 'material-ui/styles/colors'

// our components
import BlueButton from '../BlueButton'

// json styles
const labelStyles = {
  textTransform: 'capitalize',
  color: grey500,
  fontSize: '14px',
  paddingLeft: '0px',
  paddingRight: '0px'
}

//Appbar custom
const AppBarGowlook = (props) => (
  <AppBar
    zDepth={0}
    iconElementLeft={ <IconButton> <Link to="/"> <img src={ logo } alt=" " style={{width: '170px'}}/> </Link> </IconButton>}
    iconElementRight={
      <div className="buttons-wrapper">
        <MediaQuery query="(min-width: 800px)">
          <div
            style={{paddingBottom: '0px'}}
            className="pull-left blue-hover pointer hover-border"
            onTouchTap={() => props.redirect("/histories")} >
            <span style={{margin: '10px 5px 0px 5px', float: 'left'}}>
              <Book/>
            </span>
            <span style={{margin: '15px 10px 0px 5px', float: 'left'}}>
              Stories
            </span>
          </div>
          <FlatButton
            hoverColor="white"
            className="blue-hover hover-border-span"
            labelStyle={labelStyles}
            label="Sign In"
            rippleColor={cyan400}
            style={{
              marginLeft: '20px',
              marginRight: '10px',
              marginTop: '5px',
              minWidth: '60px',
            }}
            onTouchTap={props.toggleSignIn}/>
          <BlueButton text="Create Acount" fontSize="14px" action={props.toggleLogin} primary/>
        </MediaQuery>
        <MediaQuery query="(max-width: 800px)">

        </MediaQuery>
      </div>
    } >
      <div className="nav-wrapper">
        <MediaQuery query="(min-width: 800px)">
          <MediaQuery query="(min-width: 900px)">
            <Link className="nav-item blue-hover hover-border" to="/results/20/3/1">
              Tablets
            </Link>
          </MediaQuery>
          <Link className="nav-item blue-hover pointer hover-border" to="/results/11/1/0">
            Smartphones
          </Link>
          <p className="nav-item blue-hover pointer hover-border" to="/" onClick={props.moreAction}>
            More <DownIcon style={{height: '17px', width: '17px'}}/>
          </p>
        </MediaQuery>
      </div>
    </AppBar>
)

export default AppBarGowlook
