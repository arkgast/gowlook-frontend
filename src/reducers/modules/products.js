import {
  FETCH,
  FETCH_MODAL,
  FETCH_FULLFILED,
  FETCH_COMPARISON_FULLFILED,
  FETCH_MODAL_FULLFILED,
  FETCH_PRODUCTS_TOP3,
  FETCH_PRODUCTS_OFFER,
  FETCH_MODAL_REJECTED,
  FETCH_REJECTED,
  FETCH_COMPARISON_REJECTED,
  //FETCH_SEARCH_FULLFILED,
  //FETCH_SEARCH_REJECTED,
  FETCH_PRODUCTS,
  FETCH_PRODUCTS_REJECTED,
  SET_PRODUCTS_TO_COMPARE,
  PRODUCT_SEARCH_INIT,
  PRODUCT_SEARCH_ERROR,
  PRODUCT_SEARCH_SUCCESS
} from '../constants/productsConst'

const initialStates = {
  productModal: {},
  fetching: false,
  fetched: false,
  error: null,
  product: {},
  productToCompare: [],
  productDataCompare: [],

  // get full product from content key
  productsByCategory: {},
  //feature array for filters
  featureDTOS: [],
  // product type array for filters
  productTypeDTOS: [],
  // brand array for filters
  brandDTOS: []
}

export default function reducer(state = initialStates, action) {
  switch (action.type) {
    case FETCH_MODAL:
    case FETCH:
      {
        return {
          ...state,
          fetching: true
        }
      }
    case FETCH_PRODUCTS:
      {
        return {
          ...state,
          productsByCategory: action.payload.pageProductByCategory,
          brandDTOS: action.payload.brandDTOS,
          featureDTOS: action.payload.featureDTOS,
          productTypeDTOS: action.payload.productTypeDTOS
        }
      }
    case FETCH_PRODUCTS_REJECTED:
      {
        return {
          ...state,
          error: action.payload
        }
      }
    case FETCH_MODAL_FULLFILED:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          productModal: action.payload,

        }
      }

    case FETCH_FULLFILED:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          product: action.payload,
        }
      }
    case FETCH_PRODUCTS_OFFER:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          offer: action.payload,
        }
      }
    case FETCH_PRODUCTS_TOP3:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          top3: action.payload,
        }
      }

    case FETCH_COMPARISON_FULLFILED:
      {
        return {
          ...state,
          fetching: false,
          fetched: true,
          productDataCompare: action.payload.productComparedResponses,

        }
      }

    case FETCH_MODAL_REJECTED:
    case FETCH_REJECTED:
    case FETCH_COMPARISON_REJECTED:
      {
        return {
          ...state,
          fetching: false,
          error: action.payload
        }
      }

    case SET_PRODUCTS_TO_COMPARE:
      {
        return {
          ...state,
          fetching: true,
          productToCompare: action.payload
        }
      }

    case PRODUCT_SEARCH_INIT:
      return { ...state, fetching: true, fetched: false }
    case PRODUCT_SEARCH_SUCCESS:
      return { ...state, fetching: false, fetched: true, product: action.payload }
    case PRODUCT_SEARCH_ERROR:
      return { ...state, fetching: false, fetched: true, error: action.error }

    default:
      break;
  }

  return state;
}
