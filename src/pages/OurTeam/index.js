import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Divider from 'material-ui/Divider'

import AppActions from '../../reducers/actions/appActions'

import './style.css'

class OurTeam extends React.Component {
  componentWillMount () {
    this.props.actions.toggleFooterGow(false)
    this.props.actions.toggleFooterHistories(false)
    this.props.actions.toggleFooterRegister(false)
  }
  render () {
    return (
      <section className='our-team'>
        <header className='our-team-header'>
          <h1>"Ninguno de nosotros es tan bueno como todos nosotros juntos."</h1>
        </header>
        <div className='our-team-content'>
          <h2 className='text-grey'>
            Our Team
          </h2>
          <Divider />
          <div className='our-team-grid'>
            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-joan.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Joan</span>
                <span>CEO</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-albert.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Albert</span>
                <span>Financial Director</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-bibiana.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Bibiana</span>
                <span>Digital Layer</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-manuel.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Manuel</span>
                <span>CTO</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-arquimedes.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Arquimedes</span>
                <span>Art Director</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-noleidy.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Noleidy</span>
                <span>Content Director</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-reinaldo.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Reinaldo</span>
                <span>CIO</span>
              </div>
            </div>

            <div className='our-team-hero'>
              <img src={require('../../assets/img/our-team/retrato-alexander.png')} alt="Albert" />
              <div className='text-grey'>
                <span>Alexander</span>
                <span>Multimedia Editor</span>
              </div>
            </div>

          </div>
        </div>
      </section>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(AppActions, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(OurTeam)
