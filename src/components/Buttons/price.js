import React from 'react'
import './styles/price.css'

const PriceButton = (props) => (
  <div className={ "price-buttom " + props.className} style={props.containerStyle} onClick={props.action}>
    <p style={props.priceStyle}>
      {props.price} €
    </p>
    <span style={props.textStyle}>
      Ver Precios >>
    </span>
  </div>
)

export default PriceButton
