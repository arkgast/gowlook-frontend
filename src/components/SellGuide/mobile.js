import React , {Component} from 'react'

// common components
import SellingSection from './'
import Top3Section from './top3Section'

// swipeableViews
import SwipeableViews from 'react-swipeable-views';


//Sample Data
import sampleData from '../../pages/Results/tabViews/sampleSellguide.data'
import uid from 'uid'
import Slider from 'react-slick'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './SellGuide.css'

class MobileSellingGuide extends Component {

  constructor() {
    super()
    this.state = {
      sliderSettings: {
        customPaging: function(i) {
          return <span className='dot'></span>
        },
        dots: true,
        dotsClass: 'slick-dots sell-guide-dots',
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }

    this.handleSlide = this.handleSlide.bind(this)
  }

  handleSlide(value, index) {
    this.setState({['slideIndex'+index]: value})
  }

  render() {

    const SellingSections = () => {
      return sampleData.map((item, index) => {
        const newIndex = (index + 1)
        const slideIndex = this.state['slideIndex' + newIndex]
        return (<SwipeableViews
                  index={slideIndex}
                  onChangeIndex={this.handleChange}
                  key={uid()}
                >
                  <SellingSection
                    right={true}
                    handleToggle={value => this.handleSlide(value, newIndex)}
                    slideIndex={slideIndex}
                    title={item.title}
                    content={item.content}
                    mobile={true}
                    />
                  <Top3Section
                    mobile={true}
                    handleToggle={value => this.handleSlide(value, newIndex)}
                    top3Products={this.props.top3Products}
                    slideIndex={slideIndex}/>
                </SwipeableViews>)
              })
    }

    return (
        <Slider className='sell-guide-mobile' {...this.state.sliderSettings}>
          <div>
            <h2 className='blue-title'>
              Qué lavadora comprar: guía de compra de uno de los electrodomésticos más importantes
            </h2>
            <div className='text-grey content'>
              <p>Las lavadoras se han vuelto un elemento imprescindible en el hogar. Y es que lavar a mano la ropa pasó a mejor vida. Ahora bien, si echas un vistazo a todas las opciones y no eres un experto en la materia, es muy posible que no sepas qué elementos debes tener en cuenta para decidir el modelo que se ajuste más a tus necesidades.</p>
              <a className='text-grey' href="#" >Leer más ></a>
            </div>

        </div>

        { SellingSections() }

        <div>
          <h2 className='blue-title'>
            Conclusiones: lavadora eficiente, de carga frontal y con funciones normales
          </h2>
          <p className='text-grey'>
            <p>Poniéndotelo fácil, en Gowlook te vamos a describir la lavadora perfecta desde nuestro humilde punto de vista. Y lo primero que te aconsejamos es que decidas el diseño que más se ajuste a tus necesidades: carga frontal o superior, siempre teniendo en cuenta lo que te hemos comentado a lo largo de la guía de compra.</p>

            <p>En segundo lugar, debes decidir la carga máxima de ropa que puede aceptar tu nueva lavadora. No te pilles los dedos con esta decisión y apuesta a caballo ganador: elije un modelo de siete kilogramos de carga máxima.</p>

            <p>Por último, deja a un lado las funciones llamativas y sé práctico en la decisión. ¿Realmente vas a sacar partido encender la lavadora cuando estés fuera de casa? Todas estas características encarecen el precio final y seguramente se queden en eso, una novedad muy llamativa que dispone tu modelo pero que pocas veces pondrás en práctica.</p>
          </p>
        </div>

      </Slider>)

  }
}

export default MobileSellingGuide
