import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-slick'
import uid from 'uid'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import KeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import KeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
import PostCard from './PostCard'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

function PostImageCarousel (props) {

  let settings = {
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      responsive: [ { breakpoint: 1024, settings: { slidesToShow: 2 } }, { breakpoint: 764, settings: 'unslick' } ]
   }

   return (
     <div className="single-post-articles-carousel">
          <Carousel {...settings}>
            {
              props.posts.map((item, index) => <div key={uid()} className='post-slide'><PostCard { ...item } /></div>)
            }
          </Carousel>
      </div>
   )
}

function PrevArrow (props) {
  const {className, style, onClick} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={onClick} backgroundColor='transparent'><KeyboardArrowLeft style={{fill:'#515151', width: 60, marginLeft: -20 }} /></FloatingActionButton>)
}
function NextArrow (props) {
  const {className, style, onClick} = props
  return (<FloatingActionButton mini className={className} style={style} onClick={onClick} backgroundColor='transparent'><KeyboardArrowRight style={{fill:'#515151', width: 60 }} /></FloatingActionButton>)
}

PostImageCarousel.propTypes = {
  posts: PropTypes.array.isRequired
}

export default PostImageCarousel;
