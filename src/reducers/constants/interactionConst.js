export const SET_COMPARATE_PRODUCTS = 'gowlook/interaction/SET_COMPARATE_PRODUCTS'
export const SELECT_DETAILS_PRODUCT = 'gowlook/interaction/SELECT_DETAILS_PRODUCT'
export const REMOVE_COMPARATE_PRODUCT = 'gowlook/interaction/REMOVE_COMPARATE_PRODUCT'
export const CLEAR_COMPARATIVE = 'gowlook/interaction/CLEAR_COMPARATIVE'
