import React from 'react';

// material ui Component
import Divider from 'material-ui/Divider'

// Grid
import { Row, Col } from 'react-flexbox-grid'

// material ui color palette
import {grey200} from 'material-ui/styles/colors'

// common components
import FilProducts from '../../../components/Products/ResultsProducts'

//styles
import './tabresult.css'

// paginator
import Pagination from 'react-js-pagination'

const Results = (props) => {
  console.log(props)
  return (
    <Row style={{marginTop: '30px'}}>
      {props.products.map((item, index) => (
        <Col
          xs={props.viewType ==='icons' ? 6 : 12}
          md={props.viewType === 'icons' ? 4 : 12}
          key={index}>
          {/* this conditional need to deporate, this is validation to show comparate button in checkbox*/}
          <FilProducts
            img={item.images[0]}
            product={item}
            customText={
              ( ( props.comparativeProducts.filter(comparate => comparate.id === item.id).length > 0 ) && props.comparativeProducts.length > 1 )
                  && "Comparar ya" }
            viewType={props.viewType}
            isChecked={props.comparativeProducts.filter(comparate => comparate.id === item.id).length > 0 }
            checkboxAction={props.checkboxAction}
            unCheckAction={props.unCheckAction}
            priceAction={() => props.priceAction(true, 0, item)}
            rankingAction={() => props.priceAction(true, 1, item)}
          />
          { props.viewType !== 'icons' &&  <Divider style={{backgroundColor: grey200}}/> }
        </Col>
      ))}
      <Col xs={12}>
          <Pagination
            innerClass="pagination-inner"
            activeClass="pagination-active"
            activePage={1}
            itemsCountPerPage={10}
            totalItemsCount={450}
            pageRangeDisplayed={5}
            onChange={console.log}
            onClick={console.log}
          />
      </Col>
    </Row>
  )
}

export default Results
