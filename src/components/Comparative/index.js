import React from 'react';

// router addons
import {withRouter} from 'react-router'

// material ui components
//import FontIcon from 'material-ui/FontIcon'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
//import IconLocationOn from 'material-ui/svg-icons/communication/location-on'

// color pallete
import {grey200, grey300} from 'material-ui/styles/colors'

// icons
import Cross from 'material-ui/svg-icons/navigation/close'

// json styles
const productBox = {
  display: 'inline-block',
  position: 'relative',
  width: '90px',
  overflow: 'hidden',
  padding: '0px 20px 0px 20px',
  borderRight: '1px solid ' + grey200
}
const productTitle = {
  width: '90%',
  overflow: 'hidden',
  marginBottom: '10px',
  textOverflow: 'ellipsis',
  fontSize: '12px'
}

// stateless component
// this styles should be depurate
const Comparative = (props) => {
  let paperStyles = {
    position: 'fixed',
    borderTop: '1px solid ' + grey200,
    left: '0px',
    paddingTop: '20px',
    boxSizing: 'border-box',
    width: '100%',
    height: '133x',
    display: 'flex',
    zIndex: '99999'
  }

  paperStyles[props.mobile ? "top" : "bottom"] = "0px";
  if( props.mobile ) paperStyles.borderBottom = '1px solid ' + grey300

  return props.products.length > 1 ? (
      <Paper zDepth={0}
        style={paperStyles}>
        <div style={{ width: 'calc(100% - 180px)', whiteSpace: 'nowrap', overflowX: 'auto' }}>
          {
            props.products &&
            props.products.map((product) => (
              <div
                style={productBox}
                key={product.id}>
                <div className="flex-center flex-wrap">
                  <IconButton
                    iconStyle={{width: '15px', height: '15px'}}
                    onTouchTap={props.removeAction.bind(this, product.id)}
                    style={{position: 'absolute', right: '5px', top: '-15px', width: '15px', height: '15px'}}>
                    <Cross/>
                  </IconButton>
                  <span
                    className="pointer blue-hover hover-border-non-padding pull-left"
                    onClick={() => props.history.push(`/sheet/${product.id}`)}
                    style={productTitle}>{product.name}</span>
                  <img src={product.images[0]} style={{maxWidth: '45px', height: '45px'}} alt=""/>
                </div>
              </div>
            ))

          }
        </div>
        <div style={{width: '180px', height: '100%'}} className="flex-center flex-wrap">
          <div className="text-center">
            <span
              onClick={props.clearAction}
              className="pointer hover-border-non-padding blue-hover"
              style={{fontSize: '14px'}}>Limpiar</span>
            {/*should be remove br*/}
            <br/>
            <RaisedButton
              primary
              style={{marginTop: '10px'}}
              onTouchTap={() => props.history.push(`/comparison/`)}
              labelStyle={{textTransform: 'capitalize'}}
              label="Comparar"
            />
          </div>
        </div>
      </Paper>
    ) : null ;
}

export default withRouter( Comparative );
