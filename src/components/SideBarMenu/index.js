import React, { Component } from 'react'
import Scroll from 'react-scroll'
// this component need  replace
import { Nav } from 'react-bootstrap';
import './SideBarMenu.css'


//const scroller = Scroll.scroller
const Link = Scroll.Link
// change to stateless component please
export default class SideBarMenu extends Component {
  render() {
    return (
      <Nav bsStyle="pills" stacked className="sideBarMenu" activeKey={0} onSelect={this.handleSelect}>
        {
          this.props.categories &&
            this.props.categories.map(( category, index ) => (
              <li key={index} className="remove-list-style side-item">
                <ul>
                  <Link activeClass="active"
                    className="pointer smooth-hover"
                    to={'mainCategory' + ( category.id || index )}
                    spy={true}
                    smooth={true}
                    offset={this.props.offset || -10}
                    duration={1500}
                    delay={0}
                    isDynamic={true}
                    onSetActive={this.handleSetActive}
                    onSetInactive={this.handleSetInactive}
                    ignoreCancelEvents={false} >
                    {category.name}
                  </Link>
                </ul>
              </li>
            ))
        }
      </Nav>
    )
  }
}
