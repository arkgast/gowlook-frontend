import React from 'react'
import PercentBar from './PercentBar'

const ComparisonTop = (props) => {
  return (
    <div className='comparison-top'>
      <div className='comparison-top-buttons'>
        <button className='gray-button'><i className='fa fa-plus'></i> Agregar</button>
        <button className='gray-button'><i className='fa fa-cloud-download'></i> Guardar</button>
        <button className='gray-button'><i className='fa fa-share'></i> Compartir</button>
      </div>

      <div className='comparison-top-products'>

        {
          Array(3).fill(null).map(() => (
            <div className='comparison-top-product'>
              <img src='https://s3-us-west-2.amazonaws.com/gowlook/categories/camaras' alt='camera' />
              <h3>Sony 45 GFT 67 TY</h3>
              <div className='comparison-top-product-feeds'>
                <div className='comparison-top-product-feed'>
                    <div className='text-grey'>Ranking</div><div><PercentBar percent={89} /></div>
                </div>
                <div className='comparison-top-product-feed'>
                    <div className='text-grey'>Índice social</div><div><PercentBar percent={38} /></div>
                </div>
              </div>
              <div className='comparison-top-product-price'>
                <button className='blue-line-button'>1.987 €</button>
                <div>
                  <a className='text-grey' href='#'>Ver precios ></a>
                </div>
              </div>
            </div>
          ))
        }

      </div>
    </div>
  )
}

export default ComparisonTop
