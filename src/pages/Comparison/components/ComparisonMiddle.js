import React from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-slick'
import uid from 'uid'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import emitterClient from '../../../services/emitter'


class ComparisonMiddle extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      carouselRef: 'middle'
    }

  }

  componentDidMount () {
    emitterClient.on('carousel:setIndex', (currentSlide) => {
        this.refs[this.state.carouselRef].slickGoTo(currentSlide)
    })
  }

  componentWillUnMount () {
    eventEmitter.removeEventListener('carousel:setIndex')
  }

  render () {

    return (
      <div className='comparison-middle'>

        <div className='comparison-middle-buttons'>
          <button className='gray-button'><i className='fa fa-plus'></i> Agregar</button>
          <button className='gray-button'><i className='fa fa-cloud-download'></i> Guardar</button>
          <button className='gray-button'><i className='fa fa-share'></i> Compartir</button>
          <div>
            <h5>Ranking Gowlook</h5>
            <h5>Precio</h5>
          </div>
        </div>



        <div className='comparison-middle-products'>
          <Carousel {...this.props.carouselSettings} ref={this.state.carouselRef}>

          {
            Array(7).fill(null).map(() => (
              <div className='comparison-middle-product' key={uid()}>
                <span className='close-button'><i className='fa fa-times'></i></span>
                <img src='https://s3-us-west-2.amazonaws.com/gowlook/categories/camaras' alt='camera' />
                <h3>Canon 6D</h3>
                <div className='comparison-middle-ranking'>
                   <span className='text-blue'>4.5</span>
                </div>
                <div className='comparison-middle-price-rank'>
                  <span>456 - 566 €</span>
                </div>
              </div>
            ))
          }
        </Carousel>

        </div>

      </div>
    )
  }
}


ComparisonMiddle.propTypes = {
  carouselSettings: PropTypes.object.isRequired
}

export default ComparisonMiddle
