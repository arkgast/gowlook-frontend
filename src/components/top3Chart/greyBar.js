import React from 'react'

const GreyBar = (props) => (
  <div className="progress-bar-ap">
    <div className="progress-ap-track">
      <div className="progress-ap-fill" style={props.style2}>
        <span></span>
      </div>
    </div>
  </div>
)

export default GreyBar
