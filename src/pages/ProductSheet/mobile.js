import React, {Component} from 'react'

// material ui components
import Divider from 'material-ui/Divider'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

// material ui colors
import {
  grey300,
  //grey500,
  cyan400,
  grey200,
  //grey400,
  grey600,
} from 'material-ui/styles/colors'

//material ui icons
//import Star from 'material-ui/svg-icons/toggle/star'

// use this icons when rate working
//import StarBorder from 'material-ui/svg-icons/toggle/star-border'
//import StarHalf from 'material-ui/svg-icons/toggle/star-half'

// Redux elements
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'
import {fetchProduct} from '../../reducers/actions/productActions'

//grid
import { Grid, Row, Col } from 'react-flexbox-grid'

// slider action component
import SwipeableViews from 'react-swipeable-views';

// common componets
import RankingChart from '../../components/RankingChart'
import PriceButton from '../../components/Buttons/price'
//import SideBarMenu from '../../components/SideBarMenu'
import Slider from '../../components/Slider'
import RaisedButton from 'material-ui/RaisedButton'


// ES6 component
class MobileSheet extends Component {
  constructor() {
    super()
    this.state = {
       image: 0
    }
    this.handleChange = this.handleChange.bind(this)
  }
  componentDidMount() {
    this.props.fetchProduct(this.props.match.params.id)
  }
  handleChange(value) {
    this.setState(() => ({image: value}))
  }
  render() {
    const {product} = this.props
    const nextIndex = product.images && ( this.state.image === product.images.length -1  ? this.state.image  : this.state.image + 1 )
    const prevIndex = product.images && ( this.state.image === 0 ? 0 : this.state.image - 1 )
    return (
      <Grid fluid style={{minHeight: '500px', paddingTop: '15px'}} className="relative">
        <Divider style={{position: 'absolute', top: '1px', left: '0px', width: '100%'}}/>
        <Row style={{marginTop: '10px'}} >
          <Col xs={12} className="flex-space-around blue-hover" style={{borderBottom: '1px solid' + grey300}}>
            <p className="text-grey pointer blue-hover flex-center margin-none" style={{marginBottom: "5px"}}>
              <span className="icon-010-square-grid" style={{fontSize: '1.2em'}}/>
            </p>
            <p className="text-grey pointer blue-hover flex-center margin-none" style={{marginBottom: "5px"}}>
              <span className="icon-009-students-cap" style={{fontSize: '1.5em'}}/>
            </p>
            <p className="text-grey pointer blue-hover margin-none" style={{marginBottom: "5px"}}>
              <span className="icon-008-logout-button" style={{fontSize: '1.2em'}}/>
            </p>
          </Col>
          <Col xs={12} style={{borderBottom: '1px solid' + grey300}}>
            <Row>
              <Col xs={12} className="relative">
                <div >
                  <h1 className="text-center text-grey" style={{fontSize: '2.3em', fontWeight: 'normal'}}>
                    {product.name}
                  </h1>
                  <SwipeableViews
                    index={this.state.image}
                    onChangeIndex={this.handleChange} >
                    {
                      product.images &&
                      product.images.map(( image, index ) => (
                        <div className="flex-center" >
                          <img src={image} alt="" style={{maxWidth: '100%', height: '280px'}} key={index}/>
                        </div>
                      ))
                    }
                  </SwipeableViews>
                </div>
                <span
                  className="icon-006-back pointer"
                  onClick={() => this.handleChange(prevIndex)}
                  style={{fontSize: '3em', position: 'absolute', top: '200px', left: '50px', color: grey300}}/>
                <span
                  className="icon-007-next pointer"
                  onClick={() => this.handleChange(nextIndex)}
                  style={{fontSize: '3em', position: 'absolute', top: '200px', right: '50px', color: grey300}}/>
                  <ul className="flex-center">
                    {
                      product.images &&
                      product.images.map(( image, index ) => (
                      <li
                        key={index}
                        style={{
                          display: 'inline-block',
                          backgroundColor: this.state.image === index ? cyan400 : grey200,
                          height: '15px',
                          width: '15px',
                          marginLeft: '5px',
                          borderRadius: '50%'
                          }}/>
                      ))
                    }
                  </ul>
              </Col>
              <Col xs={12}>
                <Row>
                  <Col xs={6} className="flex-center">
                    <PriceButton
                      containerStyle={{width: '120px', padding: '15px 0px'}}
                      priceStyle={{fontSize: '26px'}}
                      textStyle={{fontSize: '13px', margin: '22px 0px 0px 0px'}}
                      price={product.price}/>
                  </Col>
                  <Col xs={6} className="flex-center">
                    <RankingChart
                      size="92px"
                      ranking={product.ranking}/>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          {/* product feature container */}
          <Col xs={12}>
            <Row>
              <Col xs={12}>
                {
                  product.groupFeatureByCategoryResponses &&
                    product.groupFeatureByCategoryResponses.map((item, index) => (
                      <div style={{margin: '20px 0px'}}>
                        <h2
                          style={{
                              fontWeight: 'normal',
                              textTransform: 'capitalize',
                              textAlign: 'left',
                              fontSize: '33px',
                              marginTop: '10px'}}>
                          {item.name}
                        </h2>
                        {
                          item.productFeatureResponses.map((element, key) =>(
                            <Row key={key} style={{margin: '10px 0px', fontSize: '12px'}}>
                              <Col xs={6} className="text-left">
                                { element.name }
                              </Col>
                              <Col xs={6} className="text-right" style={{color: grey600}}>
                                { element.value }
                              </Col>
                            </Row>
                          ))
                        }
                      </div>
                    ))
                  }
              </Col>
            </Row>
            <Divider/>
          </Col>
          <Col xs={12} style={{marginTop: '20px'}}>
            <Row>
              <Col xs={12} style={{padding: '20px'}}>
                <Row style={{marginBottom: '10px'}}>
                  <Col xs={12}>
                    <p style={{color: cyan400, fontSize: '1.5em', textAlign: 'center'}}> Vota y Opina</p>
                  </Col>
                </Row>
                <Row style={{margin: '0px'}} className="flex-center">
                  <Col xs={4}>
                    <img
                      src="https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p160x160/16864101_10155183204123783_2537964751586452809_n.jpg?oh=c311f1df2fe626e81be168d9c5e87cc6&oe=5A0A05ED&__gda__=1510467993_e004c799929e4cba51847cdaf9500b2f"
                      alt=""
                      style={{width: '80px', height: '80px', borderRadius: '50%'}}/>
                  </Col>
                  <Col xs={8}>
                    <p style={{textAlign: 'left'}}> Joan Figueras</p>
                  </Col>
                </Row>
                <div style={{marginTop: '20px'}}>
                   <SelectField
                      value={this.state.value}
                      onChange={this.handleChange}
                      fullWidth
                      floatingLabelStyle={{color: cyan400}}
                      floatingLabelText="Puntuar todo"
                    >
                      { [<MenuItem key={1} value={1} primaryText="Todo" />] }
                    </SelectField>
                </div>
                <div>
                  <Row style={{margin: '20px 0px'}}>
                    <Col xs={7}>
                      <Slider/>
                    </Col>
                    <Col xs={5} className="text-right truncate" style={{fontSize: '12px'}}>
                      Caracteriastica1
                    </Col>
                  </Row>
                  <Row style={{margin: '20px 0px'}}>
                    <Col xs={7}>
                      <Slider/>
                    </Col>
                    <Col xs={5} className="text-right truncate" style={{fontSize: '12px'}}>
                      Caracteriastica2
                    </Col>
                  </Row>
                  <Row style={{margin: '20px 0px'}}>
                    <Col xs={7}>
                      <Slider/>
                    </Col>
                    <Col xs={5} className="text-right truncate" style={{fontSize: '12px'}}>
                      Caracteriastica3
                    </Col>
                  </Row>
                  <Row style={{margin: '20px 0px'}}>
                    <Col xs={7}>
                      <Slider/>
                    </Col>
                    <Col xs={5} className="text-right truncate" style={{fontSize: '12px'}}>
                      Caracteriastica4
                    </Col>
                  </Row>
                  <Row style={{margin: '20px 0px'}}>
                    <Col xs={7}>
                      <Slider/>
                    </Col>
                    <Col xs={5} className="text-right truncate" style={{fontSize: '12px'}}>
                      Caracteriastica5
                    </Col>
                  </Row>
                </div>
                <Divider/>
                <div style={{marginTop: '15px'}}>
                  <p style={{textAlign: 'left'}}>Comentario</p>
                  <textarea style={{width: '100%', border: `1px solid ${grey200}`, height: '100px'}} cols="30" rows="10">
                  </textarea>
                  <RaisedButton
                    primary
                    style={{marginTop: '10px'}}
                    labelStyle={{textTransform: 'capitalize'}}
                    fullWidth
                    label="Publicar"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    )
  }
}
const mapStates = (states) => ({
  product: states.product.product
})
const mapDispatch = {
  fetchProduct
}

export default withRouter( connect(mapStates, mapDispatch)( MobileSheet ) )

