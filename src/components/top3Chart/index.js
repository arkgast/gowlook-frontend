import React from 'react'

//grid
import { Row, Col } from 'react-flexbox-grid';

// subcomponents
import BlueBar from './blueBar'
import GreyBar from './greyBar'

//styles
import './top3Chart.css'

const Top3Chart = () => {
  var percent = 75
  var height = percent + "%"
  var top = (100 - percent) + '%'

  //percent product
  var percent3 = 55
  var height3 = percent3 + "%"
  var top3 = (100 - percent3) + '%'

  //percent general
  var percent2 = 35
  var height2 = percent2 + "%"
  var top2 = (100 - percent2) + '%'

  var style = {
    height: height,
    top: top
  }
  var style2 = {
    height: height2,
    top: top2
  }
  var style3 = {
    height: height3,
    top: top3
  }

  return (
    <div className="chart-bar-container">
        <Row center="xs">
          <Col xs={12}>
            <div className="containerProgressBar vertical rounded">
              <div className="chart-group">
                <GreyBar/>
                <BlueBar style={style} percent="75%" character="36 MP GHU"/>
              </div>
              <div className="chart-group">
                <GreyBar/>
                <BlueBar style={style2} percent="20%" character="56 MP GHU"/>
              </div>
              <div className="chart-group">
                <GreyBar/>
                <BlueBar style={style3} percent="46%" character="40 MP GHT" />
              </div>
              <div className="metric-section section-100">
                <div className="relative">
                  <p> 100% </p>
                </div>
              </div>
              <div className="metric-section section-70">
                <div className="relative">
                  <p>
                    70%
                  </p>
                </div>
              </div>
              <div className="metric-section section-35">
                <p>
                  35%
                </p>
              </div>
           </div>
          </Col>
        </Row>
   </div>
  );
}

export default Top3Chart
